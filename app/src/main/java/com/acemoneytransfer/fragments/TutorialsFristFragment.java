package com.acemoneytransfer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acemoneytransfer.R;
import com.acemoneytransfer.activities.TutorialsActivity;
import com.acemoneytransfer.views.TextViewAvenirLTStdMedium;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suarebits on 11/2/16.
 */
public class TutorialsFristFragment extends Fragment {
    View tutorialView;
    @Bind(R.id.nextTxt)
    TextViewAvenirLTStdMedium nextTxt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        tutorialView = inflater.inflate(R.layout.tutorialsfristfragment_layout, container, false);
        ButterKnife.bind(this, tutorialView);
        return tutorialView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    @OnClick(R.id.nextTxt)
    public void nextTxt() {
        TutorialsActivity.viewPager.setCurrentItem(1);
    }
}
