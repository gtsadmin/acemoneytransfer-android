package com.acemoneytransfer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acemoneytransfer.R;
import com.acemoneytransfer.activities.LoginActivity;
import com.acemoneytransfer.views.TextViewAvenirLTStdMedium;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TutorialsThirdFragment extends Fragment {
    View tutorialView;
    @Bind(R.id.letsgoTxt)
    TextViewAvenirLTStdMedium letsgoTxt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        tutorialView = inflater.inflate(R.layout.tutorialsthirdfragment_layout, container, false);
        ButterKnife.bind(this, tutorialView);
        return tutorialView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.letsgoTxt)
    public void letsgoTxt() {

        startActivity(new Intent(getActivity(), LoginActivity.class).putExtra("login", 1));
        getActivity().overridePendingTransition(R.anim.left, R.anim.right);
        getActivity().finish();
    }
}
