package com.acemoneytransfer.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.activities.LoginActivity;
import com.acemoneytransfer.activities.MainActivity;
import com.acemoneytransfer.activities.UpdateIDActivity;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.model.IDListModel;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class IDListFragment extends Fragment {
    View view;
    @Bind(R.id.listView)
    SwipeMenuListView listView;
    ArrayList<IDListModel> rowItems;
    IDSAdapter idsAdapter;
    @Bind(R.id.swipeTxt)
    TextViewAvenirLTStdBook swipeTxt;
    @Bind(R.id.norecordfoundTxt)
    TextViewAvenirLTStdBook norecordfoundTxt;

    //     AddIDFragment addIDFragment = new AddIDFragment();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.idlist_layout, container, false);
        ButterKnife.bind(this, view);

        createDeletebtn();
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
//                        DataManager.getInstance().setIdListModel(rowItems.get(position));
//                        ((MainActivity) getActivity()).loadHomeFragment(addIDFragment, "AddIDFragment");
                        Intent updateIntent = new Intent(getActivity(), UpdateIDActivity.class);
                        updateIntent.putExtra("idmodel", rowItems.get(position));
                        getActivity().startActivity(updateIntent);
                        break;
                    case 1:

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Are you sure you want to delete this ID?")
                                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (MyApplication.isInternetWorking(getActivity())) {
                                    deleteID(rowItems.get(position).getDocID(), position);
                                }
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
        if (MyApplication.isInternetWorking(getActivity())) {
            getIdoc();
        }
//        getIdoc();
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();


        ((MainActivity) getActivity()).toolbar.setTitle("");
        ((TextView) ((MainActivity) getActivity()).logo.findViewById(R.id.txt)).setText("ID List");
//        if (!MyApplication.DATAUPDATESTUTAS) {
//            if (MyApplication.isInternetWorking(getActivity())) {
//                getIdoc();
//            }
//        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getIdoc() {
        final ProgressDialog dialog = ProgressDialog.show(getActivity(),"","Please wait...");
        MyApplication.getApplication().hideSoftKeyBoard(getActivity());
        final ArrayList<IDListModel> idListModels = new ArrayList<>();

        final JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceIDocs/DocList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null}}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceDocList")) {
                                    JSONArray aceDocListArr = response.getJSONArray("AceDocList");
                                    for (int i = 0; i < aceDocListArr.length(); i++) {
                                        JSONObject subobj = aceDocListArr.getJSONObject(i);
                                        IDListModel idListModel = new IDListModel();
                                        if (subobj.has("DocID"))
                                            idListModel.setDocID(subobj.getString("DocID"));

                                        if (subobj.has("DocNumber"))
                                            idListModel.setDocNumber(subobj.getString("DocNumber"));
                                        if (subobj.has("DocType"))
                                            idListModel.setDocType(subobj.getString("DocType"));
                                        if (subobj.has("DocIssueDate"))
                                            idListModel.setDocIssueDate(subobj.getString("DocIssueDate"));
                                        if (subobj.has("DocExpireDate"))
                                            idListModel.setDocExpireDate(subobj.getString("DocExpireDate"));
                                        if (subobj.has("DocFileUrl"))
                                            idListModel.setDocFileUrl(subobj.getString("DocFileUrl"));
                                        if (subobj.has("DocFileBackUrl"))
                                            idListModel.setDocFileBackUrl(subobj.getString("DocFileBackUrl"));



                                        idListModels.add(idListModel);
                                    }
                                    if (idListModels.size() == 0) {
                                        norecordfoundTxt.setVisibility(View.VISIBLE);
                                        swipeTxt.setVisibility(View.GONE);
                                    }else
                                    {
                                        swipeTxt.setVisibility(View.VISIBLE);
                                        norecordfoundTxt.setVisibility(View.GONE);
                                    }
                                    idsAdapter = new IDSAdapter(getActivity(), idListModels);
                                    listView.setAdapter(idsAdapter);
                                }
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage("Please login agian", getActivity());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void deleteID(String delete_id, final int position) {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(getActivity());
        MyApplication.getApplication().hideSoftKeyBoard(getActivity());

        final JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("DocID", delete_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceIDocs/IdocDelete", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.getApplication().hideProgressDialog();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null}}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                rowItems.remove(position);
                                idsAdapter.notifyDataSetChanged();

                            } else if (rflag == 0 && code == 101) {
                                popMessage("Please login agian");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    public void popMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private class IDSAdapter extends BaseAdapter {
        Context context;


        public IDSAdapter(Context context, ArrayList<IDListModel> rowItems) {
            this.context = context;
            IDListFragment.this.rowItems = rowItems;

        }

        /* private view holder class */
        private class ViewHolder {
            TextViewAvenirLTStdBook idnumberTxt, idnameTxt;
            ImageView iddocImgView;

        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder = null;

            try {
                LayoutInflater mInflater = (LayoutInflater) context
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                if (convertView == null) {
                    convertView = mInflater.inflate(
                            R.layout.add_id_list_layout, null);
                    holder = new ViewHolder();
                    holder.idnumberTxt = (TextViewAvenirLTStdBook) convertView
                            .findViewById(R.id.idnumberTxt);

                    holder.idnameTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.idnameTxt);
                    holder.iddocImgView = (ImageView) convertView.findViewById(R.id.iddocImgView);
                    holder.iddocImgView.setVisibility(View.VISIBLE);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }


                holder.idnameTxt.setText(rowItems.get(position).getDocType());
                holder.idnumberTxt.setText(rowItems.get(position).getDocNumber());
                MyApplication.getApplication().loader.displayImage(rowItems.get(position).getDocFileUrl(), holder.iddocImgView, MyApplication.options);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }
    }

    public void createDeletebtn() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth((int) getResources().getDimension(R.dimen.dim_50));
                // set item title
                openItem.setTitle("Update");
                // set item title fontsize
                openItem.setTitleSize(15);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
//                SwipeMenuItem deleteItem = new SwipeMenuItem(
//                        getActivity());
//                // set item background
//                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
//                        0x3F, 0x25)));
//                // set item width
//                deleteItem.setWidth((int) getResources().getDimension(R.dimen.dim_50));
//                deleteItem.setTitle("Delete");
//                // set item title fontsize
//                deleteItem.setTitleSize(15);
//                // set item title font color
//                deleteItem.setTitleColor(Color.WHITE);
//                // add to menu
//                menu.addMenuItem(deleteItem);
            }
        };

// set creator
        listView.setMenuCreator(creator);
    }

}
