package com.acemoneytransfer.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.acemoneytransfer.R;
import com.acemoneytransfer.activities.ProfileActivity;
import com.acemoneytransfer.activities.RecepientListActivity;
import com.acemoneytransfer.activities.SendMoneyActivity;
import com.acemoneytransfer.activities.TransactionActivity;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.model.UserInfoWrapper;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeFragment extends Fragment {


    @Bind(R.id.transactionsTxt)
    TextViewAvenirLTStdBook transactionsTxt;
    @Bind(R.id.sendMoneyTxt)
    TextViewAvenirLTStdBook sendMoneyTxt;
    @Bind(R.id.reciepentLisTxt)
    TextViewAvenirLTStdBook reciepentLisTxt;
    @Bind(R.id.bottomBar)
    LinearLayout bottomBar;
    @Bind(R.id.completedTxtViw)
    TextViewAvenirLTStdBook completedTxtViw;
    @Bind(R.id.inCompleteTxtViw)
    TextViewAvenirLTStdBook inCompleteTxtViw;
    @Bind(R.id.centerLayout)
    LinearLayout centerLayout;
    @Bind(R.id.transactionsTxtViw)
    TextViewAvenirLTStdBook transactionsTxtViw;
    @Bind(R.id.welcomeTxt)
    public TextViewAvenirLTStdBook welcomeTxt;
    UserInfoWrapper userInfoWrapper;
    ArrayList<String> countryList = new ArrayList<>();
    HashMap<String, String> ios_code = new HashMap<>();
    ArrayList<String> isoList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_home, container, false);


        setHasOptionsMenu(true);
        ButterKnife.bind(this, v);


//        getActivity().startService(new Intent(getActivity(),TimeService.class));

        return v;

    }

    @OnClick(R.id.transactionsTxtViw)
    public void transactionsTxtViw() {
        MyApplication.getApplication().produceAnimation(transactionsTxtViw);
        startActivity(new Intent(getActivity(), TransactionActivity.class).putExtra("status",Constants.ALLTRANSCATION));
        getActivity().overridePendingTransition(R.anim.left, R.anim.right);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MyApplication.isInternetWorking(getActivity())) {
            getDashboard();
        }
        getTime();
        setData();
    }

    @OnClick(R.id.completedTxtViw)
    public void completedTxtViw() {
        MyApplication.getApplication().produceAnimation(completedTxtViw);
        startActivity(new Intent(getActivity(), TransactionActivity.class).putExtra("status",Constants.COMPLETE));
        getActivity().overridePendingTransition(R.anim.left, R.anim.right);
    }

    @OnClick(R.id.inCompleteTxtViw)
    public void inCompleteTxtViw() {
        MyApplication.getApplication().produceAnimation(inCompleteTxtViw);
        startActivity(new Intent(getActivity(), TransactionActivity.class).putExtra("status",Constants.INCOMPLETE));
        getActivity().overridePendingTransition(R.anim.left, R.anim.right);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        if (menu != null) {
            menu.clear();
        }
        inflater.inflate(R.menu.menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:

                break;


        }
        return true;

    }


    @OnClick(R.id.reciepentLisTxt)
    public void reciepentLisTxt() {
        MyApplication.getApplication().produceAnimation(reciepentLisTxt);
        startActivity(new Intent(getActivity(), RecepientListActivity.class).putExtra("code", Constants.MAINACTIVITYTORECEIVERLIST));
        getActivity().overridePendingTransition(R.anim.left,
                R.anim.right);
    }

    @OnClick(R.id.sendMoneyTxt)
    public void sendMoneyTxt() {
       try {
           if (TextUtils.isEmpty(userInfoWrapper.getAddress())||
                   TextUtils.isEmpty(userInfoWrapper.getCity())||
                   TextUtils.isEmpty(userInfoWrapper.getCountryIsoCode())||
                   TextUtils.isEmpty(userInfoWrapper.getCountryOfBirthIsoCode())||
                   TextUtils.isEmpty(userInfoWrapper.getNationalityIsoCode())||
                   TextUtils.isEmpty(userInfoWrapper.getGender())||
                   TextUtils.isEmpty(userInfoWrapper.getPostalCode())||
                   TextUtils.isEmpty(userInfoWrapper.getCurrencyCode())||
                   TextUtils.isEmpty(userInfoWrapper.getFullName()))
           {
               AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
               builder.setMessage("Please note that your transactions cannot be processed without a complete profile")
                       .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       dialog.dismiss();
                       startActivity(new Intent(getActivity(), ProfileActivity.class));
                       getActivity().overridePendingTransition(R.anim.left,
                               R.anim.right);

                   }
               }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       dialog.dismiss();
                   }
               });

               AlertDialog alert = builder.create();
               alert.show();
           }else {
//               MyApplication.getApplication().getReceivingMethod().setPayInAmount("");

               startActivity(new Intent(getActivity(), SendMoneyActivity.class).putExtra("code", Constants.MAINACTIVITYTOSENDMONEY));
               getActivity().overridePendingTransition(R.anim.left,
                       R.anim.right);
           }
       }catch (Exception ex)
       {
           ex.printStackTrace();
       }

    }

    @OnClick(R.id.transactionsTxt)
    public void transactionsTxt() {
        MyApplication.getApplication().produceAnimation(transactionsTxt);
        startActivity(new Intent(getActivity(), TransactionActivity.class).putExtra("status",Constants.ALLTRANSCATION));
        getActivity().overridePendingTransition(R.anim.left,
                R.anim.right);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getDashboard() {



        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUser/GetDashboard", object.toString(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response"+response);
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                transactionsTxtViw.setText(response.getString("TotalTransections"));
                                completedTxtViw.setText(response.getString("CompletedTransections"));
                                inCompleteTxtViw.setText(response.getString("InCompleteTransections"));
                                JSONObject ace_userObj = response.getJSONObject("AceUser");
                                userInfoWrapper = new UserInfoWrapper();
                                userInfoWrapper.setAddress(ace_userObj.getString("Address"));
                                userInfoWrapper.setCity(ace_userObj.getString("City"));
                                userInfoWrapper.setCountryIsoCode(ace_userObj.getString("CountryIsoCode"));
                                userInfoWrapper.setDOB(ace_userObj.getString("DOB"));
                                userInfoWrapper.setCountryOfBirthIsoCode(ace_userObj.getString("CountryOfBirthIsoCode"));
                                userInfoWrapper.setEmail(ace_userObj.getString("Email"));
                                userInfoWrapper.setFullName(ace_userObj.getString("FullName"));
                                userInfoWrapper.setGender(ace_userObj.getString("Gender"));
                                userInfoWrapper.setNationalityIsoCode(ace_userObj.getString("NationalityIsoCode"));
                                userInfoWrapper.setPhone(ace_userObj.getString("Phone"));
                                userInfoWrapper.setPostalCode(ace_userObj.getString("PostalCode"));

                                userInfoWrapper.setCurrencyCode(ios_code.get(ace_userObj.getString("CountryIsoCode")));

                                DataManager.getInstance().setUserInfoWrapper(userInfoWrapper);
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, getActivity());
                            } else {
                                popMessage(message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    public void popMessage(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");


        Date afternoonTime = null;
        Date eveningTime = null;
        Date nightTime = null;
        Date morningTime = null;
        try {
            afternoonTime = dateFormat.parse("11:59 AM");
            eveningTime = dateFormat.parse("05:00 PM");
            nightTime = dateFormat.parse("08:00 PM");
            morningTime = dateFormat.parse("04:00 AM");

            Date afternoonTimeCurrent = dateFormat.parse(dateFormat.format(new Date()));
            Date eveningTimeeCurrent = dateFormat.parse(dateFormat.format(new Date()));
            Date nightTimeCurrent = dateFormat.parse(dateFormat.format(new Date()));
            Date morningTimeCurrent = dateFormat.parse(dateFormat.format(new Date()));

            if (afternoonTimeCurrent.after(afternoonTime) && eveningTimeeCurrent.before(eveningTime)) {
                welcomeTxt.setText("Good Afternoon !");
            } else if (eveningTimeeCurrent.after(eveningTime) && nightTimeCurrent.before(nightTime))

            {

                welcomeTxt.setText("Good Evening !");
            } else if (nightTimeCurrent.after(nightTime) && morningTimeCurrent.before(morningTime)) {
                welcomeTxt.setText("Good Night !");
            } else if (morningTimeCurrent.after(morningTime) && afternoonTimeCurrent.before(afternoonTime)) {
                welcomeTxt.setText("Good Morning !");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setData() {


        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(getActivity());
        dbHelper.openDataBase();
        countryList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
        isoList = dbHelper.getCountryList(SqlLiteDbHelper.CURRENCY_CODE);
        for (int i = 0; i < countryList.size(); i++) {

            ios_code.put(countryList.get(i), isoList.get(i));
        }


    }
}
