package com.acemoneytransfer.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.activities.LoginActivity;
import com.acemoneytransfer.activities.MainActivity;
import com.acemoneytransfer.activities.ProfileActivity;
import com.acemoneytransfer.activities.SendMoneyActivity;
import com.acemoneytransfer.activities.TransactionActivity;
import com.acemoneytransfer.activities.TransactionSummery;
import com.acemoneytransfer.activities.TutorialsActivity;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.datacontroller.DataParser;
import com.acemoneytransfer.model.ReceiverModel;
import com.acemoneytransfer.model.ReceivingMethod;
import com.acemoneytransfer.model.TransactioListModel;
import com.acemoneytransfer.model.UserInfoWrapper;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MenuFragment extends Fragment {


    public AddIDFragment addIDFragment = new AddIDFragment();
    HomeFragment homeFragment = new HomeFragment();
    public IDListFragment idListFragment = new IDListFragment();
    @Bind(R.id.closeImgBtn)
    ImageButton closeImgBtn;
    @Bind(R.id.nameTxt)
    TextViewAvenirLTStdBook nameTxt;
    @Bind(R.id.emailTxt)
    TextViewAvenirLTStdBook emailTxt;
    @Bind(R.id.topRightContainer)
    LinearLayout topRightContainer;
    @Bind(R.id.homeTxt)
    TextViewAvenirLTStdBook homeTxt;
    @Bind(R.id.createTransactionTxt)
    TextViewAvenirLTStdBook createTransactionTxt;
    @Bind(R.id.reuseTransactionTxt)
    TextViewAvenirLTStdBook reuseTransactionTxt;
    @Bind(R.id.allTransactionTxt)
    TextViewAvenirLTStdBook allTransactionTxt;
    @Bind(R.id.profileTxt)
    TextViewAvenirLTStdBook profileTxt;
    @Bind(R.id.addIdTxt)
    TextViewAvenirLTStdBook addIdTxt;
    @Bind(R.id.aboutTxt)
    TextViewAvenirLTStdBook aboutTxt;
    @Bind(R.id.tutorialsTxt)
    TextViewAvenirLTStdBook tutorialsTxt;
    @Bind(R.id.settingsTxt)
    TextViewAvenirLTStdBook settingsTxt;
    @Bind(R.id.logoutTxt)
    TextViewAvenirLTStdBook logoutTxt;
    @Bind(R.id.container_ll)
    RelativeLayout containerLl;
    @Bind(R.id.idListTxt)
    TextViewAvenirLTStdBook idListTxt;
    UserInfoWrapper userInfoWrapper;
    ArrayList<TransactioListModel> transactioListModels;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, v);

        emailTxt.setText(MyApplication.getUserEmail("email"));
        userInfoWrapper = DataManager.getInstance().getUserInfoWrapper();
        return v;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @OnClick(R.id.container_ll)
    public void containerLl() {

        return;
    }

    @OnClick(R.id.logoutTxt)
    public void logoutTxt() {

        logoutAlert();

    }

    @OnClick(R.id.tutorialsTxt)
    public void tutorialsTxt() {
        ((MainActivity) getActivity()).closeDrawers();
        getActivity().overridePendingTransition(R.anim.left,
                R.anim.right);
        startActivity(new Intent(getActivity(), TutorialsActivity.class));
    }


    @OnClick(R.id.closeImgBtn)
    public void closeImgBtn() {
        ((MainActivity) getActivity()).closeDrawers();
    }

    @OnClick(R.id.reuseTransactionTxt)
    public void reuseTransactionTxt() {
        ((MainActivity) getActivity()).loadHomeFragment(homeFragment, "HomeFragment");
        ((MainActivity) getActivity()).closeDrawers();
        getTranslist();
//        getActivity().overridePendingTransition(R.anim.left,
//                R.anim.right);
//        if (!DataManager.getInstance().getPaymentId().equalsIgnoreCase("")) {
////            startActivity(new Intent(getActivity(), TransactionSummery.class));
//            startActivity(new Intent(getActivity(), SendMoneyActivity.class).putExtra("code", Constants.SENDMONEYTORECEIVERDETAILS));
//        }else
//        {
//            Toast.makeText(getActivity(),"No Previous Transactions",Toast.LENGTH_SHORT).show();
//        }

    }

    @OnClick(R.id.allTransactionTxt)
    public void allTransactionTxt() {
        ((MainActivity) getActivity()).loadHomeFragment(homeFragment, "HomeFragment");
        ((MainActivity) getActivity()).closeDrawers();
        getActivity().overridePendingTransition(R.anim.left,
                R.anim.right);
        startActivity(new Intent(getActivity(), TransactionActivity.class).putExtra("status", Constants.ALLTRANSCATION));

    }

    @OnClick(R.id.profileTxt)
    public void profileTxt() {
        ((MainActivity) getActivity()).loadHomeFragment(homeFragment, "HomeFragment");
        ((MainActivity) getActivity()).closeDrawers();
        getActivity().overridePendingTransition(R.anim.left,
                R.anim.right);
        startActivity(new Intent(getActivity(), ProfileActivity.class));
    }

    @OnClick(R.id.idListTxt)
    public void idListTxt() {

        ((MainActivity) getActivity()).loadHomeFragment(idListFragment, "IDSListFragment");
    }

    @OnClick(R.id.addIdTxt)
    public void addIdTxt() {

        ((MainActivity) getActivity()).loadHomeFragment(addIDFragment, "AddIDFragment");

    }

    @OnClick(R.id.aboutTxt)
    public void aboutTxt() {
        ((MainActivity) getActivity()).closeDrawers();
    }

    @OnClick(R.id.homeTxt)
    public void homeTxt() {
        if (homeFragment.isAdded())
        {
            getActivity().getSupportFragmentManager().beginTransaction().show(homeFragment);
        }else {
            ((MainActivity) getActivity()).loadHomeFragment(homeFragment, "HomeFragment");
        }
    }

    @OnClick(R.id.createTransactionTxt)
    public void createTransactionTxt() {
        try {
            ((MainActivity) getActivity()).loadHomeFragment(homeFragment, "HomeFragment");
            userInfoWrapper = DataManager.getInstance().getUserInfoWrapper();
            if (TextUtils.isEmpty(userInfoWrapper.getAddress())||
                    TextUtils.isEmpty(userInfoWrapper.getCity())||
                    TextUtils.isEmpty(userInfoWrapper.getCountryIsoCode())||
                    TextUtils.isEmpty(userInfoWrapper.getCountryOfBirthIsoCode())||
                    TextUtils.isEmpty(userInfoWrapper.getGender())||
                    TextUtils.isEmpty(userInfoWrapper.getPostalCode())||
                    TextUtils.isEmpty(userInfoWrapper.getCurrencyCode())||
                    TextUtils.isEmpty(userInfoWrapper.getFullName()))
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Please note that your transactions cannot be processed without a complete profile")
                        .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), ProfileActivity.class));
                        getActivity().overridePendingTransition(R.anim.left,
                                R.anim.right);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }else {
                ((MainActivity) getActivity()).closeDrawers();
                getActivity().overridePendingTransition(R.anim.left,
                        R.anim.right);
                startActivity(new Intent(getActivity(), SendMoneyActivity.class).putExtra("code", Constants.MAINACTIVITYTORECEIVERLIST));
            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void logoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("" + getResources().getString(R.string.app_name));
        builder.setMessage("Are you sure you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                MyApplication.saveAuthToken("AuthToken", "");
                                startActivity(new Intent(getActivity(), LoginActivity.class).putExtra("login",1));
                                getActivity().finish();

                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }
    public void getTranslist() {

        MyApplication.getApplication().showProgressDialog(getActivity());
        MyApplication.getApplication().hideSoftKeyBoard(getActivity());


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceTrans/TransList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());


                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceTransList")) {
                                    JSONArray aceTransListArr = response.getJSONArray("AceTransList");
                                    transactioListModels = new DataParser(Constants.TRANSCATIONLISTCODE, aceTransListArr).getTranscationlist(-1);

                                    if (transactioListModels.size() > 0) {
                                        ReceivingMethod receivingMethod = new ReceivingMethod();
                                        receivingMethod.setPayerId(transactioListModels.get(0).getPayerID());
                                        receivingMethod.setSelect_payment_method(transactioListModels.get(0).getPaymentMethod());
                                        receivingMethod.setPayInAmount(transactioListModels.get(0).getPayInAmount());
                                        Constants.RECEIVINGMETHOD = true;
                                        Constants.SENDINGMETHOD = true;
                                        MyApplication.getApplication().setReceivingMethod(receivingMethod);
                                        DataManager.getInstance().transactionModel.setPayment_method(transactioListModels.get(0).getPaymentMethod());
                                        DataManager.getInstance().transactionModel.setPayerBranchcode(transactioListModels.get(0).getPayoutBranchCode());
                                        DataManager.getInstance().transactionModel.setReceiver_currency(transactioListModels.get(0).getReceivingCurrency());
                                        DataManager.getInstance().transactionModel.setSending_amount(transactioListModels.get(0).getPayInAmount());
                                        DataManager.getInstance().transactionModel.setReceiver_amount(transactioListModels.get(0).getPayOutAmount());
                                        DataManager.getInstance().transactionModel.setPayerBankname(transactioListModels.get(0).getPayerName());
                                        DataManager.getInstance().transactionModel.setPayerBranchname(transactioListModels.get(0).getPayoutBranchName());
                                        MyApplication.getApplication().setSending_method(transactioListModels.get(0).getSendingPaymentMethod());

                                        DataManager.getInstance().transactionModel.setBene_id(transactioListModels.get(0).getBeneID());
//                                        ReceiverModel model = new ReceiverModel();
//                                        model.setBeneID(sub_obj.getString("BeneID"));
//                                        model.setBeneName(transactioListModels.get(0).get);

//                                        startActivity(new Intent(getActivity(), SendMoneyActivity.class).putExtra("code", Constants.SENDMONEYTORECEIVERDETAILS));
                                        if (MyApplication.isInternetWorking(getActivity())) {
                                            getBeneInfo(transactioListModels.get(0).getBeneID());
                                        }
                                    }else
                                    {
                                        MyApplication.getApplication().hideProgressDialog();
                                        Toast.makeText(getActivity(),"You have no previous transactions",Toast.LENGTH_SHORT).show();
                                    }

                                }

                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, getActivity());
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
    private void getBeneInfo(String beneid) {


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BeneID", beneid);

//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/GetBene", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.getApplication().hideProgressDialog();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONObject sub_obj = null;
                                ReceiverModel model = new ReceiverModel();
                                sub_obj = response.getJSONObject("AceBene");

                                if (sub_obj.has("BeneID")) {
                                    sub_obj.getString("BeneID");
                                    model.setBeneID(sub_obj.getString("BeneID"));
                                }
                                if (sub_obj.has("BeneName")) {
                                    model.setBeneName(sub_obj.getString("BeneName"));
                                }
                                if (sub_obj.has("BeneAddress")) {
                                    model.setBeneAddress(sub_obj.getString("BeneAddress"));
                                }
                                if (sub_obj.has("BeneCity")) {
                                    model.setBeneCity(sub_obj.getString("BeneCity"));
                                }

                                if (sub_obj.has("BenePostCode")) {
                                    model.setBenePostCode(sub_obj.getString("BenePostCode"));
                                }
                                if (sub_obj.has("BeneCountryIsoCode")) {
                                    model.setBeneCountryIsoCode(sub_obj.getString("BeneCountryIsoCode"));
                                }
                                if (sub_obj.has("BenePhone")) {
                                    model.setBenePhone(sub_obj.getString("BenePhone"));

                                }
                                if (sub_obj.has("BeneBankName")) {

                                    model.setBeneBankName(sub_obj.getString("BeneBankName"));
                                    DataManager.getInstance().transactionModel.setReceiverBankName(sub_obj.getString("BeneBankName"));
                                }
                                if (sub_obj.has("BeneBranchName")) {
                                    model.setBeneBranchName(sub_obj.getString("BeneBranchName"));
                                    DataManager.getInstance().transactionModel.setReceiverBranchName(sub_obj.getString("BeneBranchName"));

                                }
                                if (sub_obj.has("BeneAccountNumber")) {
                                    model.setBeneAccountNumber(sub_obj.getString("BeneAccountNumber"));

                                }

                                if (sub_obj.has("BeneBankCode")) {
                                    sub_obj.getString("BeneBankCode");
                                    DataManager.getInstance().transactionModel.setReceiverBankCode(sub_obj.getString("BeneBankCode"));
                                }
                                if (sub_obj.has("BeneBankBranchCode")) {
                                    sub_obj.getString("BeneBankBranchCode");
                                    DataManager.getInstance().transactionModel.setReceiverBankBranchCode(sub_obj.getString("BeneBankBranchCode"));
                                }
                                if (sub_obj.has("PayoutBranchCode")) {
//                                    DataManager.getInstance().transactionModel.setPayerBranchcode(sub_obj.getString("PayoutBranchCode"));
                                }
                                DataManager.getInstance().setModel(model);
                                startActivity(new Intent(getActivity(), SendMoneyActivity.class).putExtra("code", Constants.SENDMONEYTORECEIVERDETAILS));
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, getActivity());
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
}
