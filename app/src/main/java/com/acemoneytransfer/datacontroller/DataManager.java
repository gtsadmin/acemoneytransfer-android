package com.acemoneytransfer.datacontroller;

import com.acemoneytransfer.model.ReceiverModel;
import com.acemoneytransfer.model.TransactionModel;
import com.acemoneytransfer.model.UserInfoWrapper;

import java.util.HashMap;

public class DataManager {
    private static DataManager dManager;

    public String getReceivingMethod() {
        return receivingMethod;
    }

    public void setReceivingMethod(String receivingMethod) {
        this.receivingMethod = receivingMethod;
    }

    String receivingMethod ="";
    public ReceiverModel getModel() {
        return model;
    }

    public void setModel(ReceiverModel model) {
        this.model = model;
    }

    //    public IDListModel getIdListModel() {
//        return idListModel;
//    }
//
//    public void setIdListModel(IDListModel idListModel) {
//        this.idListModel = idListModel;
//    }
//
//    IDListModel idListModel = new IDListModel();
//
    ReceiverModel model = new ReceiverModel();
    UserInfoWrapper userInfoWrapper = new UserInfoWrapper();

    public UserInfoWrapper getUserInfoWrapper() {
        return userInfoWrapper;
    }

    public void setUserInfoWrapper(UserInfoWrapper userInfoWrapper) {
        this.userInfoWrapper = userInfoWrapper;
    }

    public HashMap<String, String> getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(HashMap<String, String> currency_code) {
        this.currency_code = currency_code;
    }

    HashMap<String, String> currency_code = new HashMap<>();

    public TransactionModel transactionModel = new TransactionModel();

    public static DataManager getInstance() {

        if (dManager == null) {
            dManager = new DataManager();
            return dManager;
        } else {
            return dManager;
        }
    }

    String paymentId = "";

    public boolean isUpdateTrans() {
        return updateTrans;
    }

    public void setUpdateTrans(boolean updateTrans) {
        this.updateTrans = updateTrans;
    }

    boolean updateTrans =true;

    public String getCountryNamestr() {
        return countryNamestr;
    }

    public void setCountryNamestr(String countryNamestr) {
        this.countryNamestr = countryNamestr;
    }

    String countryNamestr ="";
    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }
}
