package com.acemoneytransfer.datacontroller;

public class Constants {
    public static final String SUCCESS = "success";
    public static final String ERROR_MESSAGE = "Sorry for your inconvenience, Ace will be up shortly.";

    public static String NEWTORK_ERROR = "Could not connect to Internet!";
    public static String NO_INTERNET_CONNECTION = "Please connect to working Internet connection!";
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";

    public static final String AUTH_KEY = "!ACE1K5|EY";

    public static final String USER_ID = "USER_ID";
    public static final int RECEPIENTLISCODE = 4;
    public static final int LOGINHANDLENUM = 0;
    public static boolean RECEIVINGMETHOD = false;
    public static boolean SENDINGMETHOD = false;
    public static String current_country = "";
    public static int MAINACTIVITYTORECEIVERLIST=1;
    public static int RECEIVERLISTTOSENDMONEY=2;
    public static int SENDMONEYTORECEIVERLIST=3;
    public static int MAINACTIVITYTOSENDMONEY=4;
    public static int TRANSCATIONSUCESS =5;
    public static int TRANSCATIONLISTCODE =6;
    public static int SENDMONEYTORECEIVERDETAILS =7;

    public static int  INCOMPLETE =0;
    public static int  COMPLETE =1;
    public static int  ALLTRANSCATION =2;

    public static int FRONTIMAGE_BACKIMAGE = 8;
    //public static int BACKIMAGE =9;


    public static String debit_url = "https://www.aftabcurrency.com/app-debit.php?id=";
    public static String credit_url ="https://www.aftabcurrency.com/app-credit.php?id=";
}

