package com.acemoneytransfer.datacontroller;

import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.model.BankBranchModel;
import com.acemoneytransfer.model.ReceiverModel;
import com.acemoneytransfer.model.TransactioListModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by nitin on 8/10/15.
 */
public class DataParser {


    JSONObject object;
    JSONArray jsonArray;

    public DataParser(int class_code, JSONArray jsonArray) {
        this.jsonArray = jsonArray;

    }


    public ArrayList<TransactioListModel> getTranscationlist(int payment_status) {
        ArrayList<TransactioListModel> transactioListModels = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            TransactioListModel transactioListModel = new TransactioListModel();
            try {
                JSONObject transcationSubObj = jsonArray.getJSONObject(i);
                if (transcationSubObj.has("PaymentNumber")) {
                    transactioListModel.setPaymentNumber(transcationSubObj.getString("PaymentNumber"));
                }
                if (transcationSubObj.has("PaymentMethod")) {
                    transactioListModel.setPaymentMethod(transcationSubObj.getString("PaymentMethod"));
                }
                if (transcationSubObj.has("PaymentDate")) {
                    transactioListModel.setPaymentDate(transcationSubObj.getString("PaymentDate"));
                }
                if (transcationSubObj.has("SendingCountry")) {
                    transactioListModel.setSendingCountry(transcationSubObj.getString("SendingCountry"));
                }
                if (transcationSubObj.has("SendingCurrency")) {
                    transactioListModel.setSendingCurrency(transcationSubObj.getString("SendingCurrency"));
                }
                if (transcationSubObj.has("ReceivingCountry")) {
                    transactioListModel.setReceivingCountry(transcationSubObj.getString("ReceivingCountry"));
                }
                if (transcationSubObj.has("ReceivingCurrency")) {
                    transactioListModel.setReceivingCurrency(transcationSubObj.getString("ReceivingCurrency"));
                }
                if (transcationSubObj.has("PayInAmount")) {
                    transactioListModel.setPayInAmount(transcationSubObj.getString("PayInAmount"));
                }
                if (transcationSubObj.has("PayOutAmount")) {
                    transactioListModel.setPayOutAmount(transcationSubObj.getString("PayOutAmount"));
                }
                if (transcationSubObj.has("ExchangeRate")) {
                    transactioListModel.setExchangeRate(transcationSubObj.getString("ExchangeRate"));
                }
                if (transcationSubObj.has("SenderName")) {
                    transactioListModel.setSenderName(transcationSubObj.getString("SenderName"));
                }
                if (transcationSubObj.has("BeneName")) {
                    transactioListModel.setBeneName(transcationSubObj.getString("BeneName"));
                }
                if (transcationSubObj.has("PaymentStatus")) {
                    transactioListModel.setPaymentStatus(transcationSubObj.getString("PaymentStatus"));
                }
                if (transcationSubObj.has("PayerID")) {
                    transactioListModel.setPayerID(transcationSubObj.getString("PayerID"));
                }
                if (transcationSubObj.has("PayerName")) {
                    transactioListModel.setPayerName(transcationSubObj.getString("PayerName"));
                }
                if (transcationSubObj.has("PayerBranchCode")) {
                    transactioListModel.setPayerBranchCode(transcationSubObj.getString("PayerBranchCode"));
                }
                if (transcationSubObj.has("BeneID")) {
                    transactioListModel.setBeneID(transcationSubObj.getString("BeneID"));
                }
                if (transcationSubObj.has("PayoutBranchCode")) {
                    transactioListModel.setPayoutBranchCode(transcationSubObj.getString("PayoutBranchCode"));
                }

                if (transcationSubObj.has("SendingPaymentMethod")) {
                    transactioListModel.setSendingPaymentMethod(transcationSubObj.getString("SendingPaymentMethod"));
                }
                if (transcationSubObj.has("PayoutBranchName")) {
                    transactioListModel.setPayoutBranchName(transcationSubObj.getString("PayoutBranchName"));
                }
                if (payment_status == Constants.COMPLETE) {

                    if (transcationSubObj.getString("PaymentStatus").equalsIgnoreCase("Complete")) {
                        transactioListModels.add(transactioListModel);
                    }
                } else if (payment_status == Constants.INCOMPLETE) {
                    if (transcationSubObj.getString("PaymentStatus").equalsIgnoreCase("Incomplete")) {
                        transactioListModels.add(transactioListModel);
                    }
                } else {
                    transactioListModels.add(transactioListModel);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return transactioListModels;

    }

    public ArrayList<BankBranchModel> getBankBranch() {
        ArrayList<BankBranchModel> bankBranchModels = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject subbankbrachObj = jsonArray.getJSONObject(i);
                BankBranchModel bankBranchModel = new BankBranchModel();
                if (subbankbrachObj.has("BranchId")) {
                    bankBranchModel.setBranchId(subbankbrachObj.getString("BranchId"));
                }
                if (subbankbrachObj.has("BranchName")) {
                    bankBranchModel.setBranchName(subbankbrachObj.getString("BranchName"));
                }
                if (subbankbrachObj.has("BranchAddress")) {
                    bankBranchModel.setBranchAddress(subbankbrachObj.getString("BranchAddress"));
                }

                if (subbankbrachObj.has("BranchCode")) {
                    bankBranchModel.setBranchCode(subbankbrachObj.getString("BranchCode"));
                }
                if (subbankbrachObj.has("BranchPayCode")) {
                    bankBranchModel.setBranchPayCode(subbankbrachObj.getString("BranchPayCode"));
                }
                bankBranchModels.add(bankBranchModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        return bankBranchModels;
    }

    public ArrayList<ReceiverModel> getReceivers() {
        ArrayList<ReceiverModel> receiverModels = new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                ReceiverModel receiverModel = new ReceiverModel();
                JSONObject sub_obj = null;

                sub_obj = jsonArray.getJSONObject(i);

                if (sub_obj.has("BeneID")) {
                    receiverModel.setBeneID(sub_obj.getString("BeneID"));
                }
                if (sub_obj.has("BeneName")) {
                    receiverModel.setBeneName(sub_obj.getString("BeneName"));
                }
                if (sub_obj.has("BeneAddress")) {
                    receiverModel.setBeneAddress(sub_obj.getString("BeneAddress"));
                }
                if (sub_obj.has("BeneCity")) {
                    receiverModel.setBeneCity(sub_obj.getString("BeneCity"));
                }
                if (sub_obj.has("BenePostCode")) {
                    receiverModel.setBenePostCode(sub_obj.getString("BenePostCode"));
                }
                if (sub_obj.has("BeneCountryIsoCode")) {
                    receiverModel.setBeneCountryIsoCode(sub_obj.getString("BeneCountryIsoCode"));
                }
                if (sub_obj.has("BenePhone")) {
                    receiverModel.setBenePhone(sub_obj.getString("BenePhone"));
                }
                if (sub_obj.has("BeneBankName")) {
                    receiverModel.setBeneBankName(sub_obj.getString("BeneBankName"));

                }
                if (sub_obj.has("BeneBranchName")) {
                    receiverModel.setBeneBranchName(sub_obj.getString("BeneBranchName"));

                }
                if (sub_obj.has("BeneAccountNumber")) {
                    receiverModel.setBeneAccountNumber(sub_obj.getString("BeneAccountNumber"));

                }

                if (sub_obj.has("BeneBankCode")) {
                    receiverModel.setBeneBankCode(sub_obj.getString("BeneBankCode"));
                }
                if (sub_obj.has("BeneBankBranchCode")) {
                    receiverModel.setBeneBankBranchCode(sub_obj.getString("BeneBankBranchCode"));
                }
                receiverModels.add(receiverModel);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return receiverModels;
    }
}
