package com.acemoneytransfer.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.model.ReceivingMethod;

import butterknife.Bind;
import butterknife.ButterKnife;



public class CardSelectActivity extends AppCompatActivity {
    @Bind(R.id.webview)
    WebView webview;
    Toolbar toolbar;
    String cardUrl ="";
    boolean call_back_urlBoolean = false;
    View logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_select_layout);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
         logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Select Card");
//        getSupportActionBar().setTitle("Transaction List");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
        try {
            cardUrl = getIntent().getExtras().getString("PaymentID");
            System.out.println("cardUrl"+cardUrl);
        }catch (Exception ex)
        {

        }

        webview.setWebViewClient(new MyBrowser());
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        if (MyApplication.isInternetWorking(CardSelectActivity.this)) {
            webview.loadUrl(cardUrl);
        }
        DataManager.getInstance().transactionModel.setPayment_method("");
        DataManager.getInstance().transactionModel.setStation("");
        DataManager.getInstance().transactionModel.setStation_Address("");
        DataManager.getInstance().transactionModel.setPayerBranchcode("");
        DataManager.getInstance().transactionModel.setPayerBranchname("");
        DataManager.getInstance().transactionModel.setPayerBankname("");
        DataManager.getInstance().setReceivingMethod("");
//        if (!receiverbranchModel.getReceivingCurrencyIsoCode().equalsIgnoreCase("")) {
//            DataManager.getInstance().transactionModel.setReceiver_currency(receiverbranchModel.getReceivingCurrencyIsoCode());
//        }else
//        {
        DataManager.getInstance().transactionModel.setReceiver_currency("");
//        }
        ReceivingMethod receivingMethod = new ReceivingMethod();
        MyApplication.getApplication().setReceivingMethod(receivingMethod);
        MyApplication.getApplication().setSending_method("");
    }
    private class MyBrowser extends WebViewClient {
        ProgressDialog dialog;
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            String [] call_back_url = url.split("php?");

            System.out.println("urlllllllllllllllllllllllll" + call_back_url[0]);
            if (!call_back_url[0].equalsIgnoreCase("http://www.aftabcurrency.com/order_st_confirm."))
            {
                dialog =ProgressDialog.show(CardSelectActivity.this,"","Please wait...");


            }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (dialog!=null) {
                dialog.dismiss();
            }

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            String [] call_back_url = url.split("php?");

            System.out.println("urlllllllllllllllllllllllll" + call_back_url[0]);
            if (call_back_url[0].equalsIgnoreCase("http://www.aftabcurrency.com/order_st_confirm."))
            {
                ((TextView) logo.findViewById(R.id.txt)).setText("Payment Successfully");
                call_back_urlBoolean =true;


            }
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        if (call_back_urlBoolean)
        {
            Intent i = new Intent(CardSelectActivity.this,MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }else {
            if (webview.canGoBack()) {
                webview.goBack();
            } else {
                backPressAlert();

            }
        }
    }
    private void backPressAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CardSelectActivity.this);
        builder.setTitle("" + getResources().getString(R.string.app_name));
        builder.setMessage("Are you sure you want to go back. Your transaction will remain incomplete")
                .setCancelable(false)
                .setPositiveButton("Yes",

                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                Intent i = new Intent(CardSelectActivity.this,MainActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }
}
