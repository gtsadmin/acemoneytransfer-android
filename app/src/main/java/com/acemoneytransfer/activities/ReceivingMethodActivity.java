package com.acemoneytransfer.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.model.PayerBranchModel;
import com.acemoneytransfer.model.ReceivingMethod;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ReceivingMethodActivity extends AppCompatActivity {


    @Bind(R.id.bankSpinner)
    Spinner bankSpinner;
    @Bind(R.id.txt)
    TextViewAvenirLTStdBook txt;
    @Bind(R.id.nationalitySpinner)
    Spinner nationalitySpinner;
    @Bind(R.id.txt2)
    TextViewAvenirLTStdBook txt2;
    @Bind(R.id.selectPaymntMethodSpinner)
    Spinner selectPaymntMethodSpinner;
    @Bind(R.id.txt3)
    TextViewAvenirLTStdBook txt3;
    @Bind(R.id.selectCompanySpinner)
    Spinner selectCompanySpinner;

    @Bind(R.id.selectBranchSpinner)
    Spinner selectBranchSpinner;
    @Bind(R.id.doneTxt)
    ImageButton doneTxt;
    @Bind(R.id.bankbranchTxt)
    TextViewAvenirLTStdBook bankbranchTxt;

    private Toolbar toolbar;

    ArrayList<String> isoList = new ArrayList<>();

    String select_payment_method = "", receiver_currency = "";
    HashMap<String, String> country_name = new HashMap<>();
    HashMap<String, String> ios_code = new HashMap<>();

    private String bankArr[] = new String[]{"Bank", "Case"};
    String iso_code_country = "", payerid_str = "";
    PayerBranchModel receiverbranchModel;

    ArrayList<String> paymentMethodArr = new ArrayList<>();
    ReceivingMethod receivingMethod = new ReceivingMethod();
    ArrayList<String> countryPayerList = new ArrayList<>();
    ArrayList<String> payerBranchList = new ArrayList<>();
    ArrayList<PayerBranchModel> branchModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_receiving_method);
        ButterKnife.bind(this);
        paymentMethodArr.add("Select payment method");
        paymentMethodArr.add("Bank");
        paymentMethodArr.add("Cash");
        payerBranchList.add("Select branch");
        countryPayerList.add("Select bank");
        setAdapter();
        toolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(toolbar);
//        setData();

        // cat_price = getIntent().getExtras().getString("cat_price");
        toolbar.setNavigationIcon(R.mipmap.icn_close);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Receiving Method");


        setNationalityArr();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

    }

    @OnClick(R.id.doneTxt)
    public void doneTxt() {
        try {
//            DataManager.getInstance().transactionModel.setReceiverBankBranchCode(receiverbranchModel.getBranchCode());
//            DataManager.getInstance().transactionModel.setReceiverBankCode(payerid_str);
//            DataManager.getInstance().transactionModel.setReceiverBankName(selectCompanySpinner.getSelectedItem().toString());
//            DataManager.getInstance().transactionModel.setReceiverBranchName(receiverbranchModel.getBranchName());
            if (nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                Toast.makeText(ReceivingMethodActivity.this, "Please select country", Toast.LENGTH_SHORT).show();
                Constants.RECEIVINGMETHOD = false;
            } else if (selectPaymntMethodSpinner.getSelectedItem().toString().equalsIgnoreCase("Select payment method")) {
                Toast.makeText(ReceivingMethodActivity.this, "Please select payment method", Toast.LENGTH_SHORT).show();
                Constants.RECEIVINGMETHOD = false;
            } else if (selectCompanySpinner.getSelectedItem().toString().equalsIgnoreCase("Select bank")) {
                Toast.makeText(ReceivingMethodActivity.this, "Please select bank", Toast.LENGTH_SHORT).show();
                Constants.RECEIVINGMETHOD = false;

            } else if (selectBranchSpinner.getSelectedItem().toString().equalsIgnoreCase("Select branch")) {
                Toast.makeText(ReceivingMethodActivity.this, "Please select branch", Toast.LENGTH_SHORT).show();
                Constants.RECEIVINGMETHOD = false;

            } else {
                receivingMethod.setSelect_payment_method(select_payment_method);
                DataManager.getInstance().transactionModel.setPayment_method(select_payment_method);
                DataManager.getInstance().transactionModel.setStation(receiverbranchModel.getBranhCity());
                DataManager.getInstance().transactionModel.setStation_Address(receiverbranchModel.getBranchAddress());
                DataManager.getInstance().transactionModel.setPayerBranchcode(receiverbranchModel.getBranchCode());
                DataManager.getInstance().transactionModel.setPayerBranchname(selectBranchSpinner.getSelectedItem().toString());
                DataManager.getInstance().transactionModel.setPayerBankname(selectCompanySpinner.getSelectedItem().toString());
                DataManager.getInstance().setReceivingMethod(selectPaymntMethodSpinner.getSelectedItem().toString());
//        if (!receiverbranchModel.getReceivingCurrencyIsoCode().equalsIgnoreCase("")) {
//            DataManager.getInstance().transactionModel.setReceiver_currency(receiverbranchModel.getReceivingCurrencyIsoCode());
//        }else
//        {
                DataManager.getInstance().transactionModel.setReceiver_currency(receiver_currency);
//        }
                MyApplication.getApplication().setReceivingMethod(receivingMethod);

                Constants.RECEIVINGMETHOD = true;

                overridePendingTransition(R.anim.back_in, R.anim.back_out);
                finish();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    public void setNationalityArr() {
        String selectbankstr[] = {"Select bank"};
        String selectbranchstr[] = {"Select branch"};
//        selectPaymntMethodSpinner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country"))
//                {
//                    Toast.makeText(ReceivingMethodActivity.this,"Please select country",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
        ArrayAdapter<String> paymentArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, paymentMethodArr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        paymentArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        selectPaymntMethodSpinner.setAdapter(paymentArrayAdapter);
        selectPaymntMethodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                                @Override
                                                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                                    try {
                                                                        if (!iso_code_country.equalsIgnoreCase("")) {
                                                                            if (!selectPaymntMethodSpinner.getSelectedItem().toString().equalsIgnoreCase("Select payment method")) {
//                        getCountryBankList(iso_code_country);

                                                                                select_payment_method = selectPaymntMethodSpinner.getSelectedItem().toString();

                                                                                getCountryPayerList(iso_code_country);
                                                                            }

                                                                        }
                                                                    } catch (Exception ex) {
                                                                        ex.printStackTrace();
                                                                    }


                                                                }

                                                                @Override
                                                                public void onNothingSelected(AdapterView<?> parent) {

                                                                }
                                                            }

        );

        ArrayAdapter<String> bankSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, selectbankstr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        bankSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        selectCompanySpinner.setAdapter(bankSpinnerArrayAdapter);
        ArrayAdapter<String> branchSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, selectbranchstr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        bankSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        selectBranchSpinner.setAdapter(branchSpinnerArrayAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country") || selectPaymntMethodSpinner.getSelectedItem().toString().equalsIgnoreCase("Select payment method") || selectCompanySpinner.getSelectedItem().toString().equalsIgnoreCase("Select bank") || selectBranchSpinner.getSelectedItem().toString().equalsIgnoreCase("Select branch")) {
            Constants.RECEIVINGMETHOD = false;
        }
        if (Constants.RECEIVINGMETHOD == false) {
            ReceivingMethod model = new ReceivingMethod();
            MyApplication.getApplication().setReceivingMethod(model);
        }
        overridePendingTransition(R.anim.back_in, R.anim.back_out);
        finish();

    }

    private void getCountrylist() {
        final HashMap<String, String> currency_code = new HashMap<>();
        final ArrayList<String> countryList = new ArrayList<>();
        MyApplication.getApplication().showProgressDialog(ReceivingMethodActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(ReceivingMethodActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetReceivingCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.getApplication().hideProgressDialog();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceReceivingCountryList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryList.add(sub_obj.getString("CountryName"));
                                    ios_code.put(sub_obj.getString("CountryName"), sub_obj.getString("Iso3Code"));
                                    currency_code.put(sub_obj.getString("CountryName"), sub_obj.getString("CurrencyIsoCode"));
                                }
                                Collections.reverse(countryList);
                                countryList.add("Select country");
                                Collections.reverse(countryList);
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(ReceivingMethodActivity.this, R.layout.spinner_layout, countryList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

                                nationalitySpinner.setAdapter(spinnerArrayAdapter);
                                MyApplication.getApplication().setCountryList(countryList);
                                MyApplication.getApplication().setIos_code(ios_code);
                                DataManager.getInstance().setCurrency_code(currency_code);
//                                if (!Constants.current_country.equalsIgnoreCase("")) {
//                                    nationalitySpinner.setSelection(countryList.indexOf(Constants.current_country));
//                                }
                                nationalitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        try {
                                            if (!nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                                                selectCompanySpinner.setSelection(0);
                                                selectPaymntMethodSpinner.setSelection(0);
                                                selectBranchSpinner.setSelection(0);
                                                iso_code_country = ios_code.get(nationalitySpinner.getSelectedItem().toString()).toString();
                                                receivingMethod.setISO_Code(iso_code_country);
                                                receiver_currency = currency_code.get(nationalitySpinner.getSelectedItem().toString()).toString();
                                                String country_name = nationalitySpinner.getSelectedItem().toString();
                                                receivingMethod.setCountry_name(country_name);
                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, ReceivingMethodActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MyApplication.getApplication().hideProgressDialog();
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getCountryPayerList(String CountryIso3Code) {
        countryPayerList = new ArrayList<>();
        final HashMap<String, String> payerids_map = new HashMap<>();
//        MyApplication.getApplication().showProgressDialog(ReceivingMethodActivity.this);
//        MyApplication.getApplication().hideSoftKeyBoard(ReceivingMethodActivity.this);
        final ProgressDialog dialog = ProgressDialog.show(ReceivingMethodActivity.this, "", "Please wait...");


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("ReceivingCountryIso3Code", CountryIso3Code);
            object.put("ReceivingPaymentMethod", select_payment_method);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(ReceivingMethodActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetCountryPayerList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {


                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            countryPayerList.clear();
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AcePayerList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryPayerList.add(sub_obj.getString("PayerName"));
                                    payerids_map.put(sub_obj.getString("PayerName"), sub_obj.getString("PayerId"));
                                }
                                if (aceCountryJsonArray.length() == 0) {
                                    Toast.makeText(ReceivingMethodActivity.this, "No banks", Toast.LENGTH_SHORT).show();
                                }
                                countryPayerList.add("Select bank");
                                Collections.reverse(countryPayerList);
                                ArrayAdapter<String> companyArrayAdapter = new ArrayAdapter<String>(ReceivingMethodActivity.this, R.layout.spinner_layout, countryPayerList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                companyArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                selectCompanySpinner.setAdapter(companyArrayAdapter);
                                selectCompanySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        try {
                                            if (!selectCompanySpinner.getSelectedItem().toString().equalsIgnoreCase("Select bank")) {
                                                payerid_str = payerids_map.get(selectCompanySpinner.getSelectedItem().toString()).toString();
                                                receivingMethod.setPayerId(payerid_str);
                                                receivingMethod.setPayerName(selectCompanySpinner.getSelectedItem().toString());

                                                getPayerBranchList(payerid_str, iso_code_country);

                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                                if (!receivingMethod.getPayerName().equalsIgnoreCase(""))
                                    selectCompanySpinner.setSelection(countryPayerList.indexOf(receivingMethod.getPayerName()));
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, ReceivingMethodActivity.this);
                            }


                        } catch (Exception e) {
                            MyApplication.getApplication().hideProgressDialog();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getPayerBranchList(String PayerID, String CountryIso3Code) {
//        MyApplication.getApplication().showProgressDialog(ReceivingMethodActivity.this);
        final ProgressDialog dialog = ProgressDialog.show(ReceivingMethodActivity.this, "", "Please wait...");
        payerBranchList = new ArrayList<>();
        branchModels = new ArrayList<>();

        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("ReceivingCountryIso3Code", CountryIso3Code);
            object.put("PayerID", PayerID);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(ReceivingMethodActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetPayerBranchList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            System.out.println("response>>> " + response.toString());
                            payerBranchList.clear();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AcePayerBranchList")) {
                                    JSONArray aceCountryJsonArray = response.getJSONArray("AcePayerBranchList");
                                    for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                        PayerBranchModel branchModel = new PayerBranchModel();
                                        JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                        if (sub_obj.has("BranchName")) {
                                            payerBranchList.add(sub_obj.getString("BranchName"));
                                            branchModel.setBranchName(sub_obj.getString("BranchName"));
                                        }
                                        if (sub_obj.has("BranchAddress")) {
                                            branchModel.setBranchAddress(sub_obj.getString("BranchAddress"));
                                        }
                                        if (sub_obj.has("BranchCode")) {
                                            branchModel.setBranchCode(sub_obj.getString("BranchCode"));
                                        }
                                        if (sub_obj.has("BranchPayCode")) {
                                            branchModel.setBranchPayCode(sub_obj.getString("BranchPayCode"));
                                        }
                                        if (sub_obj.has("BranhCity")) {
                                            branchModel.setBranhCity(sub_obj.getString("BranhCity"));
                                        }
                                        if (sub_obj.has("ReceivingCountryIsoCode")) {
                                            branchModel.setReceivingCountryIsoCode(sub_obj.getString("ReceivingCountryIsoCode"));
                                        }
                                        if (sub_obj.has("ReceivingCurrencyIsoCode")) {
                                            branchModel.setReceivingCurrencyIsoCode(sub_obj.getString("ReceivingCurrencyIsoCode"));
                                        }
                                        branchModels.add(branchModel);
                                    }
                                    if (aceCountryJsonArray.length() == 0) {
                                        Toast.makeText(ReceivingMethodActivity.this, "No branches", Toast.LENGTH_SHORT).show();
                                    }
                                    if (aceCountryJsonArray.length() > 0) {
                                        bankbranchTxt.setText("Select Branch");
                                    } else {
                                        bankbranchTxt.setText("No branches");
                                    }
                                    payerBranchList.add("Select branch");
                                    Collections.reverse(payerBranchList);
                                    ArrayAdapter<String> branchArrayAdapter = new ArrayAdapter<String>(ReceivingMethodActivity.this, R.layout.spinner_layout, payerBranchList) {
                                        public View getView(int position, View convertView,
                                                            ViewGroup parent) {
                                            View v = super.getView(position, convertView, parent);


                                            if (position == 0) {

                                                ((TextView) v).setTextColor(getResources()
                                                        .getColorStateList(R.color.gray));
                                            } else {

                                                ((TextView) v).setTextColor(getResources()
                                                        .getColorStateList(R.color.black));
                                            }
                                            return v;
                                        }

                                        public View getDropDownView(int position, View convertView,
                                                                    ViewGroup parent) {
                                            View v = super.getDropDownView(position, convertView,
                                                    parent);


                                            ((TextView) v).setTextColor(Color.BLACK);


                                            return v;
                                        }
                                    }; //selected item will look like a spinner set from XML
                                    branchArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                    MyApplication.getApplication().setPayerBranchList(payerBranchList);
                                    selectBranchSpinner.setAdapter(branchArrayAdapter);
                                    selectBranchSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            try {
                                                if (!selectBranchSpinner.getSelectedItem().toString().equalsIgnoreCase("Select branch")) {
                                                    receivingMethod.setPayer_branch_name(selectBranchSpinner.getSelectedItem().toString());

                                                    receiverbranchModel = branchModels.get(payerBranchList.indexOf(selectBranchSpinner.getSelectedItem().toString()) - 1);
                                                }
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });
                                    if (!receivingMethod.getPayer_branch_name().equalsIgnoreCase("")) {
                                        selectBranchSpinner.setSelection(MyApplication.getApplication().getPayerBranchList().indexOf(receivingMethod.getPayer_branch_name()));
                                    }
                                }
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, ReceivingMethodActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
//                        MyApplication.getApplication().hideProgressDialog();
                        dialog.dismiss();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (receivingMethod != null) {
            receivingMethod = MyApplication.getApplication().getReceivingMethod();
        }
        if (MyApplication.isInternetWorking(ReceivingMethodActivity.this)) {
            callApi();
        }

//        selectPaymntMethodSpinner.setSelection(paymentMethodArr.indexOf(receivingMethod.getSelect_payment_method()));
//        nationalitySpinner.setSelection();
    }

    public void callApi() {
        if (MyApplication.getApplication().getCountryList().size() == 0) {
            getCountrylist();
        } else {
            if (receivingMethod != null) {
                selectPaymntMethodSpinner.setSelection(paymentMethodArr.indexOf(receivingMethod.getSelect_payment_method()));
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(ReceivingMethodActivity.this, R.layout.spinner_layout, MyApplication.getApplication().getCountryList()) {
                public View getView(int position, View convertView,
                                    ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);


                    if (position == 0) {

                        ((TextView) v).setTextColor(getResources()
                                .getColorStateList(R.color.gray));
                    } else {

                        ((TextView) v).setTextColor(getResources()
                                .getColorStateList(R.color.black));
                    }
                    return v;
                }

                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView,
                            parent);


                    ((TextView) v).setTextColor(Color.BLACK);


                    return v;
                }
            }; //selected item will look like a spinner set from XML
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

            nationalitySpinner.setAdapter(spinnerArrayAdapter);
            if (receivingMethod != null) {
                nationalitySpinner.setSelection(MyApplication.getApplication().getCountryList().indexOf(receivingMethod.getCountry_name()));
            }
            nationalitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                        try {
                            selectCompanySpinner.setSelection(0);
                            selectPaymntMethodSpinner.setSelection(0);
                            selectBranchSpinner.setSelection(0);
                            iso_code_country = MyApplication.getApplication().getIos_code().get(nationalitySpinner.getSelectedItem().toString()).toString();
                            receivingMethod.setISO_Code(iso_code_country);
                            receiver_currency = DataManager.getInstance().getCurrency_code().get(nationalitySpinner.getSelectedItem().toString()).toString();
                            getCountryPayerList(iso_code_country);
                            String country_name = nationalitySpinner.getSelectedItem().toString();
                            receivingMethod.setCountry_name(country_name);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }

    public void setAdapter() {
        ArrayAdapter<String> companyArrayAdapter = new ArrayAdapter<String>(ReceivingMethodActivity.this, R.layout.spinner_layout, countryPayerList) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        companyArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        selectCompanySpinner.setAdapter(companyArrayAdapter);
        ArrayAdapter<String> branchArrayAdapter = new ArrayAdapter<String>(ReceivingMethodActivity.this, R.layout.spinner_layout, payerBranchList) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        branchArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        MyApplication.getApplication().setPayerBranchList(payerBranchList);
        selectBranchSpinner.setAdapter(branchArrayAdapter);
    }
}
