package com.acemoneytransfer.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.ImageButton;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.fragments.TutorialsFristFragment;
import com.acemoneytransfer.fragments.TutorialsSecondFragment;
import com.acemoneytransfer.fragments.TutorialsThirdFragment;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TutorialsActivity extends FragmentActivity {
    public static ViewPager viewPager;
    ProfilePagerAdapter adapter;

    @Bind(R.id.titles_pageIndicator)
    CirclePageIndicator titlesPageIndicator;
    @Bind(R.id.skipNowIMgBtn)
    ImageButton skipNowIMgBtn;
    Toolbar toolbar;
    private static final int NUM_PAGES = 3;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorials_layout);
        ButterKnife.bind(this);
        viewPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mPagerAdapter);
        MyApplication.savetutorialActivity(true);

//        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);
//        toolbar.setNavigationIcon(R.mipmap.icn_back);
//        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
//        toolbar.addView(logo);
//        getSupportActionBar().setTitle("");
//        ((TextView) logo.findViewById(R.id.txt)).setText("Tutorial");
//
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                onBackPressed();
//            }
//        });
//        getUIComponent();
//        if (!TextUtils.isEmpty(MyApplication.getAuthToken("AuthToken"))) {
//            toolbar.setVisibility(View.VISIBLE);
//
//        }else
//        {
//            toolbar.setVisibility(View.GONE);
//        }
    }

    @OnClick(R.id.skipNowIMgBtn)
    public void skipNowIMgBtn() {
        if (!TextUtils.isEmpty(MyApplication.getAuthToken("AuthToken"))) {
            finish();

        }else
        {
            startActivity(new Intent(TutorialsActivity.this, LoginActivity.class).putExtra("login", 1));
            TutorialsActivity.this.overridePendingTransition(R.anim.left, R.anim.right);
            finish();
        }


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }
    private void getUIComponent() {


        ArrayList<Integer> imageInfosList = new ArrayList<>();
        imageInfosList.add(R.drawable.tut1);
        imageInfosList.add(R.drawable.tut2);
        imageInfosList.add(R.drawable.tut3);
        imageInfosList.add(R.drawable.tut4);
        imageInfosList.add(R.drawable.tut5);
        imageInfosList.add(R.drawable.tut6);
        imageInfosList.add(R.drawable.tut7);
        imageInfosList.add(R.drawable.tut8);
        imageInfosList.add(R.drawable.tut9);

        // Pass results to ViewPagerAdapter Class
        adapter = new ProfilePagerAdapter(TutorialsActivity.this, imageInfosList);
        // Binds the Adapter to the ViewPager
        viewPager.setAdapter(adapter);
        CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.titles_pageIndicator);
        pageIndicator.setViewPager(viewPager);



    }

    static void makeToast(Context ctx, String s) {
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position==0)
            {
                fragment = new TutorialsFristFragment();
            }else if (position==1)
            {
                fragment = new TutorialsSecondFragment();
            }else
            {
                fragment = new TutorialsThirdFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

}

}
