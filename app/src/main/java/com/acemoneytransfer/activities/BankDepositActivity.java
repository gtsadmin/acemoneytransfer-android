package com.acemoneytransfer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.model.ReceivingMethod;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.acemoneytransfer.views.TextViewAvenirLTStdLight;
import com.acemoneytransfer.views.TextViewMontserratBold;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class BankDepositActivity extends AppCompatActivity {
    @Bind(R.id.doneTxt)
    TextViewAvenirLTStdBook doneTxt;
    @Bind(R.id.paymentnumberTxt)
    TextViewMontserratBold paymentnumberTxt;
    String paymentId;
    @Bind(R.id.greececurrency)
    TextViewAvenirLTStdLight greececurrency;
    @Bind(R.id.greeceaccount)
    TextViewAvenirLTStdLight greeceaccount;
    @Bind(R.id.greeceibn)
    TextViewAvenirLTStdLight greeceibn;
    @Bind(R.id.Swedencurrency)
    TextViewAvenirLTStdLight Swedencurrency;
    @Bind(R.id.franceaccounttitle)
    TextViewAvenirLTStdLight franceaccounttitle;
    @Bind(R.id.GreeceLayout)
    LinearLayout GreeceLayout;
    @Bind(R.id.SwedenLayout)
    LinearLayout SwedenLayout;
    @Bind(R.id.UnitedKingdomLayout)
    LinearLayout UnitedKingdomLayout;
    @Bind(R.id.FranceLayout)
    LinearLayout FranceLayout;
    @Bind(R.id.ItalyLayout)
    LinearLayout ItalyLayout;
    @Bind(R.id.SpainLayout)
    LinearLayout SpainLayout;
    @Bind(R.id.NetherlandsLayout)
    LinearLayout NetherlandsLayout;
    @Bind(R.id.NorwayLayout)
    LinearLayout NorwayLayout;
    @Bind(R.id.DenmarkLayout)
    LinearLayout DenmarkLayout;
    @Bind(R.id.PortugalLayout)
    LinearLayout PortugalLayout;
    HashMap<String,String> country_name = new HashMap<>();
    String countryNamestr=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_deposit_layout);
        ButterKnife.bind(this);
        DataManager.getInstance().transactionModel.setPayment_method("");
        DataManager.getInstance().transactionModel.setStation("");
        DataManager.getInstance().transactionModel.setStation_Address("");
        DataManager.getInstance().transactionModel.setPayerBranchcode("");
        DataManager.getInstance().transactionModel.setPayerBranchname("");
        DataManager.getInstance().transactionModel.setPayerBankname("");
        DataManager.getInstance().setReceivingMethod("");
        MyApplication.getApplication().setSending_method("");
        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(BankDepositActivity.this);
        dbHelper.openDataBase();
        ArrayList<String> countryList = dbHelper.getCountryList(SqlLiteDbHelper.NAME);
        ArrayList<String> isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);

        for (int i = 0; i < countryList.size(); i++) {
            country_name.put(isoList.get(i),countryList.get(i));

        }
        dbHelper.close();
        countryNamestr =country_name.get(DataManager.getInstance().getUserInfoWrapper().getCountryIsoCode());
//        if (!receiverbranchModel.getReceivingCurrencyIsoCode().equalsIgnoreCase("")) {
//            DataManager.getInstance().transactionModel.setReceiver_currency(receiverbranchModel.getReceivingCurrencyIsoCode());
//        }else
//        {
        ReceivingMethod receivingMethod = new ReceivingMethod();
        DataManager.getInstance().transactionModel.setReceiver_currency("");
//        }
        MyApplication.getApplication().setReceivingMethod(receivingMethod);
        paymentId = getIntent().getExtras().getString("PaymentID");
        paymentnumberTxt.setText("Deposit Reference #: " + paymentId);
        greececurrency.setText("Currency : EUR");
        greeceaccount.setText("Account Number : 61447097132");
        greeceibn.setText("IBAN : GR46 0110 6140 0000 6144 7097 132");
        Swedencurrency.setText("Currency : SEK");
        franceaccounttitle.setText("A/C TITLE : AFTAB CURRENCY EXCHANGE rives de paris agence gonesse 1,avenue georges pompidou centre commercial la grande vallee 95500 gonesse");
        HashMap<String,String> countymap = new HashMap<>();
        countymap.put("Greece", "Greece");
        countymap.put("Sweden", "Sweden");
        countymap.put("United Kingdom","United Kingdom");
        countymap.put("France", "France");
        countymap.put("Italy", "Italy");
        countymap.put("Spain","Spain");
        countymap.put("Netherlands", "Netherlands");
        countymap.put("Norway", "Norway");
        countymap.put("Denmark", "Denmark");
        countymap.put("Portugal", "Portugal");
//        String countryNamestr = DataManager.getInstance().getCountryNamestr();
        if (countryNamestr.equalsIgnoreCase("Greece"))
        {
            GreeceLayout.setVisibility(View.VISIBLE);

        }else if (countryNamestr.equalsIgnoreCase("Sweden"))
        {
            SwedenLayout.setVisibility(View.VISIBLE);
        }else if (countryNamestr.equalsIgnoreCase("United Kingdom"))
        {
            UnitedKingdomLayout.setVisibility(View.VISIBLE);
        }else if (countryNamestr.equalsIgnoreCase("France"))
        {
            FranceLayout.setVisibility(View.VISIBLE);
        }else if (countryNamestr.equalsIgnoreCase("Italy"))
        {
            ItalyLayout.setVisibility(View.VISIBLE);
        }else if (countryNamestr.equalsIgnoreCase("Spain"))
        {
            SpainLayout.setVisibility(View.VISIBLE);
        }

        else if (countryNamestr.equalsIgnoreCase("Netherlands"))
        {
            NetherlandsLayout.setVisibility(View.VISIBLE);
        }else if (countryNamestr.equalsIgnoreCase("Norway"))
        {
            NorwayLayout.setVisibility(View.VISIBLE);
        }else if (countryNamestr.equalsIgnoreCase("Denmark"))
        {
            DenmarkLayout.setVisibility(View.VISIBLE);
        }else if (countryNamestr.equalsIgnoreCase("Portugal"))

        {
            PortugalLayout.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.doneTxt)
    public void doneTxt() {
        Intent i = new Intent(BankDepositActivity.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {

    }
}
