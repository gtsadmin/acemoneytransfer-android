package com.acemoneytransfer.activities;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;

import java.util.ArrayList;


public class ProfilePagerAdapter extends PagerAdapter {
    // Declare Variables
    Context context;
    ArrayList<Integer> flag;
    LayoutInflater inflater;

    // UI image loader

    public ProfilePagerAdapter(Context context,
                               ArrayList<Integer> imageInfosList) {
        this.context = context;
        this.flag = imageInfosList;

    }

    @Override
    public int getCount() {
        return flag.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables
        // TextView txtpopulation;
        ImageView imgflag;
        final Button btnClose, btnLIke;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.model_pager_profile,
                container, false);

        // Locate the ImageView in viewpager_item.xml
        imgflag = (ImageView) itemView.findViewById(R.id.image);
        // Capture position and set to the ImageView
        // imgflag.setImageResource(flag[position]);
        imgflag.setImageResource(0);
        imgflag.setImageResource(android.R.color.transparent);
//        imgflag.setImageResource(flag.get(position));


        MyApplication.getApplication().loader.displayImage("drawable://" + flag.get(position), imgflag, MyApplication.options);
        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((LinearLayout) object);

    }
}
