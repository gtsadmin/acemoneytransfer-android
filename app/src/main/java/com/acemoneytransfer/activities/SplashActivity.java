package com.acemoneytransfer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.crittercism.app.Crittercism;


public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        Crittercism.initialize(getApplicationContext(), "5683644e6c33dc0f00f11513");
        new CountDownTimer(2000,2000)
        {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (!TextUtils.isEmpty(MyApplication.getAuthToken("AuthToken"))) {
                    startActivity(new Intent(SplashActivity.this,MainActivity.class));
                    SplashActivity.this.overridePendingTransition(R.anim.left,
                            R.anim.right);
                    finish();
                }else
                {
                    if (!MyApplication.getTutarialActivity()) {
                        startActivity(new Intent(SplashActivity.this, TutorialsActivity.class));
                        SplashActivity.this.overridePendingTransition(R.anim.left,
                                R.anim.right);
                        finish();
                    }else
                    {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class).putExtra("login", 1));
                        SplashActivity.this.overridePendingTransition(R.anim.left,
                                R.anim.right);
                        finish();
                    }

                }

            }
        }.start();
    }
}
