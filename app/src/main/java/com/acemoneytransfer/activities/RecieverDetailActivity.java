package com.acemoneytransfer.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.datacontroller.DataParser;
import com.acemoneytransfer.model.BankBranchModel;
import com.acemoneytransfer.model.ReceiverModel;
import com.acemoneytransfer.views.EditTextAvenirLTStdBook;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RecieverDetailActivity extends AppCompatActivity {
    @Bind(R.id.amountEdt)
    TextViewAvenirLTStdBook amountEdt;
    @Bind(R.id.firstNameEdt)
    EditTextAvenirLTStdBook firstNameEdt;
    @Bind(R.id.lastnameEdt)
    EditTextAvenirLTStdBook lastnameEdt;
    @Bind(R.id.contactEdt)
    EditTextAvenirLTStdBook contactEdt;
    @Bind(R.id.nationalitySpinner)
    Spinner nationalitySpinner;
    @Bind(R.id.cityTxt)
    EditTextAvenirLTStdBook cityTxt;
    @Bind(R.id.postalCodeTxt)
    EditTextAvenirLTStdBook postalCodeTxt;
    @Bind(R.id.addressEdt)
    EditTextAvenirLTStdBook addressEdt;
    @Bind(R.id.relationSpinner)
    Spinner relationSpinner;
    @Bind(R.id.sendingPurposeSpinner)
    Spinner sendingPurposeSpinner;
    @Bind(R.id.proceedTxt)
    TextViewAvenirLTStdBook proceedTxt;
    @Bind(R.id.cancelTxt)
    TextViewAvenirLTStdBook cancelTxt;
    @Bind(R.id.bottomLayout)
    LinearLayout bottomLayout;
    @Bind(R.id.ibanEdt)
    EditTextAvenirLTStdBook ibanEdt;

    @Bind(R.id.backnameSpinner)
    Spinner backnameSpinner;
    @Bind(R.id.branchnameSpinner)
    Spinner branchnameSpinner;
    @Bind(R.id.accountTxt)
    EditTextAvenirLTStdBook accountTxt;
    @Bind(R.id.bankbranchTxt)
    TextViewAvenirLTStdBook bankbranchTxt;
    @Bind(R.id.bankbranchselectLayout)
    LinearLayout bankbranchselectLayout;
    @Bind(R.id.bankbranchnameEdt)
    EditTextAvenirLTStdBook bankbranchnameEdt;
    @Bind(R.id.bankbranchnameLayout)
    LinearLayout bankbranchnameLayout;
    @Bind(R.id.bankbranchcodeEdt)
    EditTextAvenirLTStdBook bankbranchcodeEdt;
    @Bind(R.id.bankbranchcodeLayout)
    LinearLayout bankbranchcodeLayout;
    @Bind(R.id.accountNoTxt)
    TextViewAvenirLTStdBook accountNoTxt;
    @Bind(R.id.banknameTxt)
    TextViewAvenirLTStdBook banknameTxt;
    @Bind(R.id.bankbranchnameTxt)
    TextViewAvenirLTStdBook bankbranchnameTxt;
    @Bind(R.id.bankbranchcodeTxt)
    TextViewAvenirLTStdBook bankbranchcodeTxt;

    private Toolbar toolbar;

    private String relationArr[] = new String[]{"Select relation", "Father", "Mother", "Husband", "Wife", "Son", "Daughter", "Cousin", "Brother", "Sister", "Uncle", "Aunt", "Friend", "Niece", "Nephew", "Grand Father", "Grand Mother", "Grand Son", "Father in Law", "Father in Law", "Mother in Law" +
            "Brother in Law", "Sister in Law", "Fiancee", "Business Associate", "Employer", "Self", "Not Applicable"};
    HashMap<String, String> branchnamemap = new HashMap<>();
    String branchName = "", branchCode = "";

    private String sendPurposeArr[] = new String[]{"Select send purpose", "Faimly Support", "Loan", "Charity", "Buying Property", "Construction", "Business", "Helping Friend", "Medical Treatment", "Educational Fee", "Others"};

    ArrayList<String> countryArr = new ArrayList<>();
    ArrayList<String> isoList = new ArrayList<>();
    String iso_code_str = "";
    HashMap<String, String> iso_country_code = new HashMap<>();
    HashMap<String, String> country_name_map = new HashMap<>();
    ArrayList<String> countryList = new ArrayList<>();

    HashMap<String, String> country_name = new HashMap<>();
    HashMap<String, String> ios_code = new HashMap<>();

    String iso_code_country = "";
    ReceiverModel receiverModel;
    BankBranchModel bankBranchModel = null;
    String bank_code = "";
    boolean getbranch_status = true;
    boolean getbank_status = true;
    String bank_nameStr = "", branch_nameStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_reciever_detail);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setNationalityArr();
        setData();
        getCountrylist();

        setSupportActionBar(toolbar);


        // cat_price = getIntent().getExtras().getString("cat_price");
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Receiver Details");
//        getSupportActionBar().setTitle("Reciever Details");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
        firstNameEdt.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z ]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });
        cityTxt.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z ]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });
        if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Bank")) {
            accountNoTxt.setText("Account No *");
            banknameTxt.setText("Bank Name *");
            bankbranchnameTxt.setText("Bank Branch *");
            bankbranchcodeTxt.setText("Bank Branch Code *");
            bankbranchnameTxt.setText("Bank Branch Name *");


        }

    }

    public void setCountryData() {
        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(RecieverDetailActivity.this);
        dbHelper.openDataBase();
        countryArr = dbHelper.getCountryList(SqlLiteDbHelper.NAME);

        isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
        for (int i = 0; i < countryArr.size(); i++) {
            iso_country_code.put(countryArr.get(i), isoList.get(i));
            country_name_map.put(isoList.get(i), countryArr.get(i));
        }
    }

    @OnClick(R.id.proceedTxt)
    public void proceedTxt() {
        try {
            if (relationSpinner.getSelectedItem().toString().equalsIgnoreCase("Select relation")) {
                Toast.makeText(RecieverDetailActivity.this, "Please select relation", Toast.LENGTH_SHORT).show();
            } else if (sendingPurposeSpinner.getSelectedItem().toString().equalsIgnoreCase("Select send purpose")) {
                Toast.makeText(RecieverDetailActivity.this, "Please select sending purpose", Toast.LENGTH_SHORT).show();
            } else if (nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                Toast.makeText(RecieverDetailActivity.this, "Please select country", Toast.LENGTH_SHORT).show();
            } else if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Bank")) {
                if (backnameSpinner.getSelectedItem().toString().equalsIgnoreCase("Select bank")) {
                    Toast.makeText(RecieverDetailActivity.this, "Please select bank", Toast.LENGTH_SHORT).show();
                } else {
                    if (bankBranchModel == null) {
                        if (TextUtils.isEmpty(bankbranchnameEdt.getText().toString())) {
                            Toast.makeText(RecieverDetailActivity.this, "Please enter branch name", Toast.LENGTH_SHORT).show();
                        } else if (TextUtils.isEmpty(bankbranchcodeEdt.getText().toString())) {
                            Toast.makeText(RecieverDetailActivity.this, "Please enter branch code", Toast.LENGTH_SHORT).show();

                        }else
                        {
                            sendNextActivity();
                        }
                    } else {
                        if (branchnameSpinner.getSelectedItem().toString().equalsIgnoreCase("Select branch")) {
                            Toast.makeText(RecieverDetailActivity.this, "Please select branch name", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            sendNextActivity();
                        }
                    }
                }
            }else
            {
                sendNextActivity();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void sendNextActivity() {

            DataManager.getInstance().transactionModel.setReceiver_name(firstNameEdt.getText().toString());
            DataManager.getInstance().transactionModel.setReceiver_lastname(lastnameEdt.getText().toString());
            DataManager.getInstance().transactionModel.setReceiver_contact(contactEdt.getText().toString());
            DataManager.getInstance().transactionModel.setReceiver_country(nationalitySpinner.getSelectedItem().toString());
            DataManager.getInstance().transactionModel.setReceiver_city(cityTxt.getText().toString());
            DataManager.getInstance().transactionModel.setReceiver_postalcode(postalCodeTxt.getText().toString());
            DataManager.getInstance().transactionModel.setReceiver_address(addressEdt.getText().toString());
            DataManager.getInstance().transactionModel.setRelationship(relationSpinner.getSelectedItem().toString());
            DataManager.getInstance().transactionModel.setSending_purpose(sendingPurposeSpinner.getSelectedItem().toString());
            if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Bank")) {
                DataManager.getInstance().transactionModel.setReceiverBankAccountNumber(receiverModel.getBeneAccountNumber());
                DataManager.getInstance().transactionModel.setReceiverBankName(receiverModel.getBeneBankName());
                if (bankBranchModel == null) {
                    DataManager.getInstance().transactionModel.setReceiverBranchName(bankbranchnameEdt.getText().toString());
                    DataManager.getInstance().transactionModel.setReceiverBankBranchCode(bankbranchcodeEdt.getText().toString());
                } else {
                    DataManager.getInstance().transactionModel.setReceiverBranchName(bankBranchModel.getBranchName());
                    DataManager.getInstance().transactionModel.setReceiverBankBranchCode(bankBranchModel.getBranchCode());
                }


//                    DataManager.getInstance().transactionModel.setReceiverBankBranchCode();
                DataManager.getInstance().transactionModel.setIbanNumber(ibanEdt.getText().toString());
            }
//                if (!firstNameEdt.getText().toString().equalsIgnoreCase(receiverModel.getBeneName())
//                        || !contactEdt.getText().toString().equalsIgnoreCase(receiverModel.getBenePhone())
//                        || !iso_code_country.equalsIgnoreCase(receiverModel.getBeneCountryIsoCode())
//                        || !cityTxt.getText().toString().equalsIgnoreCase(receiverModel.getBeneCity())
//                        || !postalCodeTxt.getText().toString().equalsIgnoreCase(receiverModel.getBenePostCode())
//                        || !addressEdt.getText().toString().equalsIgnoreCase(receiverModel.getBeneAddress())
//                        || !accountTxt.getText().toString().equalsIgnoreCase(receiverModel.getBeneAccountNumber())
//                        || !bank_nameStr.equalsIgnoreCase(receiverModel.getBeneBankName())
//                        || !branch_nameStr.equalsIgnoreCase(receiverModel.getBeneBranchName()))
//
//                {
            updateReceiver();
//                } else {
//                    startActivityForResult(new Intent(RecieverDetailActivity.this, TransactionSummery.class), Constants.TRANSCATIONSUCESS);
//                }

    }

    @OnClick(R.id.cancelTxt)
    public void cancelTxt() {
        finish();
    }

    public void setNationalityArr() {
//        ArrayAdapter<String> nationalityArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, countryArr); //selected item will look like a spinner set from XML
//        nationalityArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
//        nationalitySpinner.setAdapter(nationalityArrayAdapter);
        String selectbankstr[] = {"Select bank"};
        String selectbranchstr[] = {"Select branch"};
        ArrayAdapter<String> bankSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, selectbankstr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        bankSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        backnameSpinner.setAdapter(bankSpinnerArrayAdapter);
        ArrayAdapter<String> branchSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, selectbranchstr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        bankSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        branchnameSpinner.setAdapter(branchSpinnerArrayAdapter);
        ArrayAdapter<String> relationArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, relationArr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        relationArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        relationSpinner.setAdapter(relationArrayAdapter);


        ArrayAdapter<String> sendingPurposeArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, sendPurposeArr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        };//selected item will look like a spinner set from XML
        sendingPurposeArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        sendingPurposeSpinner.setAdapter(sendingPurposeArrayAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void setData() {
        amountEdt.setText(DataManager.getInstance().transactionModel.getAmount() + " " + DataManager.getInstance().getUserInfoWrapper().getCurrencyCode());

        receiverModel = DataManager.getInstance().getModel();
        if (receiverModel != null) {
//            firstNameEdt.setText(receiverModel.getBeneName());

            firstNameEdt.append(receiverModel.getBeneName());


//            contactEdt.append(receiverModel.getBenePhone());
//            nationalitySpinner.setSelection(countryList.indexOf(country_name_map.get(receiverModel.getBeneCountryIsoCode().toString())));
            cityTxt.append(receiverModel.getBeneCity());
            addressEdt.append(receiverModel.getBeneAddress());

            accountTxt.append(receiverModel.getBeneAccountNumber());
            postalCodeTxt.append(receiverModel.getBenePostCode());


        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.TRANSCATIONSUCESS) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    private void getCountrylist() {
        final ArrayList<String> countryList = new ArrayList<>();
        MyApplication.getApplication().showProgressDialog(RecieverDetailActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(RecieverDetailActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(RecieverDetailActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetReceivingCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceReceivingCountryList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryList.add(sub_obj.getString("CountryName"));
                                    ios_code.put(sub_obj.getString("CountryName"), sub_obj.getString("Iso3Code"));
                                    country_name_map.put(sub_obj.getString("Iso3Code"), sub_obj.getString("CountryName"));
                                }
                                Collections.reverse(countryList);
                                countryList.add("Select country");
                                Collections.reverse(countryList);
//                                Collections.reverse(countryList);
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(RecieverDetailActivity.this, R.layout.spinner_layout, countryList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

                                nationalitySpinner.setAdapter(spinnerArrayAdapter);
                                MyApplication.getApplication().setCountryList(countryList);
                                MyApplication.getApplication().setIos_code(ios_code);
//                                if (!Constants.current_country.equalsIgnoreCase("")) {
//                                    nationalitySpinner.setSelection(countryList.indexOf(Constants.current_country));
//                                }
//                                final String globleCode = MyApplication.countryCodeMap.get(country_name_map.get(receiverModel.getBeneCountryIsoCode().toString()));
//                                contactEdt.setText("+"+globleCode);
//                                Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
////                                contactEdt.addTextChangedListener(new TextWatcher() {
////
////                                    @Override
////                                    public void onTextChanged(CharSequence s, int start, int before,
////                                                              int count) {
////                                        // TODO Auto-generated method stub
////
////                                    }
////
////                                    @Override
////                                    public void beforeTextChanged(CharSequence s, int start, int count,
////                                                                  int after) {
////                                        // TODO Auto-generated method stub
////
////                                    }
////
////                                    @Override
////                                    public void afterTextChanged(Editable s) {
////                                        if (!s.toString().startsWith("+"+globleCode)) {
////                                            contactEdt.setText("+"+globleCode);
////                                            Selection.setSelection(contactEdt.getText(), contactEdt
////                                                    .getText().length());
////
////                                        }
////
////                                    }
////
////                                });
//                                contactEdt.append(receiverModel.getBenePhone());
                                nationalitySpinner.setSelection(countryList.indexOf(country_name_map.get(receiverModel.getBeneCountryIsoCode().toString())));
                                nationalitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        contactEdt.setText("");
                                        final String globleCode = MyApplication.countryCodeMap.get(nationalitySpinner.getSelectedItem().toString());
                                        if (!TextUtils.isEmpty(receiverModel.getBenePhone())) {
                                            String firstCh = "" + receiverModel.getBenePhone().charAt(0);

                                                if (firstCh.equalsIgnoreCase("+")) {
                                                    String codebreak=    receiverModel.getBenePhone().substring(globleCode.length(), receiverModel.getBenePhone().length());

//                                final String globleCode = MyApplication.countryCodeMap.get(countrySpinner.getSelectedItem().toString());
                                                    contactEdt.setText("+" + globleCode);
                                                    contactEdt.append(codebreak);
//
                                                } else {

                                                    contactEdt.setText("+" + globleCode);
                                                    Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
                                                    contactEdt.append(receiverModel.getBenePhone());
                                                }

                                        }else
                                        {


                                            contactEdt.setText("+" + globleCode);
                                            Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
                                        }
//                                        final String globleCode = MyApplication.countryCodeMap.get(nationalitySpinner.getSelectedItem().toString());
//                                        contactEdt.setText("+"+globleCode);
//                                        Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
//                                        contactEdt.append(receiverModel.getBenePhone());
                                        iso_code_country = ios_code.get(nationalitySpinner.getSelectedItem().toString()).toString();
                                        if (MyApplication.isInternetWorking(RecieverDetailActivity.this)) {
                                            getCountryBankList(iso_code_country);

                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, RecieverDetailActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getCountryBankList(String CountryIso3Code) {
        MyApplication.getApplication().showProgressDialog(RecieverDetailActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(RecieverDetailActivity.this);
        final HashMap<String, String> bank_codeMap = new HashMap<>();
        final ArrayList<String> countryBankList = new ArrayList<>();
        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("ReceivingCountryIso3Code", CountryIso3Code);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(RecieverDetailActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetCountryBankList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.getApplication().hideProgressDialog();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceCountryBankList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryBankList.add(sub_obj.getString("BankName"));
                                    bank_codeMap.put(sub_obj.getString("BankName"), sub_obj.getString("BankCode"));
                                }
                                countryBankList.add("Select bank");
                                Collections.reverse(countryBankList);
                                ArrayAdapter<String> companyArrayAdapter = new ArrayAdapter<String>(RecieverDetailActivity.this, R.layout.spinner_layout, countryBankList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                companyArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                backnameSpinner.setAdapter(companyArrayAdapter);
                                if (receiverModel != null && getbank_status) {
                                    if (!receiverModel.getBeneBankName().equalsIgnoreCase("")) {
                                        backnameSpinner.setSelection(countryBankList.indexOf(receiverModel.getBeneBankName()));
                                    }
                                }
                                getbank_status = false;
                                MyApplication.getApplication().setCountryBankList(countryBankList);
                                backnameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        try {
                                            bankbranchcodeEdt.setText("");
                                            bankbranchnameEdt.setText("");
                                            bank_code = bank_codeMap.get(backnameSpinner.getSelectedItem().toString()).toString();
                                            bank_nameStr = backnameSpinner.getSelectedItem().toString();
                                            if (MyApplication.isInternetWorking(RecieverDetailActivity.this)) {
                                                GetBankBranchList(bank_code);
                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        requestQueue.add(jsObjRequest);
    }

    boolean getbranchNameCode = true;

    private void GetBankBranchList(String bank_code) {
        final ProgressDialog dialog = ProgressDialog.show(RecieverDetailActivity.this, "", "Please wait...");

        final ArrayList<String> countryBankBranchList = new ArrayList<>();

        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BankCode", bank_code);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(RecieverDetailActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetBankBranchList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            System.out.println("response>>> " + response.toString());

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceBankBranchList")) {
                                    JSONArray aceCountryJsonArray = response.getJSONArray("AceBankBranchList");
                                    final ArrayList<BankBranchModel> bankBranchModels = new DataParser(0, aceCountryJsonArray).getBankBranch();

                                    for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                        JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                        countryBankBranchList.add(sub_obj.getString("BranchName"));
                                        branchnamemap.put(sub_obj.getString("BranchName"), sub_obj.getString("BranchCode"));

                                    }
                                    if (aceCountryJsonArray.length() > 0) {
                                        bankbranchTxt.setText("Select Branch");
                                        branchnameSpinner.setVisibility(View.VISIBLE);
                                        bankbranchselectLayout.setVisibility(View.VISIBLE);
                                        bankbranchcodeLayout.setVisibility(View.GONE);
                                        bankbranchnameLayout.setVisibility(View.GONE);
                                    } else {

                                        bankbranchTxt.setText("No branches");

                                        bankbranchselectLayout.setVisibility(View.GONE);
                                        bankbranchcodeLayout.setVisibility(View.VISIBLE);
                                        bankbranchnameLayout.setVisibility(View.VISIBLE);
                                        if (getbranchNameCode) {
                                            bankbranchcodeEdt.setText(receiverModel.getBeneBankBranchCode());
                                            bankbranchnameEdt.setText(receiverModel.getBeneBranchName());
                                            getbranchNameCode = false;
                                        }
                                        if (TextUtils.isEmpty(bankbranchnameEdt.getText().toString()) && TextUtils.isEmpty(bankbranchcodeEdt.getText().toString())) {
                                            MyApplication.popNoBranch(RecieverDetailActivity.this);
                                        }

                                    }
                                    countryBankBranchList.add("Select branch");
                                    Collections.reverse(countryBankBranchList);
                                    ArrayAdapter<String> branchArrayAdapter = new ArrayAdapter<String>(RecieverDetailActivity.this, R.layout.spinner_layout, countryBankBranchList) {
                                        public View getView(int position, View convertView,
                                                            ViewGroup parent) {
                                            View v = super.getView(position, convertView, parent);


                                            if (position == 0) {

                                                ((TextView) v).setTextColor(getResources()
                                                        .getColorStateList(R.color.gray));
                                            } else {

                                                ((TextView) v).setTextColor(getResources()
                                                        .getColorStateList(R.color.black));
                                            }
                                            return v;
                                        }

                                        public View getDropDownView(int position, View convertView,
                                                                    ViewGroup parent) {
                                            View v = super.getDropDownView(position, convertView,
                                                    parent);


                                            ((TextView) v).setTextColor(Color.BLACK);


                                            return v;
                                        }
                                    }; //selected item will look like a spinner set from XML
                                    branchArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                    branchnameSpinner.setAdapter(branchArrayAdapter);
                                    MyApplication.getApplication().setCountryBankBranchList(countryBankBranchList);


                                    if (receiverModel != null && getbranch_status) {
                                        if (!receiverModel.getBeneBankBranchCode().equalsIgnoreCase("")) {
                                            branchnameSpinner.setSelection(countryBankBranchList.indexOf(receiverModel.getBeneBranchName()));
                                        }
                                    }
                                    getbranch_status = false;
                                    branchnameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            try {
                                                if (!branchnameSpinner.getSelectedItem().toString().equalsIgnoreCase("Select branch")) {
                                                    branch_nameStr = branchnameSpinner.getSelectedItem().toString();
                                                    bankBranchModel = bankBranchModels.get(countryBankBranchList.indexOf(branchnameSpinner.getSelectedItem().toString()));
                                                    branchName = branchnameSpinner.getSelectedItem().toString();
                                                    branchCode = branchnamemap.get(branchName);
                                                }
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        dialog.dismiss();
                    }
                });
        requestQueue.add(jsObjRequest);
    }

    private void updateReceiver() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(RecieverDetailActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(RecieverDetailActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BeneID", receiverModel.getBeneID());

            JSONObject acebeneObject = new JSONObject();
            acebeneObject.put("BeneID", receiverModel.getBeneID());
            acebeneObject.put("BeneName", firstNameEdt.getText().toString());
            acebeneObject.put("BeneAddress", addressEdt.getText().toString());
            acebeneObject.put("BeneCity", cityTxt.getText().toString());
            acebeneObject.put("BenePostCode", postalCodeTxt.getText().toString());
            acebeneObject.put("BeneCountryIsoCode", iso_code_country);
            acebeneObject.put("BenePhone", contactEdt.getText().toString());
            if (!TextUtils.isEmpty(backnameSpinner.getSelectedItem().toString())) {
                acebeneObject.put("BeneBankName", backnameSpinner.getSelectedItem().toString());
            }


            acebeneObject.put("BeneAccountNumber", accountTxt.getText().toString());


            acebeneObject.put("BeneBankCode", bank_code);
            if (bankBranchModel != null) {
                acebeneObject.put("BeneBranchName",branchName);

                acebeneObject.put("BeneBankBranchCode", branchCode);

            } else {
                acebeneObject.put("BeneBranchName", bankbranchnameEdt.getText().toString());

                acebeneObject.put("BeneBankBranchCode", bankbranchcodeEdt.getText().toString());

            }
            object.put("AceBene", acebeneObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(RecieverDetailActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/BeneUpdate", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());

                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                startActivityForResult(new Intent(RecieverDetailActivity.this, TransactionSummery.class), Constants.TRANSCATIONSUCESS);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        requestQueue.add(jsObjRequest);
    }

}
