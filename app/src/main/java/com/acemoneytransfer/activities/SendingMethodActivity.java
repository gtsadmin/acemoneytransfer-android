package com.acemoneytransfer.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendingMethodActivity extends AppCompatActivity {

    @Bind(R.id.bankSpinner)
    Spinner bankSpinner;
    @Bind(R.id.onlineTransferSpinner)
    Spinner onlineTransferSpinner;

    @Bind(R.id.bankTransferlinear)
    LinearLayout bankTransferlinear;
    @Bind(R.id.doneTxt)
    ImageButton doneTxt;
    @Bind(R.id.creditLayout)
    RelativeLayout creditLayout;
    private Toolbar toolbar;
    String countryNamestr=null;
    //private String paymentMethodArr[] = new String[]{"Select", "Sofort Banking", "Online Transfer"};
    private String companyArr[] = new String[]{"HDFC"};
    //private String branchArr[] = new String[]{"Select", "Debit Card", "Credit Card", "Bank"};
    ArrayList<String> paymentMethodArr = new ArrayList<>();
    ArrayList<String> branchArr = new ArrayList<>();
    HashMap<String,String> country_name = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sending_method);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        paymentMethodArr.add("Select");
     ArrayList<String> sendContry = new ArrayList<>();
        sendContry.add("Greece");
        sendContry.add("Sweden");
        sendContry.add("United Kingdom");
        sendContry.add("France");
        sendContry.add("Italy");
        sendContry.add("Spain");
        sendContry.add("Netherlands");
        sendContry.add("Norway");
        sendContry.add("Denmark");
        sendContry.add("Portugal");
        HashMap<String,String> countymap = new HashMap<>();
        countymap.put("Greece","Greece");
        countymap.put("Sweden","Sweden");
        countymap.put("United Kingdom","United Kingdom");
        countymap.put("France","France");
        countymap.put("Italy","Italy");
        countymap.put("Spain","Spain");
        countymap.put("Netherlands","Netherlands");
        countymap.put("Norway","Norway");
        countymap.put("Denmark","Denmark");
        countymap.put("Portugal","Portugal");


        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(SendingMethodActivity.this);
        dbHelper.openDataBase();
        ArrayList<String> countryList = dbHelper.getCountryList(SqlLiteDbHelper.NAME);
        ArrayList<String> isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);

        for (int i = 0; i < countryList.size(); i++) {
            country_name.put(isoList.get(i),countryList.get(i));

        }
        dbHelper.close();
        paymentMethodArr.add("Online Transfer");
        branchArr.add("Select");
        branchArr.add("Debit Card");
        branchArr.add("Credit Card");
        countryNamestr =country_name.get(DataManager.getInstance().getUserInfoWrapper().getCountryIsoCode());
        DataManager.getInstance().setCountryNamestr(countryNamestr);
        String namecoutStr = countymap.get(countryNamestr);
       if (!TextUtils.isEmpty(namecoutStr)) {
           branchArr.add("Bank");
       }
        setSupportActionBar(toolbar);
        setNationalityArr();

        // cat_price = getIntent().getExtras().getString("cat_price");
        toolbar.setNavigationIcon(R.mipmap.icn_close);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Sending Method");

//        getSupportActionBar().setTitle("Sending Method");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        bankSpinner.setSelection(branchArr.indexOf(MyApplication.getApplication().getSending_method()));
        onlineTransferSpinner.setSelection(paymentMethodArr.indexOf(MyApplication.getApplication().getReceiving_method()));
    }

    @OnClick(R.id.doneTxt)
    public void doneTxt() {
        MyApplication.getApplication().setSending_method(bankSpinner.getSelectedItem().toString());
        if (!bankSpinner.getSelectedItem().toString().equalsIgnoreCase("Select")) {
            Constants.SENDINGMETHOD = true;
        }
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    public void setNationalityArr() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, branchArr); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        bankSpinner.setAdapter(spinnerArrayAdapter);
        bankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (bankSpinner.getSelectedItem().toString().equalsIgnoreCase("Bank")) {
                    bankTransferlinear.setVisibility(View.VISIBLE);
                } else {
                    bankTransferlinear.setVisibility(View.GONE);
                }
                 if (bankSpinner.getSelectedItem().toString().equalsIgnoreCase("Credit Card"))
                 {
                     creditLayout.setVisibility(View.VISIBLE);
                 }else
                 {
                     creditLayout.setVisibility(View.GONE);
                 }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> paymentArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, paymentMethodArr); //selected item will look like a spinner set from XML
        paymentArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        onlineTransferSpinner.setAdapter(paymentArrayAdapter);
        onlineTransferSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                MyApplication.getApplication().setReceiving_method(onlineTransferSpinner.getSelectedItem().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


}
