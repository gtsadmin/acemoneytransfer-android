package com.acemoneytransfer.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.datacontroller.DataParser;
import com.acemoneytransfer.model.BankBranchModel;
import com.acemoneytransfer.model.TransactioListModel;
import com.acemoneytransfer.views.CircularImageView;
import com.acemoneytransfer.views.EditTextAvenirLTStdBook;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateTranscationActvitity extends AppCompatActivity {

    @Bind(R.id.userImgView)
    CircularImageView userImgView;
    @Bind(R.id.imageContainer)
    RelativeLayout imageContainer;
    @Bind(R.id.nameEdt)
    EditTextAvenirLTStdBook nameEdt;
    @Bind(R.id.contactEdt)
    EditTextAvenirLTStdBook contactEdt;
    @Bind(R.id.addressEdt)
    EditTextAvenirLTStdBook addressEdt;
    @Bind(R.id.cityEdt)
    EditTextAvenirLTStdBook cityEdt;
    @Bind(R.id.postalCodeEdt)
    EditTextAvenirLTStdBook postalCodeEdt;
    @Bind(R.id.countrySpinner)
    Spinner countrySpinner;
    @Bind(R.id.accountNumberEdt)
    EditTextAvenirLTStdBook accountNumberEdt;
    @Bind(R.id.backnameSpinner)
    Spinner backnameSpinner;
    @Bind(R.id.branchnameSpinner)
    Spinner branchnameSpinner;
    @Bind(R.id.updateTxt)
    TextViewAvenirLTStdBook updateTxt;
    Toolbar toolbar;
     ArrayList<String> countryBankList;
    ArrayList<String> countryBankBranchList;
    BankBranchModel bankBranchModel;
    String bank_code = "";
    ArrayList<String> countryList = new ArrayList<>();
    ArrayList<String> isoList = new ArrayList<>();
    HashMap<String, String> country_name = new HashMap<>();
    HashMap<String, String> ios_code = new HashMap<>();
    HashMap<String, String> countrynameHashMap = new HashMap<>();
    String iso_code_country = "";

    @Bind(R.id.bankbranchselectLayout)
    LinearLayout bankbranchselectLayout;
    @Bind(R.id.bankbranchnameEdt)
    EditTextAvenirLTStdBook bankbranchnameEdt;
    @Bind(R.id.bankbranchnameLayout)
    LinearLayout bankbranchnameLayout;
    @Bind(R.id.bankbranchcodeEdt)
    EditTextAvenirLTStdBook bankbranchcodeEdt;
    @Bind(R.id.bankbranchcodeLayout)
    LinearLayout bankbranchcodeLayout;
    TransactioListModel transactioListModel;

    HashMap<String, String> branchnamemap = new HashMap<>();
    String branchName = "", branchCode = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_transcation_layout);
        ButterKnife.bind(this);
        transactioListModel = (TransactioListModel) getIntent().getExtras().getSerializable("model");
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Update Transaction");
//        getSupportActionBar().setTitle("Transaction List");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
        transactioListModel = (TransactioListModel) getIntent().getExtras().getSerializable("model");
        setData();
//        setCountry();
//        setNationalityArr();
        getCountrylist();
//        getBeneInfo();
//        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                iso_code_country = ios_code.get(countrySpinner.getSelectedItem().toString()).toString();
//                if (MyApplication.isInternetWorking(UpdateTranscationActvitity.this)) {
//                    getCountryBankList(iso_code_country);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

    }

    @OnClick(R.id.updateTxt)
    public void updateTxt() {
        if (MyApplication.isInternetWorking(UpdateTranscationActvitity.this)) {
            updateTransaction1();
        }
    }

    private void setData() {
//        nameEdt.setText(transactioListModel.getBeneName());
    }

    private void getCountrylist() {
        countryList = new ArrayList<>();
        MyApplication.getApplication().showProgressDialog(UpdateTranscationActvitity.this);
        MyApplication.getApplication().hideSoftKeyBoard(UpdateTranscationActvitity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(UpdateTranscationActvitity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetReceivingCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceReceivingCountryList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryList.add(sub_obj.getString("CountryName"));
                                    ios_code.put(sub_obj.getString("CountryName"), sub_obj.getString("Iso3Code"));
                                    country_name.put(sub_obj.getString("Iso3Code"), sub_obj.getString("CountryName"));
                                }
                                Collections.reverse(countryList);
                                countryList.add("Select country");
                                Collections.reverse(countryList);
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(UpdateTranscationActvitity.this, R.layout.spinner_layout, countryList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

                                countrySpinner.setAdapter(spinnerArrayAdapter);
//                                setInformation();
                                MyApplication.getApplication().setCountryList(countryList);
                                MyApplication.getApplication().setIos_code(ios_code);
//                                if (!Constants.current_country.equalsIgnoreCase("")) {
//                                    nationalitySpinner.setSelection(countryList.indexOf(Constants.current_country));
//                                }
                                countrySpinner.setSelection(countryList.indexOf(transactioListModel.getReceivingCountry()));
                                countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (!countrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                                            iso_code_country = ios_code.get(countrySpinner.getSelectedItem().toString()).toString();
                                            if (MyApplication.isInternetWorking(UpdateTranscationActvitity.this)) {
                                                getCountryBankList(iso_code_country);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, UpdateTranscationActvitity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void setCountry() {


        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(UpdateTranscationActvitity.this);
        dbHelper.openDataBase();
        countryList = dbHelper.getCountryList(SqlLiteDbHelper.NAME);
        isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
        for (int i = 0; i < countryList.size(); i++) {
            country_name.put(isoList.get(i), countryList.get(i));
            ios_code.put(countryList.get(i), isoList.get(i));
            countrynameHashMap.put(isoList.get(i), countryList.get(i));
        }


    }

    public void setNationalityArr() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, countryList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

        countrySpinner.setAdapter(spinnerArrayAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }
boolean emptyBln =true;
    private void getCountryBankList(String CountryIso3Code) {
       final ProgressDialog dialog = ProgressDialog.show(UpdateTranscationActvitity.this,"","Please wait...");
        MyApplication.getApplication().hideSoftKeyBoard(UpdateTranscationActvitity.this);
        countryBankList = new ArrayList<>();
        final HashMap<String, String> bank_codeMap = new HashMap<>();

        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("ReceivingCountryIso3Code", CountryIso3Code);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(UpdateTranscationActvitity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetCountryBankList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceCountryBankList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryBankList.add(sub_obj.getString("BankName"));
                                    bank_codeMap.put(sub_obj.getString("BankName"), sub_obj.getString("BankCode"));
                                }
                                getBeneInfo();
//                                if (aceCountryJsonArray.length()<0)
//                                {
//                                   getBeneInfo();
//                                }
                                ArrayAdapter<String> companyArrayAdapter = new ArrayAdapter<String>(UpdateTranscationActvitity.this, R.layout.spinner_layout, countryBankList); //selected item will look like a spinner set from XML
                                companyArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                backnameSpinner.setAdapter(companyArrayAdapter);
//                                backnameSpinner.setSelection(countryBankList.indexOf(transactioListModel.get));
                                MyApplication.getApplication().setCountryBankList(countryBankList);
                                backnameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        try {
                                            if (!emptyBln) {
                                                bankbranchcodeEdt.setText("");
                                                bankbranchnameEdt.setText("");
                                            }
                                            bank_code = bank_codeMap.get(backnameSpinner.getSelectedItem().toString()).toString();
                                            GetBankBranchList(bank_code);
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void GetBankBranchList(String bank_code) {
        final ProgressDialog dialog = ProgressDialog.show(UpdateTranscationActvitity.this,"","Please wait...");
        MyApplication.getApplication().hideSoftKeyBoard(UpdateTranscationActvitity.this);
        countryBankBranchList = new ArrayList<>();

        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BankCode", bank_code);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(UpdateTranscationActvitity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetBankBranchList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            System.out.println("response>>> " + response.toString());

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceBankBranchList")) {
                                    JSONArray aceCountryJsonArray = response.getJSONArray("AceBankBranchList");
                                    final ArrayList<BankBranchModel> bankBranchModels = new DataParser(0, aceCountryJsonArray).getBankBranch();

                                    for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                        JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                        countryBankBranchList.add(sub_obj.getString("BranchName"));
                                        branchnamemap.put(sub_obj.getString("BranchName"), sub_obj.getString("BranchCode"));

                                    }
                                    countryBankBranchList.add("Select branch");
                                    Collections.reverse(countryBankBranchList);
                                    ArrayAdapter<String> branchArrayAdapter = new ArrayAdapter<String>(UpdateTranscationActvitity.this, R.layout.spinner_layout, countryBankBranchList); //selected item will look like a spinner set from XML
                                    branchArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                    MyApplication.getApplication().setCountryBankBranchList(countryBankBranchList);
                                    if (aceCountryJsonArray.length() > 0) {

                                        branchnameSpinner.setVisibility(View.VISIBLE);
                                        bankbranchselectLayout.setVisibility(View.VISIBLE);
                                        bankbranchcodeLayout.setVisibility(View.GONE);
                                        bankbranchnameLayout.setVisibility(View.GONE);
                                    } else {
//                                        MyApplication.popNoBranch(UpdateTranscationActvitity.this);

                                        bankbranchselectLayout.setVisibility(View.GONE);
                                        bankbranchcodeLayout.setVisibility(View.VISIBLE);
                                        bankbranchnameLayout.setVisibility(View.VISIBLE);



                                    }

                                    branchnameSpinner.setAdapter(branchArrayAdapter);

                                    branchnameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            try {
                                                if (!branchnameSpinner.getSelectedItem().toString().equalsIgnoreCase("Select branch")) {

                                                    branchName = branchnameSpinner.getSelectedItem().toString();
                                                    branchCode = branchnamemap.get(branchName);
                                                    bankBranchModel = bankBranchModels.get(countryBankBranchList.indexOf(branchnameSpinner.getSelectedItem().toString()));
                                                }
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void updateTransaction() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(UpdateTranscationActvitity.this);
        MyApplication.getApplication().hideSoftKeyBoard(UpdateTranscationActvitity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BeneID", transactioListModel.getBeneID());

            JSONObject acebeneObject = new JSONObject();
            acebeneObject.put("BeneName", nameEdt.getText().toString());
            acebeneObject.put("BeneAddress", addressEdt.getText().toString());
            acebeneObject.put("BeneCity", cityEdt.getText().toString());
            acebeneObject.put("BenePostCode", postalCodeEdt.getText().toString());
            acebeneObject.put("BeneCountryIsoCode", iso_code_country);
            acebeneObject.put("BenePhone", contactEdt.getText().toString());
            if (countryBankList.size()>0) {
                acebeneObject.put("BeneBankName", backnameSpinner.getSelectedItem().toString());
            }

            acebeneObject.put("BeneAccountNumber", accountNumberEdt.getText().toString());
            acebeneObject.put("BeneID", transactioListModel.getBeneID());
            acebeneObject.put("BeneBankCode", bank_code);
            if (bankBranchModel!=null)
            {
                acebeneObject.put("BeneBankBranchCode", branchCode);
                acebeneObject.put("BeneBranchName", branchName);
            }else
            {
                acebeneObject.put("BeneBankBranchCode", bankbranchcodeEdt.getText().toString());
                acebeneObject.put("BeneBranchName", bankbranchnameEdt.getText().toString());
            }

            object.put("AceBene", acebeneObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(UpdateTranscationActvitity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/BeneUpdate", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());

                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                popMessage(message);
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, UpdateTranscationActvitity.this);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateTranscationActvitity.this);
                                builder.setMessage(message).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();



                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
    private void updateTransaction1() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(UpdateTranscationActvitity.this);
        MyApplication.getApplication().hideSoftKeyBoard(UpdateTranscationActvitity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("PaymentNumber", transactioListModel.getPaymentNumber());

            JSONObject acebeneObject = new JSONObject();
            acebeneObject.put("BeneName", nameEdt.getText().toString());
            acebeneObject.put("BeneAddress", addressEdt.getText().toString());
            acebeneObject.put("BeneCity", cityEdt.getText().toString());
            acebeneObject.put("BenePostCode", postalCodeEdt.getText().toString());
            acebeneObject.put("BeneCountryIsoCode", iso_code_country);
            acebeneObject.put("BenePhone", contactEdt.getText().toString());
            if (countryBankList.size()>0) {
                acebeneObject.put("BeneBankName", backnameSpinner.getSelectedItem().toString());
            }

            acebeneObject.put("BeneAccountNumber", accountNumberEdt.getText().toString());
            acebeneObject.put("BeneID", transactioListModel.getBeneID());
            acebeneObject.put("BeneBankCode", bank_code);
            if (bankBranchModel!=null)
            {
                acebeneObject.put("BeneBankBranchCode", branchCode);
                acebeneObject.put("BeneBranchName", branchName);
            }else
            {
                acebeneObject.put("BeneBankBranchCode", bankbranchcodeEdt.getText().toString());
                acebeneObject.put("BeneBranchName", bankbranchnameEdt.getText().toString());
            }

            object.put("AceBene", acebeneObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(UpdateTranscationActvitity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceTrans/Update", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());

                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                popMessage(message);
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, UpdateTranscationActvitity.this);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateTranscationActvitity.this);
                                builder.setMessage(message).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();



                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
    public void popMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(UpdateTranscationActvitity.this);
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                DataManager.getInstance().setUpdateTrans(false);
                setResult(RESULT_OK);
                finish();


            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getBeneInfo() {


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BeneID", transactioListModel.getBeneID());

//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(UpdateTranscationActvitity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/GetBene", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONObject sub_obj = null;

                                sub_obj = response.getJSONObject("AceBene");

                                if (sub_obj.has("BeneID")) {
                                    sub_obj.getString("BeneID");
                                }
                                if (sub_obj.has("BeneName")) {
                                    nameEdt.append(sub_obj.getString("BeneName"));
                                }
                                if (sub_obj.has("BeneAddress")) {
                                    addressEdt.append(sub_obj.getString("BeneAddress"));

                                }
                                if (sub_obj.has("BeneCity")) {
                                    cityEdt.append(sub_obj.getString("BeneCity"));

                                }
                                if (sub_obj.has("BenePostCode")) {
                                    postalCodeEdt.append(sub_obj.getString("BenePostCode"));

                                }
                                if (sub_obj.has("BeneCountryIsoCode")) {

                                    countrySpinner.setSelection(countryList.indexOf(countrynameHashMap.get(sub_obj.getString("BeneCountryIsoCode"))));
                                }
                                if (sub_obj.has("BenePhone")) {

                                    contactEdt.append(sub_obj.getString("BenePhone"));

                                }
                                if (sub_obj.has("BeneBankName")) {
                                    backnameSpinner.setSelection(countryBankList.indexOf(sub_obj.getString("BeneBankName")));
//                                    emptyBln =false;

                                }
                                if (sub_obj.has("BeneBranchName")) {
                                   bankbranchnameEdt.setText(sub_obj.getString("BeneBranchName"));
                                    if (countryBankBranchList.size() > 0) {
                                        branchnameSpinner.setSelection(countryBankBranchList.indexOf(sub_obj.getString("BeneBranchName")));
                                    }


                                }
                                if (sub_obj.has("BeneAccountNumber")) {

                                    accountNumberEdt.append(sub_obj.getString("BeneAccountNumber"));

                                }

                                if (sub_obj.has("BeneBankCode")) {
                                    sub_obj.getString("BeneBankCode");
                                }
                                if (sub_obj.has("BeneBankBranchCode")) {
                                    sub_obj.getString("BeneBankBranchCode");
                                    bankbranchcodeEdt.setText(sub_obj.getString("BeneBankBranchCode"));
                                }
                              new CountDownTimer(1000,1000)
                              {

                                  @Override
                                  public void onTick(long millisUntilFinished) {

                                  }

                                  @Override
                                  public void onFinish() {
                                      emptyBln =false;
                                  }
                              }.start();
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, UpdateTranscationActvitity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
}
