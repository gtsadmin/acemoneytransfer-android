package com.acemoneytransfer.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataParser;
import com.acemoneytransfer.model.CountryModel;
import com.acemoneytransfer.views.CircularImageView;
import com.acemoneytransfer.views.EditTextAvenirLTStdBook;
import com.acemoneytransfer.views.RadioButtonAvenirLTStdBook;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SignUpActivity extends AppCompatActivity {
    @Bind(R.id.closeImgBtn)
    ImageButton closeImgBtn;
    @Bind(R.id.signInTxt)
    TextViewAvenirLTStdBook signInTxt;
    @Bind(R.id.userImgView)
    CircularImageView userImgView;
    @Bind(R.id.imageContainer)
    RelativeLayout imageContainer;
    @Bind(R.id.nameEdt)
    EditTextAvenirLTStdBook nameEdt;
    @Bind(R.id.emailEdt)
    EditTextAvenirLTStdBook emailEdt;
    @Bind(R.id.passwordEdt)
    EditTextAvenirLTStdBook passwordEdt;
    @Bind(R.id.confirmPasswordEdt)
    EditTextAvenirLTStdBook confirmPasswordEdt;
    @Bind(R.id.nationalitySpinner)
    Spinner nationalitySpinner;
    @Bind(R.id.birthdayTxt)
    TextViewAvenirLTStdBook birthdayTxt;

    @Bind(R.id.radioSex)
    RadioGroup radioSex;
    @Bind(R.id.signUpTxt)
    ImageButton signUpTxt;
    @Bind(R.id.radioMale)
    RadioButtonAvenirLTStdBook radioMale;
    @Bind(R.id.radioFemale)
    RadioButtonAvenirLTStdBook radioFemale;


    private int year;
    private int month;
    private int day;
    DataParser dataParser;
    private final int DATE_DIALOG_ID = 1;

    ArrayList<String> isoList = new ArrayList<>();
    String iso_code_str = "";

    private String genderString = "male", dobString = "";
    ArrayList<CountryModel> countryModels = new ArrayList<>();
    ArrayList<String> countryList = new ArrayList<>();
    HashMap<String, String> iso_country_code = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        setData();
        setNationalityArr();



    }

    private void setData() {

        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(SignUpActivity.this);
        dbHelper.openDataBase();
        countryList = dbHelper.getCountryList(SqlLiteDbHelper.NAME);
        isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
        for (int i = 0; i < countryList.size(); i++) {

            iso_country_code.put(countryList.get(i), isoList.get(i));
        }

        radioFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    genderString = "female";
            }
        });


        radioMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    genderString = "male";
            }
        });

    }

    @OnClick(R.id.closeImgBtn)
    public void closeImgBtn() {
        onBackPressed();
    }

    @OnClick(R.id.birthdayTxt)
    public void birthdayTxt() {

        showDialog(DATE_DIALOG_ID);

    }

    @OnClick(R.id.signUpTxt)
    public void signUpTxt() {
        if (MyApplication.isInternetWorking(SignUpActivity.this)) {
            if (checkFreeTrial()>17) {
                signUp();
            }else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                builder.setMessage("minimum age should be 18 years requared")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();

                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }

        }
    }

    private void signUp() {
        if (MyApplication.isInternetWorking(SignUpActivity.this)) {

            if (TextUtils.isEmpty(emailEdt.getText().toString())) {

                Toast.makeText(SignUpActivity.this, "Email cannot be empty", Toast.LENGTH_LONG).show();
            } else if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
                Toast.makeText(SignUpActivity.this, "Password cannot be empty", Toast.LENGTH_LONG).show();
            } else if (TextUtils.isEmpty(confirmPasswordEdt.getText().toString())) {
                Toast.makeText(SignUpActivity.this, "Confirm password cannot be empty", Toast.LENGTH_LONG).show();
            } else if (!passwordEdt.getText().toString().equalsIgnoreCase(confirmPasswordEdt.getText().toString())) {
                Toast.makeText(SignUpActivity.this, "Passwords do not match", Toast.LENGTH_LONG).show();
            } else if (passwordEdt.getText().toString().length() < 6) {
                Toast.makeText(SignUpActivity.this, "Password too short", Toast.LENGTH_LONG).show();
            } else if (!Patterns.EMAIL_ADDRESS.matcher(emailEdt.getText().toString()).matches()) {
                Toast.makeText(SignUpActivity.this, "Invaild email", Toast.LENGTH_LONG).show();
            } else if(TextUtils.isEmpty(iso_code_str))
            {
                Toast.makeText(SignUpActivity.this, "Please select country", Toast.LENGTH_LONG).show();
            }

            else {

                registerUser();
            }
        }
    }

    private void registerUser() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(SignUpActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(SignUpActivity.this);


        JSONObject object = new JSONObject();

        try {
            object.put("Password", passwordEdt.getText().toString());
            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", "");

            object.put("Username", "");

            JSONObject userObject = new JSONObject();
            userObject.put("FullName", nameEdt.getText().toString());
            userObject.put("Email", emailEdt.getText().toString());

            userObject.put("Phone", "");
            userObject.put("DOB", birthdayTxt.getText().toString());

            userObject.put("Gender", genderString);
            userObject.put("CountryIsoCode", iso_code_str);

            userObject.put("NationalityIsoCode", "");

            userObject.put("CountryOfBirthIsoCode", "");
            userObject.put("Address", "");
            userObject.put("City", "");
            userObject.put("PostalCode", "");


            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(SignUpActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUser/SignUp", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null}}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                String AuthToken = response.getString("AuthToken");
                                MyApplication.saveAuthToken("AuthToken", AuthToken);
                                MyApplication.saveUserEmail("email", emailEdt.getText().toString());
                                MyApplication.saveUserInfo("userinfo", response.toString());

                                    startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                                SignUpActivity.this.overridePendingTransition(R.anim.left,
                                            R.anim.right);


                                finish();
                            } else {
                                Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
    private void getsendCountry() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(SignUpActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(SignUpActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", "");




        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(SignUpActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null}}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceCountryList")) {
                                    JSONArray aceSendingCountryList = response.getJSONArray("AceCountryList");
                                    for (int i = 0; i <aceSendingCountryList.length(); i++) {
                                        CountryModel countryModel = new CountryModel();
                                        JSONObject acesendingSub = aceSendingCountryList.getJSONObject(i);
                                        if (acesendingSub.has("CountryName"))
                                        {
                                            countryList.add(acesendingSub.getString("CountryName"));
                                            countryModel.setCountryName(acesendingSub.getString("CountryName"));
                                        }
                                        if (acesendingSub.has("Iso2Code"))
                                        {
                                            countryModel.setIso2Code(acesendingSub.getString("Iso2Code"));
                                        }
                                        if (acesendingSub.has("Iso3Code")&&acesendingSub.has("CountryName"))
                                        {
                                            countryModel.setIso3Code(acesendingSub.getString("Iso3Code"));
                                            iso_country_code.put(acesendingSub.getString("CountryName"), acesendingSub.getString("Iso3Code"));
                                        }
                                        if (acesendingSub.has("IsoNumericCode"))
                                        {
                                            countryModel.setIsoNumericCode(acesendingSub.getString("IsoNumericCode"));
                                        }
                                        if (acesendingSub.has("CurrencyIsoCode"))
                                        {
                                            countryModel.setCurrencyIsoCode(acesendingSub.getString("CurrencyIsoCode"));
                                        }
                                        countryModels.add(countryModel);
                                    }

                                }
                            } else {
                                Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();

        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date

                return new DatePickerDialog(this, datePickerListener,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));


        }
        return null;
    }

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("MMM dd yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String formateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = targetFormat.parse(dateString);
            String formattedDate = originalFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String dateString;
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {


        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview


            String dayString = day + "", monthString = (month + 1) + "";

            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
            dateString = monthString + "-" + dayString + "-" + year;
            birthdayTxt.setText(formateDate(dateString));
            dobString = updateDateForServer(dateString);

        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    public void setNationalityArr() {
        countryList.add("Select country");
        Collections.reverse(countryList);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, countryList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        nationalitySpinner.setAdapter(spinnerArrayAdapter);
        nationalitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                    iso_code_str = iso_country_code.get(nationalitySpinner.getSelectedItem().toString()).toString();
                    System.out.println("iso_code_str" + iso_code_str);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        birthPlaceSpinner.setAdapter(spinnerArrayAdapter);
//        countrySpinner.setAdapter(spinnerArrayAdapter);

    }
    private String updateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
    public long checkFreeTrial()
    {
        long diffDays = 0;
        boolean checkFreeTrial=true;
        String pattern = "MM-dd-yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);




//                    Date two = null;
        try {

            Date one = dateFormat.parse(dateString);
            String currentDateandTime = dateFormat.format(new Date());
            Date currentDate = dateFormat.parse(currentDateandTime);
            long diff = currentDate.getTime() - one.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
             diffDays = diff / (24 * 60 * 60 * 1000);
//            if (diffDays>17) {
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
//                builder.setMessage("minimum age should be 18 years requared")
//                        .setCancelable(false)
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//
//                            }
//                        });
//
//                AlertDialog alert = builder.create();
//                alert.show();
//
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diffDays;
    }
}
