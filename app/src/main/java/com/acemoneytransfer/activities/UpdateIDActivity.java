package com.acemoneytransfer.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.model.IDListModel;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.janmuller.android.simplecropimage.CropImage;


public class UpdateIDActivity extends AppCompatActivity {

    View view;

    public int year;
    public int month;
    public int day;

    private final int DATE_DIALOG_ID = 1;
    int date_type = 1;

    ArrayList<String> identity_typeArr = new ArrayList<>();

    public static final int REQUEST_CODE_GALLERY = 0x51;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x53;
    @Bind(R.id.identityTypeSpinner)
    Spinner identityTypeSpinner;
    @Bind(R.id.arrowImage)
    ImageView arrowImage;
    @Bind(R.id.txt2)
    TextViewAvenirLTStdBook txt2;
    @Bind(R.id.idnumberEdt)
    EditText idnumberEdt;
    @Bind(R.id.arrowImage1)
    ImageView arrowImage1;
    @Bind(R.id.txt3)
    TextView txt3;
    @Bind(R.id.issueDateBtn)
    TextViewAvenirLTStdBook issueDateBtn;
    @Bind(R.id.arrowImage3)
    ImageView arrowImage3;
    @Bind(R.id.txt4)
    TextView txt4;
    @Bind(R.id.expiryDateBtn)
    TextViewAvenirLTStdBook expiryDateBtn;
    @Bind(R.id.arrowImage4)
    ImageView arrowImage4;
    @Bind(R.id.txt)
    TextViewAvenirLTStdBook txt;
    @Bind(R.id.select_image)
    ImageView selectImage;
    @Bind(R.id.doneBtn)
    ImageButton doneBtn;
    @Bind(R.id.select_imageBack)
    ImageView selectImageBack;

    private File mFileTemp;
    private String imagePath = "", images_uptedates = "";
    String identityStr = "", issue_dateStr = "", expiry_dateStr = "";
    Toolbar toolbar;
    IDListModel idListModel;
    Bitmap bitmap,bitmapBack;
    int frontimagenum_backimagenum;
    DatePickerDialog dateDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.updateactivity_add_id);
        ButterKnife.bind(this);
        identity_typeArr.add("Passport");
        identity_typeArr.add("Licence");
        identity_typeArr.add("ID-Card");
        identity_typeArr.add("Resident Card");
        ArrayAdapter<String> identity_typeArrAdapter = new ArrayAdapter<String>(UpdateIDActivity.this, R.layout.spinner_layout, identity_typeArr); //selected item will look like a spinner set from XML
        identity_typeArrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        identityTypeSpinner.setAdapter(identity_typeArrAdapter);
        identityTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                identityStr = identityTypeSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Update ID");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
        idListModel = (IDListModel) getIntent().getExtras().getSerializable("idmodel");
        identityTypeSpinner.setSelection(identity_typeArr.indexOf(idListModel.getDocType()));
        idnumberEdt.setText(idListModel.getDocNumber());
        issueDateBtn.setText(formateDateForServer(idListModel.getDocIssueDate()));
        expiryDateBtn.setText(formateDateForServer(idListModel.getDocExpireDate()));
        MyApplication.getApplication().loader.displayImage(idListModel.getDocFileUrl(), selectImage, MyApplication.options);
        MyApplication.getApplication().loader.displayImage(idListModel.getDocFileBackUrl(), selectImageBack, MyApplication.options);
        MyApplication.DATAUPDATESTUTAS = false;

    }

    @OnClick(R.id.issueDateBtn)
    public void issueDateBtn() {

        date_type = 1;

        showDatePickerDialog();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    @OnClick(R.id.expiryDateBtn)
    public void expiryDateBtn() {

        date_type = 2;

        showDatePickerDialog();
    }

    @OnClick(R.id.select_image)
    public void select_image() {
        frontimagenum_backimagenum=8;
        attachImage();
    }
    @OnClick(R.id.select_imageBack)
    public void select_imageBack() {
        frontimagenum_backimagenum=9;
        attachImage();
    }
    @OnClick(R.id.doneBtn)
    public void doneBtn() {
        if (TextUtils.isEmpty(idnumberEdt.getText().toString())) {
            idnumberEdt.setError("Please Enter ID Number");
            idnumberEdt.requestFocus();
        } else if (TextUtils.isEmpty(issueDateBtn.getText().toString())) {
            issueDateBtn.setError("Please Enter Issue Date");
            issueDateBtn.requestFocus();
        } else if (TextUtils.isEmpty(expiryDateBtn.getText().toString())) {
            expiryDateBtn.setError("Please Enter Expiry Date");
            expiryDateBtn.requestFocus();
        } else {
            if (MyApplication.isInternetWorking(UpdateIDActivity.this)) {
                IdocUpdate();
            }
        }
    }

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("MMM dd yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String formateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = targetFormat.parse(dateString);
            String formattedDate = originalFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
    @SuppressLint("ValidFragment")
    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                    this, year, month, day);
            Date date = new Date();
            date.setMonth(month);
            date.setYear(year);
            date.setDate(day);
            if (date_type==1) {
                dialog.getDatePicker().setMaxDate(new Date().getTime()- 10000);
            }else if (date_type==2)
            {
                dialog.getDatePicker().setMinDate(new Date().getTime()- 10000);
            }
            // Create a new instance of DatePickerDialog and return it
            return dialog;
        }

        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview


            String dayString = day + "", monthString = (month + 1) + "";

            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
            dateString = monthString + "-" + dayString + "-" + year;
            if (date_type == 1) {
                issueDateBtn.setText(formateDate(dateString));
                issue_dateStr = updateDateForServer(dateString);
            } else if (date_type == 2) {
                expiryDateBtn.setText(formateDate(dateString));
                expiry_dateStr = updateDateForServer(dayString);
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {


        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date

                 Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                 dateDialog = new DatePickerDialog(this, datePickerListener,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                Date date = new Date();
                date.setMonth(month);
                date.setYear(year);
                date.setDate(day);
//                if (DATE_DIALOG_ID==1) {
//                    dialog.getDatePicker().setMaxDate(new Date().getTime()- 10000);
//                }else if (DATE_DIALOG_ID==2)
//                {
//                    dialog.getDatePicker().setMinDate(new Date().getTime()- 10000);
//                }
                return dateDialog;


        }
        return null;
    }


    private String dateString;
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {


        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview


            String dayString = day + "", monthString = (month + 1) + "";

            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
            dateString = monthString + "-" + dayString + "-" + year;
            if (date_type == 1) {
                issueDateBtn.setText(formateDate(dateString));
                issue_dateStr = updateDateForServer(dateString);
            } else if (date_type == 2) {
                expiryDateBtn.setText(formateDate(dateString));
                expiry_dateStr = updateDateForServer(dayString);
            }

        }
    };


    private String updateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY) {

            try {

                System.out.println("on activity result gallery");
                InputStream inputStream = getContentResolver()
                        .openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(
                        mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {

            startCropImage();

        } else if (requestCode == REQUEST_CODE_CROP_IMAGE) {

            System.out.println("on activity result crop");
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }


            imagePath = mFileTemp.getPath();
            if (frontimagenum_backimagenum==8) {
                bitmap = BitmapFactory.decodeFile(imagePath);
                MyApplication.getApplication().loader.displayImage("file://"+ imagePath, selectImage, MyApplication.options);
            }else if (frontimagenum_backimagenum==9)
            {
                bitmapBack = BitmapFactory.decodeFile(imagePath);
                MyApplication.getApplication().loader.displayImage("file://" + imagePath, selectImageBack, MyApplication.options);
            }


        }
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public Uri getOutputMediaFileUri() {

        return Uri.fromFile(mFileTemp);
    }

    private void startCropImage() {
        try {
            System.out.println("on activity result startcrop functions");
            Intent intent = new Intent(UpdateIDActivity.this, CropImage.class);
            intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
            intent.putExtra(CropImage.SCALE, true);

            intent.putExtra(CropImage.ASPECT_X, 1);
            intent.putExtra(CropImage.ASPECT_Y, 1);

            startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private void setimagepath() {

        String fileName = "IMG_"
                + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())
                .toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {

            File sdIconStorageDir = new File(
                    Environment.getExternalStorageDirectory() + "/"
                            + getResources().getString(R.string.app_name) + "/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory()
                    + "/" + getResources().getString(R.string.app_name) + "/",
                    fileName);
        } else {
            mFileTemp = new File(UpdateIDActivity.this.getFilesDir(), fileName);
        }
    }

    private void IdocUpdate() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(UpdateIDActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(UpdateIDActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));

            JSONObject aceIDocObj = new JSONObject();
            aceIDocObj.put("DocID", idListModel.getDocID());
            aceIDocObj.put("DocType", identityStr);
            aceIDocObj.put("DocNumber", idnumberEdt.getText().toString());
            aceIDocObj.put("DocIssueDate", issue_dateStr);
            aceIDocObj.put("DocExpireDate", expiry_dateStr);
            String image = null;


                if (bitmap != null) {
                    image = getStringImage(bitmap);
                } else {
                    aceIDocObj.put("DocFileUrl", idListModel.getDocFileUrl());
                }

            String backimage = null;


                if (bitmapBack != null) {
                    backimage = getStringImage(bitmapBack);
                } else {
                    aceIDocObj.put("DocFileBackUrl", idListModel.getDocFileBackUrl());
                }




            aceIDocObj.put("DocBodyBack", backimage);
            aceIDocObj.put("DocBody", image);
            object.put("AceIDoc", aceIDocObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(UpdateIDActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceIDocs/IDocUpdate", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null}}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                finish();
                                MyApplication.DATAUPDATESTUTAS = true;
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, UpdateIDActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void attachImage() {
        setimagepath();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                UpdateIDActivity.this);

        // set title
        alertDialogBuilder.setTitle("Choose option");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please select image from ")
                .setCancelable(false)
                .setPositiveButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, closeMdsDB
                                // current activity
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just closeMdsDB
                                // the dialog box and do nothing
                                dialog.cancel();
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }


    private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();

            mImageCaptureUri = Uri.fromFile(mFileTemp);

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);

        } catch (ActivityNotFoundException e) {

            Log.d("", "cannot take picture", e);
        }

    }
}
