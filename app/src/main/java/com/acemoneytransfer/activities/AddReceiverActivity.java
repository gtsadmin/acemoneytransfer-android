package com.acemoneytransfer.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataParser;
import com.acemoneytransfer.model.BankBranchModel;
import com.acemoneytransfer.model.ReceiverModel;
import com.acemoneytransfer.views.CircularImageView;
import com.acemoneytransfer.views.EditTextAvenirLTStdBook;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddReceiverActivity extends AppCompatActivity {

    ArrayList<String> countryList;
    ArrayList<String> isoList = new ArrayList<>();
    HashMap<String, String> country_name = new HashMap<>();
    HashMap<String, String> ios_code = new HashMap<>();
    String iso_code_country = "";
    ReceiverModel receiverModel;
    String update_save_str = "";
    @Bind(R.id.userImgView)
    CircularImageView userImgView;
    @Bind(R.id.imageContainer)
    RelativeLayout imageContainer;
    @Bind(R.id.nameEdt)
    EditTextAvenirLTStdBook nameEdt;
    @Bind(R.id.contactEdt)
    EditTextAvenirLTStdBook contactEdt;
    @Bind(R.id.addressEdt)
    EditTextAvenirLTStdBook addressEdt;
    @Bind(R.id.cityEdt)
    EditTextAvenirLTStdBook cityEdt;
    @Bind(R.id.postalCodeEdt)
    EditTextAvenirLTStdBook postalCodeEdt;
    @Bind(R.id.countrySpinner)
    Spinner countrySpinner;

    @Bind(R.id.accountNumberEdt)
    EditTextAvenirLTStdBook accountNumberEdt;
    @Bind(R.id.saverecipientTxt)
    TextViewAvenirLTStdBook saverecipientTxt;
    Toolbar toolbar;
    @Bind(R.id.backnameSpinner)
    Spinner backnameSpinner;
    @Bind(R.id.branchnameSpinner)
    Spinner branchnameSpinner;
    BankBranchModel bankBranchModel;
    String bank_code = "";
    boolean getbranch_status = true;
    boolean getbranchzero = true;
    @Bind(R.id.bankbranchselectLayout)
    LinearLayout bankbranchselectLayout;
    @Bind(R.id.bankbranchnameEdt)
    EditTextAvenirLTStdBook bankbranchnameEdt;
    @Bind(R.id.bankbranchnameLayout)
    LinearLayout bankbranchnameLayout;
    @Bind(R.id.bankbranchcodeEdt)
    EditTextAvenirLTStdBook bankbranchcodeEdt;
    @Bind(R.id.bankbranchcodeLayout)
    LinearLayout bankbranchcodeLayout;
    HashMap<String, String> branchnamemap = new HashMap<>();
    String branchName = "", branchCode = "";
    @Bind(R.id.ibanEdt)
    EditTextAvenirLTStdBook ibanEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_receiver_layout);
        ButterKnife.bind(this);
        update_save_str = getIntent().getStringExtra("update");
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
//        setData();
        setNationalityArr();
        getCountrylist();
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        if (update_save_str.equalsIgnoreCase("update")) {
            ((TextView) logo.findViewById(R.id.txt)).setText("Update Receiver");
            saverecipientTxt.setText("Update Receiver");
        } else {
            ((TextView) logo.findViewById(R.id.txt)).setText("Add Receiver");
            saverecipientTxt.setText("Save Receiver");
        }
        nameEdt.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z ]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });
        cityEdt.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z ]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        try {
            receiverModel = (ReceiverModel) getIntent().getSerializableExtra("model");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!countrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                    iso_code_country = ios_code.get(countrySpinner.getSelectedItem().toString()).toString();
                    contactEdt.setText("");
                    final String globleCode = MyApplication.countryCodeMap.get(countrySpinner.getSelectedItem().toString());
                    if (receiverModel == null) {

                        contactEdt.setText("+" + globleCode);
                        Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
                    }
                    if (receiverModel != null) {
                        String firstCh = "" + receiverModel.getBenePhone().charAt(0);
                        if (!TextUtils.isEmpty(firstCh)) {

                            if (firstCh.equalsIgnoreCase("+")) {
                                String codebreak=    receiverModel.getBenePhone().substring(globleCode.length(), receiverModel.getBenePhone().length());

//                                final String globleCode = MyApplication.countryCodeMap.get(countrySpinner.getSelectedItem().toString());
                                contactEdt.setText("+" + globleCode);
                                    contactEdt.append(codebreak);
                                } else {

                                    contactEdt.setText("+" + globleCode);
                                    Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
                                }
                            }
                        }
//                    }
//                    contactEdt.addTextChangedListener(new TextWatcher() {
//
//                        @Override
//                        public void onTextChanged(CharSequence s, int start, int before,
//                                                  int count) {
//                            // TODO Auto-generated method stub
//
//                        }
//
//                        @Override
//                        public void beforeTextChanged(CharSequence s, int start, int count,
//                                                      int after) {
//                            // TODO Auto-generated method stub
//
//                        }
//
//                        @Override
//                        public void afterTextChanged(Editable s) {
//                            if (!s.toString().startsWith("+"+globleCode)) {
//                                contactEdt.setText("+"+globleCode);
//                                Selection.setSelection(contactEdt.getText(), contactEdt
//                                        .getText().length());
//
//                            }
//
//                        }
//
//                    });
                    if (MyApplication.isInternetWorking(AddReceiverActivity.this)) {
                        getCountryBankList(iso_code_country);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();


    }

    private void setData() {


        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(AddReceiverActivity.this);
        dbHelper.openDataBase();
        countryList = dbHelper.getCountryList(SqlLiteDbHelper.NAME);
        isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
        for (int i = 0; i < countryList.size(); i++) {
            country_name.put(isoList.get(i), countryList.get(i));
            ios_code.put(countryList.get(i), isoList.get(i));
        }


    }

    @OnClick(R.id.saverecipientTxt)
    public void saverecipientTxt() {


        saveReciepiet();

    }

    private void saveReciepiet() {
        if (MyApplication.isInternetWorking(AddReceiverActivity.this)) {
//            if (!isNumeric(nameEdt.getText().toString()))
//            {
//                nameEdt.setError("Please Enter Alphabetic");
//                nameEdt.requestFocus();
//            }else
            if (TextUtils.isEmpty(nameEdt.getText().toString())) {
                nameEdt.setError("Please Enter Name");
                nameEdt.requestFocus();
            } else if (TextUtils.isEmpty(contactEdt.getText().toString())) {
                contactEdt.setError("Please Contact Number");
                contactEdt.requestFocus();
            } else if (countrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                Toast.makeText(AddReceiverActivity.this, "Please select country", Toast.LENGTH_SHORT).show();
            } else {
                if (update_save_str.equalsIgnoreCase("update")) {
                    if (MyApplication.isInternetWorking(AddReceiverActivity.this)) {
                        updateReceiver();
                    }
                } else {
                    if (MyApplication.isInternetWorking(AddReceiverActivity.this)) {
                        addReceiver();
                    }
                }
            }
        } else {
            Toast.makeText(AddReceiverActivity.this, "Please connect network", Toast.LENGTH_SHORT).show();
        }
    }

    //    public boolean isNumeric(String s) {
//        boolean isChar = ;
//        return isChar;
//    }
//    public static boolean isNumeric(String str)
//    {
//        NumberFormat formatter = NumberFormat.getInstance();
//        ParsePosition pos = new ParsePosition(0);
//        formatter.parse(str, pos);
//
//        return str.length() == pos.getIndex();
//    }
//    public boolean isNumeric(String s) {
//        return s.matches("[-+]?\\d*\\.?\\d+");
//
//    }
    public void setNationalityArr() {
        String selectbankstr[] = {"Select bank"};
        String selectbranchstr[] = {"Select branch"};
        ArrayAdapter<String> spinnerbankArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, selectbankstr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        spinnerbankArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

        backnameSpinner.setAdapter(spinnerbankArrayAdapter);
        ArrayAdapter<String> spinnerbranchArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, selectbranchstr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        spinnerbranchArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

        branchnameSpinner.setAdapter(spinnerbranchArrayAdapter);

    }

    private void addReceiver() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        final ProgressDialog dialog = ProgressDialog.show(AddReceiverActivity.this, "", "Please wait...");
        MyApplication.getApplication().hideSoftKeyBoard(AddReceiverActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


            JSONObject acebeneObject = new JSONObject();
            acebeneObject.put("BeneName", nameEdt.getText().toString());
            acebeneObject.put("BeneAddress", addressEdt.getText().toString());
            acebeneObject.put("BeneCity", cityEdt.getText().toString());
            acebeneObject.put("BenePostCode", postalCodeEdt.getText().toString());
            acebeneObject.put("BeneCountryIsoCode", iso_code_country);
            acebeneObject.put("BenePhone", contactEdt.getText().toString());
            if (!backnameSpinner.getSelectedItem().toString().equalsIgnoreCase("Select bank")) {
                acebeneObject.put("BeneBankName", backnameSpinner.getSelectedItem().toString());
            }else
            {
                acebeneObject.put("BeneBankName", "");
            }
//            if (!branchnameSpinner.getSelectedItem().toString().equalsIgnoreCase("Select branch")) {
//
//                acebeneObject.put("BeneBranchName", branchnameSpinner.getSelectedItem().toString());
//            }
            acebeneObject.put("BeneAccountNumber", accountNumberEdt.getText().toString());

            acebeneObject.put("BeneBankCode", bank_code);
            if (bankBranchModel != null) {
                acebeneObject.put("BeneBankBranchCode", branchCode);
                acebeneObject.put("BeneBranchName", branchName);
            } else {
                acebeneObject.put("BeneBankBranchCode", bankbranchcodeEdt.getText().toString());
                acebeneObject.put("BeneBranchName", bankbranchnameEdt.getText().toString());
            }
            object.put("AceBene", acebeneObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(AddReceiverActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/BeneInsert", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            System.out.println("response>>> " + response.toString());

                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                finish();
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, AddReceiverActivity.this);
                            }else
                            {

                                AlertDialog.Builder builder = new AlertDialog.Builder(AddReceiverActivity.this);
                                builder.setMessage(message)
                                        .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                              dialog.dismiss();

                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        dialog.dismiss();
                    }
                });
        requestQueue.add(jsObjRequest);
    }


    private void updateReceiver() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
//        MyApplication.getApplication().showProgressDialog(AddReceiverActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(AddReceiverActivity.this);
        final ProgressDialog dialog = ProgressDialog.show(AddReceiverActivity.this, "", "Please wait...");

        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BeneID", receiverModel.getBeneID());

            JSONObject acebeneObject = new JSONObject();
            acebeneObject.put("BeneID", receiverModel.getBeneID());
            acebeneObject.put("BeneName", nameEdt.getText().toString());
            acebeneObject.put("BeneAddress", addressEdt.getText().toString());
            acebeneObject.put("BeneCity", cityEdt.getText().toString());
            acebeneObject.put("BenePostCode", postalCodeEdt.getText().toString());
            acebeneObject.put("BeneCountryIsoCode", iso_code_country);
            acebeneObject.put("BenePhone", contactEdt.getText().toString());
            acebeneObject.put("BeneBankName", backnameSpinner.getSelectedItem().toString());

            acebeneObject.put("BeneAccountNumber", accountNumberEdt.getText().toString());


            acebeneObject.put("BeneBankCode", bank_code);
            if (bankBranchModel != null) {
                acebeneObject.put("BeneBankBranchCode", branchCode);
                acebeneObject.put("BeneBranchName", branchName);
            } else {
                acebeneObject.put("BeneBankBranchCode", bankbranchcodeEdt.getText().toString());
                acebeneObject.put("BeneBranchName", bankbranchnameEdt.getText().toString());
            }
            object.put("AceBene", acebeneObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(AddReceiverActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/BeneUpdate", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                finish();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        dialog.dismiss();
                    }
                });
        requestQueue.add(jsObjRequest);
    }

    private void setInformation() {
        nameEdt.append(receiverModel.getBeneName());
        addressEdt.append(receiverModel.getBeneAddress());
        cityEdt.append(receiverModel.getBeneCity());
        postalCodeEdt.append(receiverModel.getBenePostCode());
        countrySpinner.setSelection(countryList.indexOf(country_name.get(receiverModel.getBeneCountryIsoCode()).toString()));
//        final String globleCode = MyApplication.countryCodeMap.get(countrySpinner.getSelectedItem().toString());
//        contactEdt.setText("+" + globleCode);
//        Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
//        contactEdt.append(receiverModel.getBenePhone());

//        backnameEdt.setText(receiverModel.getBeneBankName());
//        branchnameEdt.setText(receiverModel.getBeneBranchName());
        accountNumberEdt.append(receiverModel.getBeneAccountNumber());
//        if (receiverModel!=null)
//        {
//            bankbranchcodeEdt.append(receiverModel.getBeneBankBranchCode());
//            bankbranchnameEdt.append(receiverModel.getBeneBranchName());
//        }


    }

    private void getCountrylist() {
        countryList = new ArrayList<>();
        MyApplication.getApplication().showProgressDialog(AddReceiverActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(AddReceiverActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(AddReceiverActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetReceivingCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceReceivingCountryList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryList.add(sub_obj.getString("CountryName"));
                                    ios_code.put(sub_obj.getString("CountryName"), sub_obj.getString("Iso3Code"));
                                    country_name.put(sub_obj.getString("Iso3Code"), sub_obj.getString("CountryName"));
                                }
                                Collections.reverse(countryList);
                                countryList.add("Select country");
                                Collections.reverse(countryList);
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(AddReceiverActivity.this, R.layout.spinner_layout, countryList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

                                countrySpinner.setAdapter(spinnerArrayAdapter);

                                MyApplication.getApplication().setCountryList(countryList);
                                MyApplication.getApplication().setIos_code(ios_code);
//                                if (!Constants.current_country.equalsIgnoreCase("")) {
//                                    nationalitySpinner.setSelection(countryList.indexOf(Constants.current_country));
//                                }

                                countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (!countrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                                            contactEdt.setText("");
                                            iso_code_country = ios_code.get(countrySpinner.getSelectedItem().toString()).toString();
                                            final String globleCode = MyApplication.countryCodeMap.get(countrySpinner.getSelectedItem().toString());
                                            if (receiverModel == null) {

                                                contactEdt.setText("+" + globleCode);
                                                Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
                                            }
                                            if (receiverModel != null) {
                                                String firstCh = "" + receiverModel.getBenePhone().charAt(0);
                                                if (!TextUtils.isEmpty(firstCh)) {

                                                    if (firstCh.equalsIgnoreCase("+")) {
                                                        String codebreak=    receiverModel.getBenePhone().substring(globleCode.length(), receiverModel.getBenePhone().length());

                                                        contactEdt.setText("+" + globleCode);
                                                        contactEdt.append(codebreak);
                                                    } else {

                                                        contactEdt.setText("+" + globleCode);
                                                        Selection.setSelection(contactEdt.getText(), contactEdt.getText().length());
                                                    }
                                                }
                                            }
                                            if (MyApplication.isInternetWorking(AddReceiverActivity.this)) {
                                                getCountryBankList(iso_code_country);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, AddReceiverActivity.this);
                            }
                            setInformation();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getCountryBankList(String CountryIso3Code) {
        final ProgressDialog dialog = ProgressDialog.show(AddReceiverActivity.this, "", "Please wait...");
        MyApplication.getApplication().hideSoftKeyBoard(AddReceiverActivity.this);
        final HashMap<String, String> bank_codeMap = new HashMap<>();
        final ArrayList<String> countryBankList = new ArrayList<>();
        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("ReceivingCountryIso3Code", CountryIso3Code);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(AddReceiverActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetCountryBankList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceCountryBankList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryBankList.add(sub_obj.getString("BankName"));
                                    bank_codeMap.put(sub_obj.getString("BankName"), sub_obj.getString("BankCode"));
                                }
                                if (aceCountryJsonArray.length() == 0) {
                                    Toast.makeText(AddReceiverActivity.this, "No banks", Toast.LENGTH_SHORT).show();
                                }
                                countryBankList.add("Select bank");
                                Collections.reverse(countryBankList);
                                ArrayAdapter<String> companyArrayAdapter = new ArrayAdapter<String>(AddReceiverActivity.this, R.layout.spinner_layout, countryBankList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                companyArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                backnameSpinner.setAdapter(companyArrayAdapter);
                                if (receiverModel != null) {
                                    if (!receiverModel.getBeneBankName().equalsIgnoreCase("")) {
                                        backnameSpinner.setSelection(countryBankList.indexOf(receiverModel.getBeneBankName()));
                                    }
                                }
                                MyApplication.getApplication().setCountryBankList(countryBankList);
                                backnameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (!backnameSpinner.getSelectedItem().toString().equalsIgnoreCase("Select bank")) {

                                            bankbranchcodeEdt.setText("");
                                            bankbranchnameEdt.setText("");
                                            bank_code = bank_codeMap.get(backnameSpinner.getSelectedItem().toString()).toString();
                                            if (MyApplication.isInternetWorking(AddReceiverActivity.this)) {
                                                GetBankBranchList(bank_code);
                                            }
                                        }

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        dialog.dismiss();
                    }
                });
        requestQueue.add(jsObjRequest);
    }

    boolean getbranchNameCode = true;

    private void GetBankBranchList(String bank_code) {
//        MyApplication.getApplication().showProgressDialog(ReceivingMethodActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(AddReceiverActivity.this);
        final ProgressDialog dialog = ProgressDialog.show(AddReceiverActivity.this, "", "Please wait...");

        final ArrayList<String> countryBankBranchList = new ArrayList<>();

        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BankCode", bank_code);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(AddReceiverActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetBankBranchList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            System.out.println("response>>> " + response.toString());

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceBankBranchList")) {
                                    JSONArray aceCountryJsonArray = response.getJSONArray("AceBankBranchList");
                                    final ArrayList<BankBranchModel> bankBranchModels = new DataParser(0, aceCountryJsonArray).getBankBranch();

                                    for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                        JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                        countryBankBranchList.add(sub_obj.getString("BranchName"));
                                        branchnamemap.put(sub_obj.getString("BranchName"), sub_obj.getString("BranchCode"));

                                    }
                                    if (aceCountryJsonArray.length() > 0) {

                                        branchnameSpinner.setVisibility(View.VISIBLE);
                                        bankbranchselectLayout.setVisibility(View.VISIBLE);
                                        bankbranchcodeLayout.setVisibility(View.GONE);
                                        bankbranchnameLayout.setVisibility(View.GONE);
                                    } else {


                                        bankbranchselectLayout.setVisibility(View.GONE);
                                        bankbranchcodeLayout.setVisibility(View.VISIBLE);
                                        bankbranchnameLayout.setVisibility(View.VISIBLE);
                                        if (getbranchNameCode) {
                                            bankbranchcodeEdt.setText(receiverModel.getBeneBankBranchCode());
                                            bankbranchnameEdt.setText(receiverModel.getBeneBranchName());
                                            getbranchNameCode = false;
                                        }
                                        if (TextUtils.isEmpty(bankbranchnameEdt.getText().toString()) && TextUtils.isEmpty(bankbranchcodeEdt.getText().toString())) {

                                            MyApplication.popNoBranch(AddReceiverActivity.this);
                                        }
                                    }
                                    countryBankBranchList.add("Select branch");
                                    Collections.reverse(countryBankBranchList);
                                    ArrayAdapter<String> branchArrayAdapter = new ArrayAdapter<String>(AddReceiverActivity.this, R.layout.spinner_layout, countryBankBranchList) {
                                        public View getView(int position, View convertView,
                                                            ViewGroup parent) {
                                            View v = super.getView(position, convertView, parent);


                                            if (position == 0) {

                                                ((TextView) v).setTextColor(getResources()
                                                        .getColorStateList(R.color.gray));
                                            } else {

                                                ((TextView) v).setTextColor(getResources()
                                                        .getColorStateList(R.color.black));
                                            }
                                            return v;
                                        }

                                        public View getDropDownView(int position, View convertView,
                                                                    ViewGroup parent) {
                                            View v = super.getDropDownView(position, convertView,
                                                    parent);


                                            ((TextView) v).setTextColor(Color.BLACK);


                                            return v;
                                        }
                                    }; //selected item will look like a spinner set from XML
                                    branchArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                    branchnameSpinner.setAdapter(branchArrayAdapter);


                                    MyApplication.getApplication().setCountryBankBranchList(countryBankBranchList);


                                    if (receiverModel != null && getbranch_status) {

                                        if (!receiverModel.getBeneBankBranchCode().equalsIgnoreCase("")) {
                                            branchnameSpinner.setSelection(countryBankBranchList.indexOf(receiverModel.getBeneBranchName()));
                                        }
                                    }
                                    branchnameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            try {
                                                if (!branchnameSpinner.getSelectedItem().toString().equalsIgnoreCase("Select branch")) {
                                                    getbranch_status = false;
                                                    branchName = branchnameSpinner.getSelectedItem().toString();
                                                    branchCode = branchnamemap.get(branchName);
                                                    bankBranchModel = bankBranchModels.get(countryBankBranchList.indexOf(branchnameSpinner.getSelectedItem().toString()));
                                                }
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }

                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        dialog.dismiss();
                    }
                });
        requestQueue.add(jsObjRequest);
    }


}
