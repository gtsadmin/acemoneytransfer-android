package com.acemoneytransfer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.model.TransactioListModel;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TransactionDetails extends AppCompatActivity {
    @Bind(R.id.hashaccountnum)
    TextViewAvenirLTStdBook hashaccountnum;
    @Bind(R.id.dateTxt)
    TextViewAvenirLTStdBook dateTxt;
    @Bind(R.id.statusTxt)
    TextViewAvenirLTStdBook statusTxt;
    @Bind(R.id.paymentmethodTxt)
    TextViewAvenirLTStdBook paymentmethodTxt;
    @Bind(R.id.beneficiarynameTxt)
    TextViewAvenirLTStdBook beneficiarynameTxt;
    @Bind(R.id.payernameTxt)
    TextViewAvenirLTStdBook payernameTxt;
    @Bind(R.id.senderCurrencyTxt)
    TextViewAvenirLTStdBook senderCurrencyTxt;
    @Bind(R.id.senderamountTxt)
    TextViewAvenirLTStdBook senderamountTxt;
    @Bind(R.id.recieverCurrencyTxt)
    TextViewAvenirLTStdBook recieverCurrencyTxt;
    @Bind(R.id.recieveramountTxt)
    TextViewAvenirLTStdBook recieveramountTxt;

    TransactioListModel transactioListModel;

    Toolbar toolbar;
    @Bind(R.id.updateTxt)
    TextViewAvenirLTStdBook updateTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_details_layout);
        ButterKnife.bind(this);
        transactioListModel = (TransactioListModel) getIntent().getExtras().getSerializable("transactionmodel");
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Transaction Details");
//        getSupportActionBar().setTitle("Transaction List");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
        setData();
        updateTxt.setVisibility(View.GONE);
    }

    @OnClick(R.id.updateTxt)
    public void updateTxt() {
        Intent update_trascation = new Intent(TransactionDetails.this,UpdateTranscationActvitity.class);
        update_trascation.putExtra("model",transactioListModel);
        startActivityForResult(update_trascation, 100);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==100)
        {
            if (resultCode==RESULT_OK)
            {
                finish();
            }
        }
    }

    private void setData() {
        hashaccountnum.setText(transactioListModel.getPaymentNumber());
        String date_str = formateDateForServer(transactioListModel.getPaymentDate());
        dateTxt.setText(date_str);
        statusTxt.setText(transactioListModel.getPaymentStatus());
        paymentmethodTxt.setText(transactioListModel.getPaymentMethod());
        beneficiarynameTxt.setText(transactioListModel.getBeneName());
        payernameTxt.setText(transactioListModel.getPayerName());
        senderCurrencyTxt.setText("Sender(" + transactioListModel.getSendingCurrency() + ")");
        senderamountTxt.setText(transactioListModel.getPayInAmount());
        recieverCurrencyTxt.setText("Receiver(" + transactioListModel.getReceivingCurrency() + ")");
        recieveramountTxt.setText(String.format("%.2f", Double.parseDouble(transactioListModel.getPayOutAmount())));
//        recieveramountTxt.setText("" + formatDouble2(Double.parseDouble(transactioListModel.getPayOutAmount())));
    }

    private String formateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = targetFormat.parse(dateString);
            String formattedDate = originalFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    public static double formatDouble2(double value) {
        double formated = 0;
        try {
            value = value / 1000;
            formated = Double.valueOf(String.format("%.2f", value));
        } catch (NumberFormatException e) {
            formated = 0;
        }

        return formated;

    }
}
