package com.acemoneytransfer.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.datacontroller.DataParser;
import com.acemoneytransfer.model.ReceiverModel;
import com.acemoneytransfer.model.ReceivingMethod;
import com.acemoneytransfer.model.TransactioListModel;
import com.acemoneytransfer.model.UserInfoWrapper;
import com.acemoneytransfer.views.EditTextAvenirLTStdBook;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TransactionActivity extends AppCompatActivity {
    Toolbar toolbar;
    @Bind(R.id.select_option)
    Spinner selectOption;
    @Bind(R.id.selectOptionlayout)
    RelativeLayout selectOptionlayout;

    ArrayList<String> select_option_arr = new ArrayList<>();
    @Bind(R.id.transcationListview)
    SwipeMenuListView transcationListview;
    TransactionAdapter transactionAdapter;
    ArrayList<TransactioListModel> transactioListModels;
    Dialog cancelDialog;
    @Bind(R.id.norecordfoundTxt)
    TextViewAvenirLTStdBook norecordfoundTxt;
    @Bind(R.id.createTransactionTxt)
    TextViewAvenirLTStdBook createTransactionTxt;
    UserInfoWrapper userInfoWrapper;
    int paymentStatus;
    @Bind(R.id.swipeleftTxt)
    TextViewAvenirLTStdBook swipeleftTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_layout);
        select_option_arr.add("This Week");
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        paymentStatus = getIntent().getIntExtra("status", -1);
        if (paymentStatus == Constants.COMPLETE) {
            ((TextView) logo.findViewById(R.id.txt)).setText("Complete Transaction List");
        } else if (paymentStatus == Constants.INCOMPLETE) {
            ((TextView) logo.findViewById(R.id.txt)).setText("Incomplete Transaction List");
        } else {
            ((TextView) logo.findViewById(R.id.txt)).setText("Transaction List");
        }


//        getSupportActionBar().setTitle("Transaction List");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        ArrayAdapter<String> selectArrayAdapter = new ArrayAdapter<String>(this, R.layout.transactionspinner_layout, select_option_arr);
        //selected item will look like a spinner set from XML
        selectArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        selectOption.setAdapter(selectArrayAdapter);
        if (MyApplication.isInternetWorking(TransactionActivity.this)) {
            getTranslist();
        }

        createDeletebtn();
        transcationListview.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        if (!transactioListModels.get(position).getPaymentStatus().equalsIgnoreCase("Canceling") && !transactioListModels.get(position).getPaymentStatus().equalsIgnoreCase("cancelled") && !transactioListModels.get(position).getPaymentStatus().equalsIgnoreCase("refunded")) {

                            Intent transactionDetails = new Intent(TransactionActivity.this, TransactionDetails.class);
                            transactionDetails.putExtra("transactionmodel", transactioListModels.get(position));
                            startActivity(transactionDetails);
                        }
                        break;
                    case 1:

                        if (!transactioListModels.get(position).getPaymentStatus().equalsIgnoreCase("Canceling") && !transactioListModels.get(position).getPaymentStatus().equalsIgnoreCase("Canceled") && !transactioListModels.get(position).getPaymentStatus().equalsIgnoreCase("Refunded")) {
                            AlertDialog.Builder buildercancel = new AlertDialog.Builder(TransactionActivity.this);
                            buildercancel.setMessage("Are you sure you want to cancel this transaction?")
                                    .setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (MyApplication.isInternetWorking(TransactionActivity.this)) {
                                        cancelTranscationresonDialog(transactioListModels.get(position).getPaymentNumber(), transactioListModels.get(position), position);
                                    }
                                }
                            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alertcancel = buildercancel.create();
                            alertcancel.show();
                        } else {
                            AlertDialog.Builder buildercancel = new AlertDialog.Builder(TransactionActivity.this);
                            buildercancel.setMessage("This transcation can be not cancel")
                                    .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alertcancel = buildercancel.create();
                            alertcancel.show();
                        }
                        break;

                }

                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!DataManager.getInstance().isUpdateTrans()) {
            if (MyApplication.isInternetWorking(TransactionActivity.this)) {
                getTranslist();
            }
        }
    }

    @OnClick(R.id.createTransactionTxt)
    public void createTransactionTxt() {
        MyApplication.getApplication().produceAnimation(createTransactionTxt);
        try {
            userInfoWrapper = DataManager.getInstance().getUserInfoWrapper();
            if (TextUtils.isEmpty(userInfoWrapper.getAddress()) ||
                    TextUtils.isEmpty(userInfoWrapper.getCity()) ||
                    TextUtils.isEmpty(userInfoWrapper.getCountryIsoCode()) ||
                    TextUtils.isEmpty(userInfoWrapper.getCountryOfBirthIsoCode()) ||
                    TextUtils.isEmpty(userInfoWrapper.getGender()) ||
                    TextUtils.isEmpty(userInfoWrapper.getPostalCode()) ||
                    TextUtils.isEmpty(userInfoWrapper.getCurrencyCode()) ||
                    TextUtils.isEmpty(userInfoWrapper.getFullName())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TransactionActivity.this);
                builder.setMessage("Your transactions will not be processed without complete profile")
                        .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        startActivity(new Intent(TransactionActivity.this, ProfileActivity.class));
                        overridePendingTransition(R.anim.left,
                                R.anim.right);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            } else {

                MyApplication.getApplication().getReceivingMethod().setPayInAmount("");

                startActivity(new Intent(TransactionActivity.this, SendMoneyActivity.class).putExtra("code", Constants.MAINACTIVITYTOSENDMONEY));
                overridePendingTransition(R.anim.left,
                        R.anim.right);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);
        finish();
    }

    private class TransactionAdapter extends BaseAdapter {
        Context context;
        ArrayList<TransactioListModel> transactioListModels;

        public TransactionAdapter(Context context, ArrayList<TransactioListModel> rowItems) {
            this.context = context;
            this.transactioListModels = rowItems;

        }

        /* private view holder class */
        private class ViewHolder {
            TextViewAvenirLTStdBook reveiverTxt, transactionTxt, gbpTxt, statusTxt, sendCurrencyTxt, dateTxt, monthTxt;
            ImageView sendMoneyImgView;
            RelativeLayout reusetransactionLayout;

        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder = null;

            try {
                LayoutInflater mInflater = (LayoutInflater) context
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                if (convertView == null) {
                    convertView = mInflater.inflate(
                            R.layout.transcation_list_layout, null);
                    holder = new ViewHolder();
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                holder.reveiverTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.reveiverTxt);
                holder.reveiverTxt.setText("  " + transactioListModels.get(position).getBeneName());
                holder.transactionTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.transactionTxt);

                holder.sendCurrencyTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.sendCurrencyTxt);
                holder.sendCurrencyTxt.setText("  " + transactioListModels.get(position).getSendingCurrency());
                holder.transactionTxt.setText(" " + transactioListModels.get(position).getPaymentNumber());
                holder.gbpTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.gbpTxt);
                holder.gbpTxt.setText("  " + transactioListModels.get(position).getPayInAmount());
                holder.statusTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.statusTxt);
                holder.statusTxt.setText("  " + transactioListModels.get(position).getPaymentStatus());
                holder.dateTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.dateTxt);
                holder.monthTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.monthTxt);
                String date_str = formateDateForServer(transactioListModels.get(position).getPaymentDate());
                String str[] = date_str.split("-");
                holder.monthTxt.setText(str[0]);
                holder.dateTxt.setText(str[1]);
                holder.reusetransactionLayout = (RelativeLayout) convertView.findViewById(R.id.reusetransactionLayout);
                holder.reusetransactionLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReceivingMethod receivingMethod = new ReceivingMethod();
                        receivingMethod.setPayerId(transactioListModels.get(position).getPayerID());
                        receivingMethod.setSelect_payment_method(transactioListModels.get(position).getPaymentMethod());
                        receivingMethod.setPayInAmount(transactioListModels.get(position).getPayInAmount());
                        Constants.RECEIVINGMETHOD = true;
                        Constants.SENDINGMETHOD = true;
                        MyApplication.getApplication().setReceivingMethod(receivingMethod);
                        DataManager.getInstance().transactionModel.setPayment_method(transactioListModels.get(position).getPaymentMethod());
                        DataManager.getInstance().transactionModel.setPayerBranchcode(transactioListModels.get(position).getPayoutBranchCode());
                        DataManager.getInstance().transactionModel.setReceiver_currency(transactioListModels.get(position).getReceivingCurrency());
                        DataManager.getInstance().transactionModel.setSending_amount(transactioListModels.get(position).getPayInAmount());
                        DataManager.getInstance().transactionModel.setReceiver_amount(transactioListModels.get(position).getPayOutAmount());
                        DataManager.getInstance().transactionModel.setPayerBankname(transactioListModels.get(position).getPayerName());
                        DataManager.getInstance().transactionModel.setPayerBranchname(transactioListModels.get(position).getPayoutBranchName());
                        MyApplication.getApplication().setSending_method(transactioListModels.get(position).getSendingPaymentMethod());

                        DataManager.getInstance().transactionModel.setBene_id(transactioListModels.get(position).getBeneID());

                        if (MyApplication.isInternetWorking(TransactionActivity.this)) {
                            getBeneInfo(transactioListModels.get(position).getBeneID());
                        }


                    }
                });


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return transactioListModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    public void getTranslist() {

        MyApplication.getApplication().showProgressDialog(TransactionActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(TransactionActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(TransactionActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceTrans/TransList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());

                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceTransList")) {
                                    JSONArray aceTransListArr = response.getJSONArray("AceTransList");
                                    transactioListModels = new DataParser(Constants.TRANSCATIONLISTCODE, aceTransListArr).getTranscationlist(paymentStatus);

                                    if (transactioListModels.size() == 0) {
                                        norecordfoundTxt.setVisibility(View.VISIBLE);
                                        createTransactionTxt.setVisibility(View.VISIBLE);
                                        swipeleftTxt.setVisibility(View.GONE);
                                    } else {
                                        norecordfoundTxt.setVisibility(View.GONE);
                                        createTransactionTxt.setVisibility(View.GONE);
                                        swipeleftTxt.setVisibility(View.VISIBLE);
                                    }
                                    transactionAdapter = new TransactionAdapter(TransactionActivity.this, transactioListModels);
                                    transcationListview.setAdapter(transactionAdapter);
                                }

                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, TransactionActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private String formateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = targetFormat.parse(dateString);
            String formattedDate = originalFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void createDeletebtn() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth((int) getResources().getDimension(R.dimen.dim_50));
                // set item title
                openItem.setTitle("Info");
                // set item title fontsize
                openItem.setTitleSize(15);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth((int) getResources().getDimension(R.dimen.dim_50));
                deleteItem.setTitle("Cancel");
                // set item title fontsize
                deleteItem.setTitleSize(15);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

// set creator
        transcationListview.setMenuCreator(creator);
    }

    private void cancelTranscation(String payID, String cancelReason, final TransactioListModel transactioListModel, final int position) {
        MyApplication.getApplication().showProgressDialog(TransactionActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(TransactionActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("PaymentNumber", payID);
            object.put("CancelReason", cancelReason);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(TransactionActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceTrans/Cancel", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());

                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                cancelDialog.dismiss();
                                transactioListModel.setPaymentStatus("Canceling");
                                transactioListModels.set(position, transactioListModel);
                                transactionAdapter.notifyDataSetChanged();
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, TransactionActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void cancelTranscationresonDialog(final String paymentId, final TransactioListModel transactioListModel, final int position) {
        cancelDialog = new Dialog(TransactionActivity.this, R.style.Theme_Custom);
        cancelDialog.setContentView(R.layout.pop_up_canceltranscation);
        cancelDialog.show();
        cancelDialog.setCanceledOnTouchOutside(true);
        final EditTextAvenirLTStdBook enterReasonEdt = (EditTextAvenirLTStdBook) cancelDialog.findViewById(R.id.enterReasonEdt);
        Button closeBtn = (Button) cancelDialog.findViewById(R.id.closeBtn);
        TextViewAvenirLTStdBook sendTxt = (TextViewAvenirLTStdBook) cancelDialog.findViewById(R.id.sendTxt);
        sendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(enterReasonEdt.getText().toString())) {
                    enterReasonEdt.setError("Please Enter Reason");
                    enterReasonEdt.requestFocus();

                } else {
                    cancelTranscation(paymentId, enterReasonEdt.getText().toString(), transactioListModel, position);
                }
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelDialog.dismiss();
            }
        });
    }

    public static double formatDouble2(double value) {
        double formated = 0;
        try {
            value = value / 1000;
            formated = Double.valueOf(String.format("%.2f", value));
        } catch (NumberFormatException e) {
            formated = 0;
        }

        return formated;

    }

    private void getBeneInfo(String beneid) {


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BeneID", beneid);

//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(TransactionActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/GetBene", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONObject sub_obj = null;
                                ReceiverModel model = new ReceiverModel();
                                sub_obj = response.getJSONObject("AceBene");

                                if (sub_obj.has("BeneID")) {
                                    sub_obj.getString("BeneID");
                                    model.setBeneID(sub_obj.getString("BeneID"));
                                }
                                if (sub_obj.has("BeneName")) {
                                    model.setBeneName(sub_obj.getString("BeneName"));
                                }
                                if (sub_obj.has("BeneAddress")) {
                                    model.setBeneAddress(sub_obj.getString("BeneAddress"));
                                }
                                if (sub_obj.has("BeneCity")) {
                                    model.setBeneCity(sub_obj.getString("BeneCity"));
                                }

                                if (sub_obj.has("BenePostCode")) {
                                    model.setBenePostCode(sub_obj.getString("BenePostCode"));
                                }
                                if (sub_obj.has("BeneCountryIsoCode")) {
                                    model.setBeneCountryIsoCode(sub_obj.getString("BeneCountryIsoCode"));
                                }
                                if (sub_obj.has("BenePhone")) {
                                    model.setBenePhone(sub_obj.getString("BenePhone"));

                                }
                                if (sub_obj.has("BeneBankName")) {

                                    model.setBeneBankName(sub_obj.getString("BeneBankName"));
                                    DataManager.getInstance().transactionModel.setReceiverBankName(sub_obj.getString("BeneBankName"));
                                }
                                if (sub_obj.has("BeneBranchName")) {
                                    model.setBeneBranchName(sub_obj.getString("BeneBranchName"));
                                    DataManager.getInstance().transactionModel.setReceiverBranchName(sub_obj.getString("BeneBranchName"));

                                }
                                if (sub_obj.has("BeneAccountNumber")) {
                                    model.setBeneAccountNumber(sub_obj.getString("BeneAccountNumber"));

                                }

                                if (sub_obj.has("BeneBankCode")) {
                                    sub_obj.getString("BeneBankCode");
                                    DataManager.getInstance().transactionModel.setReceiverBankCode(sub_obj.getString("BeneBankCode"));
                                }
                                if (sub_obj.has("BeneBankBranchCode")) {
                                    sub_obj.getString("BeneBankBranchCode");
                                    DataManager.getInstance().transactionModel.setReceiverBankBranchCode(sub_obj.getString("BeneBankBranchCode"));
                                }
                                if (sub_obj.has("PayoutBranchCode")) {
//                                    DataManager.getInstance().transactionModel.setPayerBranchcode(sub_obj.getString("PayoutBranchCode"));
                                }
                                DataManager.getInstance().setModel(model);
                                startActivity(new Intent(TransactionActivity.this, SendMoneyActivity.class).putExtra("code", Constants.SENDMONEYTORECEIVERDETAILS));
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, TransactionActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
}
