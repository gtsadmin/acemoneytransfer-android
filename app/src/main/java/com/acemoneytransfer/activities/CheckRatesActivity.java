package com.acemoneytransfer.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.model.ExchangeRateModel;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckRatesActivity extends AppCompatActivity {

    @Bind(R.id.sendingcountrySpinner)
    Spinner sendingcountrySpinner;
    @Bind(R.id.receivecountrySpinner)
    Spinner receivecountrySpinner;
    HashMap<String, String> sendingios_code = new HashMap<>();
    HashMap<String, String> sendingcurrency_code = new HashMap<>();
    HashMap<String, String> receivingios_code = new HashMap<>();
    HashMap<String, String> receivingcurrency_code = new HashMap<>();
    String sendingiso_code_country = "", receivingiso_code_country = "", payerid_str = "";
    ArrayList<ExchangeRateModel> rowItems;
    @Bind(R.id.getrateTxt)
    TextViewAvenirLTStdBook getrateTxt;
    @Bind(R.id.exchangeRateListView)
    ListView exchangeRateListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_activity_layout);
        ButterKnife.bind(this);
        getsendCountrylist();
        getreceiveCountrylist();
    }

    @OnClick(R.id.closeImgBtn)
    public void closeImgBtn() {
        finish();
    }

    @OnClick(R.id.getrateTxt)
    public void getrateTxt() {
        if (receivingiso_code_country.equalsIgnoreCase("")||sendingiso_code_country.equalsIgnoreCase(""))
        {
            Toast.makeText(CheckRatesActivity.this,"Please select country to check rates",Toast.LENGTH_SHORT).show();
        }else {
            getExchangeratelist();
        }
    }

    private void getsendCountrylist() {
        final ArrayList<String> countryList = new ArrayList<>();
        MyApplication.getApplication().showProgressDialog(CheckRatesActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(CheckRatesActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(CheckRatesActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetSendingCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceSendingCountryList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryList.add(sub_obj.getString("CountryName"));
                                    sendingios_code.put(sub_obj.getString("CountryName"), sub_obj.getString("Iso3Code"));
                                    sendingcurrency_code.put(sub_obj.getString("CountryName"), sub_obj.getString("CurrencyIsoCode"));
                                }
                                countryList.add("Select country");
                                Collections.reverse(countryList);
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(CheckRatesActivity.this, R.layout.spinner_layout, countryList); //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                sendingcountrySpinner.setAdapter(spinnerArrayAdapter);

//
                                sendingcountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        try {
                                            if (!sendingcountrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                                                sendingiso_code_country = sendingios_code.get(sendingcountrySpinner.getSelectedItem().toString()).toString();

                                                String country_name = sendingcountrySpinner.getSelectedItem().toString();

                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, CheckRatesActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getExchangeratelist() {
        final ArrayList<ExchangeRateModel> exchangeRateModels = new ArrayList<>();

        MyApplication.getApplication().showProgressDialog(CheckRatesActivity.this);
        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            JSONObject aceExchangeRateListRequestJsonObject = new JSONObject();
            aceExchangeRateListRequestJsonObject.put("SendingCountryIso3Code", sendingiso_code_country);
            aceExchangeRateListRequestJsonObject.put("ReceivingCountryIso3Code", receivingiso_code_country);
            object.put("AceExchangeRateListRequest", aceExchangeRateListRequestJsonObject);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(CheckRatesActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetExchangeRateList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.getApplication().hideProgressDialog();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceExchangeRateListResponce");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject subAceExchangeRateListResponce = aceCountryJsonArray.getJSONObject(i);
                                    ExchangeRateModel exchangeRateModel = new ExchangeRateModel();
                                    exchangeRateModel.setExchangeRate(subAceExchangeRateListResponce.getString("ExchangeRate"));
                                    exchangeRateModel.setPayerName(subAceExchangeRateListResponce.getString("PayerName"));
                                    exchangeRateModel.setRecievingCurrencyyISOCode(subAceExchangeRateListResponce.getString("RecievingCurrencyyISOCode"));
                                    exchangeRateModels.add(exchangeRateModel);

                                }
                                if (exchangeRateModels.size()==0)
                                {
                                    Toast.makeText(CheckRatesActivity.this,"Rates are not available for this country",Toast.LENGTH_SHORT).show();
                                }
                                ExchangerateAdapter exchangerateAdapter = new ExchangerateAdapter(CheckRatesActivity.this, exchangeRateModels);
                                exchangeRateListView.setAdapter(exchangerateAdapter);

                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, CheckRatesActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getreceiveCountrylist() {
        final ArrayList<String> countryList = new ArrayList<>();


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(CheckRatesActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetReceivingCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceReceivingCountryList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryList.add(sub_obj.getString("CountryName"));
                                    receivingios_code.put(sub_obj.getString("CountryName"), sub_obj.getString("Iso3Code"));
                                    receivingcurrency_code.put(sub_obj.getString("CountryName"), sub_obj.getString("CurrencyIsoCode"));
                                }
                                countryList.add("Select country");
                                Collections.reverse(countryList);
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(CheckRatesActivity.this, R.layout.spinner_layout, countryList); //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                receivecountrySpinner.setAdapter(spinnerArrayAdapter);

//                                if (!Constants.current_country.equalsIgnoreCase("")) {
//                                    nationalitySpinner.setSelection(countryList.indexOf(Constants.current_country));
//                                }
                                receivecountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        try {
                                            if (!receivecountrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                                                receivingiso_code_country = receivingios_code.get(receivecountrySpinner.getSelectedItem().toString()).toString();

                                                String country_name = receivecountrySpinner.getSelectedItem().toString();

                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, CheckRatesActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private class ExchangerateAdapter extends BaseAdapter {
        Context context;


        public ExchangerateAdapter(Context context, ArrayList<ExchangeRateModel> rowItems) {
            this.context = context;
            CheckRatesActivity.this.rowItems = rowItems;


        }

        /* private view holder class */
        private class ViewHolder {
            TextViewAvenirLTStdBook payernameTxt, currencycodeTxt, exchangerateTxt;


        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder = null;

            try {
                LayoutInflater mInflater = (LayoutInflater) context
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                if (convertView == null) {
                    convertView = mInflater.inflate(
                            R.layout.exchangerate_list_layout, null);
                    holder = new ViewHolder();
                    holder.payernameTxt = (TextViewAvenirLTStdBook) convertView
                            .findViewById(R.id.payernameTxt);

                    holder.currencycodeTxt = (TextViewAvenirLTStdBook) convertView
                            .findViewById(R.id.currencycodeTxt);
                    holder.exchangerateTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.exchangerateTxt);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                holder.payernameTxt.setText(rowItems.get(position).getPayerName());
                holder.currencycodeTxt.setText(rowItems.get(position).getRecievingCurrencyyISOCode());
                holder.exchangerateTxt.setText(rowItems.get(position).getExchangeRate());


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

    }

}
