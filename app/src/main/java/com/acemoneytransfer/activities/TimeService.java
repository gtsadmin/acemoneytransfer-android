package com.acemoneytransfer.activities;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TimeService extends Service {
    TextView txt;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getTime();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }
    private void getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");


        Date afternoonTime = null;
        Date eveningTime = null;
        Date nightTime = null;
        Date morningTime = null;
        try {
            afternoonTime = dateFormat.parse("11:59 AM");
            eveningTime = dateFormat.parse("04:17 PM");
            nightTime = dateFormat.parse("08:00 PM");
            morningTime = dateFormat.parse("04:00 AM");

            Date afternoonTimeCurrent = dateFormat.parse(dateFormat.format(new Date()));
            Date eveningTimeeCurrent = dateFormat.parse(dateFormat.format(new Date()));
            Date nightTimeCurrent = dateFormat.parse(dateFormat.format(new Date()));
            Date morningTimeCurrent = dateFormat.parse(dateFormat.format(new Date()));

            if (afternoonTimeCurrent.after(afternoonTime) && eveningTimeeCurrent.before(eveningTime)) {
//                homeFragment.welcomeTxt.setText("Good Afternoon !");

            } else if (eveningTimeeCurrent.after(eveningTime) && nightTimeCurrent.before(nightTime))

            {
                Toast.makeText(getApplicationContext(),"Hellooo",Toast.LENGTH_SHORT).show();
//                homeFragment.welcomeTxt.setText("Good Evening !");
            } else if (nightTimeCurrent.after(nightTime) && morningTimeCurrent.before(morningTime)) {
//                homeFragment.welcomeTxt.setText("Good Night !");
            } else if (morningTimeCurrent.after(morningTime) && afternoonTimeCurrent.before(afternoonTime)) {
//                homeFragment.welcomeTxt.setText("Good Morning !");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
