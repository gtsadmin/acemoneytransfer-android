package com.acemoneytransfer.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.datacontroller.DataParser;
import com.acemoneytransfer.model.ReceiverModel;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RecepientListActivity extends AppCompatActivity {


    ArrayList<ReceiverModel> rowItems;
    RecepientAdapter recepientAdapter;
    @Bind(R.id.recepientListView)
    ListView recepientListView;
    @Bind(R.id.listView)
    SwipeMenuListView listView;
    @Bind(R.id.addRecepientTxt)
    TextViewAvenirLTStdBook addRecepientTxt;
    Toolbar toolbar;
    @Bind(R.id.norecordfoundTxt)
    TextViewAvenirLTStdBook norecordfoundTxt;
    String country_code = "";
    ArrayList<String> isoList = new ArrayList<>();
    @Bind(R.id.swipeleftTxt)
    TextViewAvenirLTStdBook swipeleftTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recepient_list);
        ButterKnife.bind(this);


        toolbar = (Toolbar) findViewById(R.id.tool_bar);


        setSupportActionBar(toolbar);

        setData();
        // cat_price = getIntent().getExtras().getString("cat_price");
        toolbar.setNavigationIcon(R.mipmap.icn_close);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        if (getIntent().getExtras().getInt("code") == Constants.MAINACTIVITYTORECEIVERLIST) {
            ((TextView) logo.findViewById(R.id.txt)).setText("Receiver List");
        } else {
            ((TextView) logo.findViewById(R.id.txt)).setText("Select Receiver");
            country_code = getIntent().getStringExtra("isocode");

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        recepientListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(RecepientListActivity.this, RecieverDetailActivity.class));
                RecepientListActivity.this.overridePendingTransition(R.anim.left,
                        R.anim.right);
            }
        });
        createDeletebtn();
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        startActivity(new Intent(RecepientListActivity.this, AddReceiverActivity.class).putExtra("model", rowItems.get(position)).putExtra("update", "update"));
                        RecepientListActivity.this.overridePendingTransition(R.anim.left,
                                R.anim.right);
                        break;
                    case 1:

                        AlertDialog.Builder builder = new AlertDialog.Builder(RecepientListActivity.this);
                        builder.setMessage("Are you sure you want to delete this receiver?")
                                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                deleteBene(rowItems.get(position).getBeneID(), position);
                            }
                        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getRecepietList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    @OnClick(R.id.addRecepientTxt)
    public void addRecepientTxt() {
        startActivity(new Intent(RecepientListActivity.this, AddReceiverActivity.class).putExtra("update", ""));
    }

    private class RecepientAdapter extends BaseAdapter {
        Context context;


        private ArrayList<ReceiverModel> arraylist;

        public RecepientAdapter(Context context, ArrayList<ReceiverModel> rowItems) {
            this.context = context;
            RecepientListActivity.this.rowItems = rowItems;
            this.arraylist = new ArrayList<>();
            this.arraylist.addAll(rowItems);

        }

        /* private view holder class */
        private class ViewHolder {
            TextViewAvenirLTStdBook recepientNameTxt, addressTxt;
            ImageView sendMoneyImgView;

        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder = null;

            try {
                LayoutInflater mInflater = (LayoutInflater) context
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                if (convertView == null) {
                    convertView = mInflater.inflate(
                            R.layout.custom_recepient_item, null);
                    holder = new ViewHolder();
                    holder.recepientNameTxt = (TextViewAvenirLTStdBook) convertView
                            .findViewById(R.id.recepientNameTxt);

                    holder.sendMoneyImgView = (ImageView) convertView
                            .findViewById(R.id.sendMoneyImgView);
                    holder.addressTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.addressTxt);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                if (getIntent().getExtras().getInt("code") == Constants.MAINACTIVITYTORECEIVERLIST) {
                    holder.sendMoneyImgView.setBackgroundResource(R.mipmap.icn_send);
                    holder.sendMoneyImgView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ReceiverModel model = rowItems.get(position);
                            DataManager.getInstance().transactionModel.setReceiverBankAccountNumber(model.getBeneAccountNumber());
                            DataManager.getInstance().transactionModel.setReceiverBranchName(model.getBeneBranchName());
                            DataManager.getInstance().transactionModel.setReceiverBranchName(model.getBeneBankName());
                            DataManager.getInstance().transactionModel.setReceiverBankCode(model.getBeneBankCode());
                            DataManager.getInstance().transactionModel.setReceiverBankBranchCode(model.getBeneBankBranchCode());
                            DataManager.getInstance().transactionModel.setBene_id(model.getBeneID());
//                            if(getIntent().getExtras().getInt("code")==Constants.MAINACTIVITYTORECEIVERLIST)
//                            {
                            DataManager.getInstance().setModel(model);
                            startActivity(new Intent(RecepientListActivity.this, SendMoneyActivity.class).putExtra("code", Constants.RECEIVERLISTTOSENDMONEY));
                            RecepientListActivity.this.overridePendingTransition(R.anim.left,
                                    R.anim.right);
//                            }else {
//                                DataManager.getInstance().setModel(model);
//                                startActivityForResult(new Intent(RecepientListActivity.this, RecieverDetailActivity.class), Constants.TRANSCATIONSUCESS);
//                                RecepientListActivity.this.overridePendingTransition(R.anim.left,
//                                        R.anim.right);
//                            }
                        }
                    });

                } else {

                    holder.sendMoneyImgView.setBackgroundResource(R.mipmap.arrow_next_black);
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ReceiverModel model = rowItems.get(position);
                            DataManager.getInstance().transactionModel.setReceiverBankAccountNumber(model.getBeneAccountNumber());
                            DataManager.getInstance().transactionModel.setReceiverBranchName(model.getBeneBranchName());
                            DataManager.getInstance().transactionModel.setReceiverBranchName(model.getBeneBankName());
                            DataManager.getInstance().transactionModel.setReceiverBankCode(model.getBeneBankCode());
                            DataManager.getInstance().transactionModel.setReceiverBankBranchCode(model.getBeneBankBranchCode());
                            DataManager.getInstance().transactionModel.setBene_id(model.getBeneID());
                            DataManager.getInstance().setModel(model);
                            startActivityForResult(new Intent(RecepientListActivity.this, RecieverDetailActivity.class), Constants.TRANSCATIONSUCESS);
                            RecepientListActivity.this.overridePendingTransition(R.anim.left,
                                    R.anim.right);
                        }
                    });
                }

                holder.addressTxt.setText(rowItems.get(position).getBeneAddress() + "  " + isoList.indexOf(rowItems.get(position).getBeneCountryIsoCode()));
                holder.recepientNameTxt.setText(rowItems.get(position).getBeneName());


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            rowItems.clear();
            if (charText.length() == 0) {
                rowItems.addAll(arraylist);
            } else {
                for (ReceiverModel wp : arraylist) {
                    if (wp.getBeneCountryIsoCode().toLowerCase(Locale.getDefault()).contains(charText)) {
                        rowItems.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
            if (rowItems.size() == 0) {
                norecordfoundTxt.setVisibility(View.VISIBLE);
            } else {
                norecordfoundTxt.setVisibility(View.GONE);
            }
        }
    }

    private void getRecepietList() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(RecepientListActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(RecepientListActivity.this);
        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(RecepientListActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/BeneList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceBeneList")) {
                                    JSONArray acebaneJsonArray = response.getJSONArray("AceBeneList");
                                    ArrayList<ReceiverModel> receiverModels = new DataParser(0, acebaneJsonArray).getReceivers();
                                    if (receiverModels.size() == 0) {
                                        norecordfoundTxt.setVisibility(View.VISIBLE);
                                        swipeleftTxt.setVisibility(View.GONE);

                                    } else {
                                        norecordfoundTxt.setVisibility(View.GONE);
                                        if (getIntent().getExtras().getInt("code") == Constants.MAINACTIVITYTORECEIVERLIST) {
                                            swipeleftTxt.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    recepientAdapter = new RecepientAdapter(RecepientListActivity.this, receiverModels);
                                    listView.setAdapter(recepientAdapter);
                                    recepientAdapter.filter(country_code);
                                }
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, RecepientListActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    public void popMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(RecepientListActivity.this);
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(RecepientListActivity.this, LoginActivity.class));
                finish();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void createDeletebtn() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background

                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth((int) getResources().getDimension(R.dimen.dim_50));
                // set item title
                openItem.setTitle("Update");
                // set item title fontsize
                openItem.setTitleSize(15);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth((int) getResources().getDimension(R.dimen.dim_50));
                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(15);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

// set creator
        listView.setMenuCreator(creator);
    }

    private void deleteBene(String BeneID, final int removepos) {
        MyApplication.getApplication().showProgressDialog(RecepientListActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(RecepientListActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            object.put("BeneID", BeneID);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(RecepientListActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceBene/BeneDelete", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                rowItems.remove(removepos);
                                recepientAdapter.notifyDataSetChanged();

                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, RecepientListActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.TRANSCATIONSUCESS) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }

    }

    public void setData() {
        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(RecepientListActivity.this);

        dbHelper.openDataBase();

        isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
    }
}
