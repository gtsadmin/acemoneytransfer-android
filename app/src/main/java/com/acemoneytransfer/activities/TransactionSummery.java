package com.acemoneytransfer.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.model.ReceivingMethod;
import com.acemoneytransfer.views.StickyScrollView;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TransactionSummery extends AppCompatActivity {
    @Bind(R.id.userNameTxt)
    TextViewAvenirLTStdBook userNameTxt;
    @Bind(R.id.dateTxt)
    TextViewAvenirLTStdBook dateTxt;


    @Bind(R.id.receivetotalamountTxt)
    TextViewAvenirLTStdBook receivetotalamountTxt;
    @Bind(R.id.receivecouncryTxt)
    TextViewAvenirLTStdBook receivecouncryTxt;
    @Bind(R.id.top_linear_layout)
    LinearLayout topLinearLayout;
    @Bind(R.id.companyBankTxt)
    TextViewAvenirLTStdBook companyBankTxt;
    @Bind(R.id.stationTxt)
    TextViewAvenirLTStdBook stationTxt;
    @Bind(R.id.stationAddressTxt)
    TextViewAvenirLTStdBook stationAddressTxt;
    @Bind(R.id.currencyTxt)
    TextViewAvenirLTStdBook currencyTxt;
    @Bind(R.id.rateTxt)
    TextViewAvenirLTStdBook rateTxt;
    @Bind(R.id.amountTxt)
    TextViewAvenirLTStdBook amountTxt;
    @Bind(R.id.dueamountTxt)
    TextViewAvenirLTStdBook dueamountTxt;
    @Bind(R.id.chargesTxt)
    TextViewAvenirLTStdBook chargesTxt;
    @Bind(R.id.adminChargesTxt)
    TextViewAvenirLTStdBook adminChargesTxt;
    @Bind(R.id.totaldueTxt)
    TextViewAvenirLTStdBook totaldueTxt;
    @Bind(R.id.totalpaidTxt)
    TextViewAvenirLTStdBook totalpaidTxt;
    @Bind(R.id.balanceTxt)
    TextViewAvenirLTStdBook balanceTxt;
    @Bind(R.id.nameTxt)
    TextViewAvenirLTStdBook nameTxt;
    @Bind(R.id.addressTxt)
    TextViewAvenirLTStdBook addressTxt;
    @Bind(R.id.relationshipTxt)
    TextViewAvenirLTStdBook relationshipTxt;
    @Bind(R.id.telephoneTxt)
    TextViewAvenirLTStdBook telephoneTxt;
    @Bind(R.id.customeridTxt)
    TextViewAvenirLTStdBook customeridTxt;
    @Bind(R.id.customernameTxt)
    TextViewAvenirLTStdBook customernameTxt;
    @Bind(R.id.customertelephoneTxt)
    TextViewAvenirLTStdBook customertelephoneTxt;
    @Bind(R.id.paymentmethodTxt)
    TextViewAvenirLTStdBook paymentmethodTxt;
    @Bind(R.id.sticky_scroll)
    StickyScrollView stickyScroll;
    @Bind(R.id.submitTxt)
    TextViewAvenirLTStdBook submitTxt;
    @Bind(R.id.cancelTxt)
    TextViewAvenirLTStdBook cancelTxt;
    @Bind(R.id.bottomLayout)
    LinearLayout bottomLayout;
    @Bind(R.id.sendingcouncryTxt)
    TextViewAvenirLTStdBook sendingcouncryTxt;
    @Bind(R.id.sendingamountTxt)
    TextViewAvenirLTStdBook sendingamountTxt;
    private Toolbar toolbar;
    ReceivingMethod receivingMethod;
    Double total_paid_amount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_status_layout);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);


        setSupportActionBar(toolbar);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView)logo.findViewById(R.id.txt)).setText("Transaction Status");
        // cat_price = getIntent().getExtras().getString("cat_price");
        toolbar.setNavigationIcon(R.mipmap.icn_back);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        setData();
        dateTxt.setText(getDate());
    }

    @OnClick(R.id.cancelTxt)
    public void cancelTxt() {
        MyApplication.getApplication().produceAnimation(cancelTxt);
        finish();
    }

    @OnClick(R.id.submitTxt)
    public void submitTxt() {
        MyApplication.getApplication().produceAnimation(submitTxt);
        if (TextUtils.isEmpty(amountTxt.getText().toString()))
        {
            amountTxt.setError("Please Enter Amount");
            amountTxt.requestFocus();
        }else {
            if (MyApplication.isInternetWorking(TransactionSummery.this)) {
               insertTrans();
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    public void setData()

    {
         try {


             companyBankTxt.setText(DataManager.getInstance().transactionModel.getPayerBankname());
             stationTxt.setText(DataManager.getInstance().transactionModel.getPayerBranchname());
             stationAddressTxt.setText(DataManager.getInstance().transactionModel.getStation_Address());
             paymentmethodTxt.setText(DataManager.getInstance().transactionModel.getPayment_method());
             sendingamountTxt.setText(DataManager.getInstance().transactionModel.getSending_amount());
             sendingcouncryTxt.setText(DataManager.getInstance().getUserInfoWrapper().getCurrencyCode());
             receivecouncryTxt.setText(DataManager.getInstance().transactionModel.getReceiver_currency());
             receivetotalamountTxt.setText(DataManager.getInstance().transactionModel.getReceiver_amount());

             currencyTxt.setText(DataManager.getInstance().transactionModel.getReceiver_currency());
//             if (TextUtils.isEmpty(DataManager.getInstance().transactionModel.getRate())) {
             rateTxt.setText(DataManager.getInstance().transactionModel.getRate());
//             }
             amountTxt.setText(DataManager.getInstance().transactionModel.getSending_amount());
             chargesTxt.setText(DataManager.getInstance().transactionModel.getCharges());
             userNameTxt.setText(DataManager.getInstance().getUserInfoWrapper().getFullName());
             nameTxt.setText(DataManager.getInstance().transactionModel.getReceiver_name() + " " + DataManager.getInstance().transactionModel.getReceiver_lastname());
             addressTxt.setText(DataManager.getInstance().transactionModel.getReceiver_address());
             relationshipTxt.setText(DataManager.getInstance().transactionModel.getRelationship());
             telephoneTxt.setText(DataManager.getInstance().transactionModel.getReceiver_contact());
             customernameTxt.setText(DataManager.getInstance().getUserInfoWrapper().getAddress());
             customeridTxt.setText(DataManager.getInstance().getUserInfoWrapper().getFullName());
             customertelephoneTxt.setText(DataManager.getInstance().getUserInfoWrapper().getPhone());
             paymentmethodTxt.setText(MyApplication.getApplication().getSending_method());
             adminChargesTxt.setText(DataManager.getInstance().transactionModel.getCharges());

              total_paid_amount = Double.valueOf(Double.parseDouble(DataManager.getInstance().transactionModel.getSending_amount()) + Double.parseDouble(DataManager.getInstance().transactionModel.getCharges()));
             totalpaidTxt.setText(""+total_paid_amount);
         }catch (Exception ex)
         {
             ex.printStackTrace();
         }


    }

    public void insertTrans() {

        MyApplication.getApplication().showProgressDialog(TransactionSummery.this);
        MyApplication.getApplication().hideSoftKeyBoard(TransactionSummery.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));

            JSONObject aceTransObj = new JSONObject();
            aceTransObj.put("PaymentMethod", DataManager.getInstance().transactionModel.getPayment_method());
            aceTransObj.put("PayoutBranchCode", DataManager.getInstance().transactionModel.getPayerBranchcode());
            aceTransObj.put("PayinCurrencyCode", DataManager.getInstance().getUserInfoWrapper().getCurrencyCode());
            aceTransObj.put("PayoutCurrencyCode", receivecouncryTxt.getText().toString());
            aceTransObj.put("PayinAmount",total_paid_amount);
            aceTransObj.put("PayoutAmount",DataManager.getInstance().transactionModel.getReceiver_amount());
            aceTransObj.put("SendingReason", DataManager.getInstance().transactionModel.getSending_purpose());
            aceTransObj.put("BeneID", DataManager.getInstance().transactionModel.getBene_id());
            aceTransObj.put("SendingPaymentMethod",MyApplication.getApplication().getSending_method());
            if (DataManager.getInstance().transactionModel.getPayment_method().equalsIgnoreCase("Bank")) {
                aceTransObj.put("ReceiverBankAccountNumber", DataManager.getInstance().transactionModel.getReceiverBankAccountNumber());
                aceTransObj.put("ReceiverBankAccountTitle", "");
                aceTransObj.put("ReceiverBankIBAN",DataManager.getInstance().transactionModel.getIbanNumber());
                aceTransObj.put("ReceiverBankCode", DataManager.getInstance().transactionModel.getReceiverBankCode());
                aceTransObj.put("ReceiverBankName", DataManager.getInstance().transactionModel.getReceiverBankName());
                aceTransObj.put("ReceiverBranchName", DataManager.getInstance().transactionModel.getReceiverBranchName());
                aceTransObj.put("ReceiverBankBranchCode", DataManager.getInstance().transactionModel.getReceiverBankBranchCode());
            }else
            {
                aceTransObj.put("ReceiverBankAccountNumber", "");
                aceTransObj.put("ReceiverBankAccountTitle", "");
                aceTransObj.put("ReceiverBankIBAN", "");
                aceTransObj.put("ReceiverBankCode", "");
                aceTransObj.put("ReceiverBankName", "");
                aceTransObj.put("ReceiverBranchName", "");
                aceTransObj.put("ReceiverBankBranchCode", "");
            }
            object.put("AceTrans", aceTransObj);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(TransactionSummery.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL +"aceTrans/Insert", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());

                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONObject aceTransactionObj = response.getJSONObject("AceTransaction");

                                DataManager.getInstance().setPaymentId(aceTransactionObj.getString("PaymentID"));
                                Intent card_selectIntent = new Intent(TransactionSummery.this, CardSelectActivity.class);
                                if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Debit Card")) {
                                    card_selectIntent.putExtra("PaymentID", Constants.debit_url + aceTransactionObj.getString("PaymentID"));
                                    startActivity(card_selectIntent);
                                } else if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Credit Card")) {
                                    card_selectIntent.putExtra("PaymentID", Constants.credit_url + aceTransactionObj.getString("PaymentID"));
                                    startActivity(card_selectIntent);
                                }else
                                {
                                    Intent selectIntent = new Intent(TransactionSummery.this, BankDepositActivity.class);
                                    selectIntent.putExtra("PaymentID",aceTransactionObj.getString("PaymentID"));
                                    startActivity(selectIntent);
                                }



                            }else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, TransactionSummery.this);




                            }

                            else{
                                MyApplication.popErrorMsg("", message, TransactionSummery.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MyApplication.getApplication().hideProgressDialog();
                        System.out.println("error"+error);
                    }
                });

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);


    }

    private String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public void popMessage(String msg, final String paymentId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(TransactionSummery.this);
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                setResult(RESULT_OK);
                if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Debit Card")||MyApplication.getApplication().getSending_method().equalsIgnoreCase("Credit Card")) {
                    Intent card_selectIntent = new Intent(TransactionSummery.this, CardSelectActivity.class);
                    if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Debit Card")) {
                        card_selectIntent.putExtra("PaymentID",Constants.debit_url+paymentId);
                    } else if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Credit Card")) {
                        card_selectIntent.putExtra("PaymentID", Constants.credit_url + paymentId);
                    }
                    startActivity(card_selectIntent);
                }else
                {
                    finish();
                }

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


}
