package com.acemoneytransfer.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataParser;
import com.acemoneytransfer.model.CountryModel;
import com.acemoneytransfer.model.ExchangeRateModel;
import com.acemoneytransfer.views.CircularImageView;
import com.acemoneytransfer.views.EditTextAvenirLTStdBook;
import com.acemoneytransfer.views.RadioButtonAvenirLTStdBook;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginActivity extends AppCompatActivity {


    @Bind(R.id.signInTxt)
    TextViewAvenirLTStdBook signInTxt;
    @Bind(R.id.checkRatesTxt)
    TextViewAvenirLTStdBook checkRatesTxt;
    @Bind(R.id.dntHaveAccTxt)
    TextViewAvenirLTStdBook dntHaveAccTxt;
    @Bind(R.id.orTxt)
    TextViewAvenirLTStdBook orTxt;


    @Bind(R.id.animImageSmall)
    ImageView animImageSmall;
    @Bind(R.id.animImageSmall_ll)
    LinearLayout animImageSmallLl;
    @Bind(R.id.emailPassword_ll)
    LinearLayout emailPassword_ll;
    @Bind(R.id.container)
    LinearLayout container;
    @Bind(R.id.emailEdt)
    EditTextAvenirLTStdBook emailEdt;
    @Bind(R.id.passwordEdt)
    EditTextAvenirLTStdBook passwordEdt;
    @Bind(R.id.forgotPassImg)
    ImageView forgotPassImg;
    Dialog forgot_dialog_box;
    @Bind(R.id.closeImgBtn)
    ImageButton closeImgBtn;
    @Bind(R.id.signupLayout)
    LinearLayout signupLayout;
    @Bind(R.id.userImgView)
    CircularImageView userImgView;
    @Bind(R.id.imageContainer)
    RelativeLayout imageContainer;
    @Bind(R.id.nameEdt)
    EditTextAvenirLTStdBook nameEdt;
    @Bind(R.id.signupemailEdt)
    EditTextAvenirLTStdBook signupemailEdt;
    @Bind(R.id.signuppasswordEdt)
    EditTextAvenirLTStdBook signuppasswordEdt;
    @Bind(R.id.confirmPasswordEdt)
    EditTextAvenirLTStdBook confirmPasswordEdt;
    @Bind(R.id.nationalitySpinner)
    Spinner nationalitySpinner;
    @Bind(R.id.birthdayTxt)
    TextViewAvenirLTStdBook birthdayTxt;
    @Bind(R.id.radioMale)
    RadioButtonAvenirLTStdBook radioMale;
    @Bind(R.id.radioFemale)
    RadioButtonAvenirLTStdBook radioFemale;
    @Bind(R.id.radioSex)
    RadioGroup radioSex;
    @Bind(R.id.signUpTxt)
    ImageButton signUpTxt;
    @Bind(R.id.closecheckrateImgBtn)
    ImageButton closecheckrateImgBtn;
    @Bind(R.id.sendingcountrySpinner)
    Spinner sendingcountrySpinner;
    @Bind(R.id.receivecountrySpinner)
    Spinner receivecountrySpinner;
    @Bind(R.id.getrateTxt)
    TextViewAvenirLTStdBook getrateTxt;
    @Bind(R.id.methodLayout)
    LinearLayout methodLayout;
    @Bind(R.id.exchangeRateListView)
    ListView exchangeRateListView;
    @Bind(R.id.checkrateLayout)
    RelativeLayout checkrateLayout;
    long remainingDays = 0;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    //sign up fields

    private int year;
    private int month;
    private int day;
    DataParser dataParser;
    private final int DATE_DIALOG_ID = 1;

    ArrayList<String> isoList = new ArrayList<>();
    String iso_code_str = "";

    private String genderString = "male", dobString = "";
    ArrayList<CountryModel> countryModels = new ArrayList<>();
    ArrayList<String> countryList = new ArrayList<>();
    HashMap<String, String> iso_country_code = new HashMap<>();

    //check rate views
    HashMap<String, String> sendingios_code = new HashMap<>();
    HashMap<String, String> sendingcurrency_code = new HashMap<>();
    HashMap<String, String> receivingios_code = new HashMap<>();
    HashMap<String, String> receivingcurrency_code = new HashMap<>();
    String sendingiso_code_country = "", receivingiso_code_country = "", payerid_str = "";
    ArrayList<ExchangeRateModel> rowItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

//        if (!MyApplication.getLoginBoolean("login")) {
//            fadAnimation();
//        }
        MyApplication.saveloginBoolean("login", true);
        String text = "<font color=#000000>Don't have an account? </font> <font color=#eb4835>Sign Up</font>";
        dntHaveAccTxt.setText(Html.fromHtml(text));
//        Handler h = new Handler();
//        h.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                showStartPopUp();
//
//            }
//        }, 2500);
        if (MyApplication.isConnectingToInternet(LoginActivity.this)) {
            getsendCountrylist();
        }
        passwordEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        signIn();
                        break;


                }
                return true;
            }
        });

        setData();
//        setNationalityArr();
        nameEdt.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z ]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });

    }

    @OnClick(R.id.checkrateLayout)
    public void checkrateLayout() {

    }
    @OnClick(R.id.signupLayout)
    public void signupLayout() {

    }
    @OnClick(R.id.checkRatesTxt)
    public void checkRatesTxt() {
        // startActivity(new Intent(LoginActivity.this, CheckRatesActivity.class));
        if (receivingcurrency_code.size() == 0) {

            getreceiveCountrylist();
            MyApplication.getApplication().buttomtoUp(checkrateLayout);
        } else {
            MyApplication.getApplication().buttomtoUp(checkrateLayout);
        }


    }

    @OnClick(R.id.birthdayTxt)
    public void birthdayTxt() {

        showDialog(DATE_DIALOG_ID);

    }

    @OnClick(R.id.signUpTxt)
    public void signUpTxt() {
        if (MyApplication.isInternetWorking(LoginActivity.this)) {

            getAge();
//            if (monthsBetween() > 17) {
//                signUp();
//            } else {
//                MyApplication.popErrorMsg("", "Minimum age should be 18 year requared", LoginActivity.this);
//            }
        }
    }

    private void setData() {

//        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(LoginActivity.this);
//        dbHelper.openDataBase();
//        countryList = dbHelper.getCountryList(SqlLiteDbHelper.NAME);
//        isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
//        for (int i = 0; i < countryList.size(); i++) {
//
//            iso_country_code.put(countryList.get(i), isoList.get(i));
//        }

        radioFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    genderString = "female";
            }
        });


        radioMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    genderString = "male";
            }
        });

    }

    private void fadAnimation() {


        Animation fadeIn_emailPassword_ll = new AlphaAnimation(0, 1);
        fadeIn_emailPassword_ll.setInterpolator(new DecelerateInterpolator());
        //add this
        fadeIn_emailPassword_ll.setDuration(3000);


        emailPassword_ll.setAnimation(fadeIn_emailPassword_ll);

        Animation fadeIn_signInTxt = new AlphaAnimation(0, 1);
        fadeIn_signInTxt.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn_signInTxt.setDuration(2000);
        fadeIn_signInTxt.setStartOffset(1000);

        signInTxt.setAnimation(fadeIn_signInTxt);


        Animation fadeIn_orTxt = new AlphaAnimation(0, 1);
        fadeIn_orTxt.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn_orTxt.setDuration(2000);
        fadeIn_orTxt.setStartOffset(1500);

        orTxt.setAnimation(fadeIn_orTxt);

        Animation fadeIn_checkRatesTxt = new AlphaAnimation(0, 1);
        fadeIn_checkRatesTxt.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn_checkRatesTxt.setDuration(2000);
        fadeIn_checkRatesTxt.setStartOffset(2000);

        checkRatesTxt.setAnimation(fadeIn_checkRatesTxt);

        Animation fadeIn_dntHaveAccTxt = new AlphaAnimation(0, 1);
        fadeIn_dntHaveAccTxt.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn_dntHaveAccTxt.setDuration(2000);
        fadeIn_dntHaveAccTxt.setStartOffset(2500);

        dntHaveAccTxt.setAnimation(fadeIn_dntHaveAccTxt);


    }

    private void signIn() {
//         if (TextUtils.isEmpty(MyApplication.getAuthToken("AuthToken"))) {
        if (TextUtils.isEmpty(emailEdt.getText().toString())) {

            Toast.makeText(LoginActivity.this, "Email cannot be empty", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
            Toast.makeText(LoginActivity.this, "Password cannot be empty", Toast.LENGTH_LONG).show();
        } else {
            if (MyApplication.isInternetWorking(LoginActivity.this)) {
                loginUser();
            }

        }
//         }else
//         {
//
//         }
    }

    @OnClick(R.id.forgotPassImg)
    public void forgotPassImg() {
        if (MyApplication.isInternetWorking(LoginActivity.this)) {
            forgotpassDialog();
        }
    }

    @OnClick(R.id.dntHaveAccTxt)
    public void dntHaveAccTxt() {
//        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
//        LoginActivity.this.overridePendingTransition(R.anim.left,
//                R.anim.right);
        MyApplication.getApplication().buttomtoUp(signupLayout);
    }

    @OnClick(R.id.signInTxt)
    public void signInTxt() {
        signIn();
    }

    @OnClick(R.id.closeImgBtn)
    public void closeImgBtn() {
        MyApplication.getApplication().uptoButtom(signupLayout);
    }

    public void showStartPopUp() {
        try {
            final Animation zoomOut = AnimationUtils.loadAnimation(this, R.anim.zoomout);
            final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoomin);
            animImageSmall.startAnimation(zoomOut);
            Handler h1 = new Handler();
            h1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    animImageSmall.clearAnimation();
                    animImageSmall.startAnimation(zoomin);
                    Handler h1 = new Handler();
                    h1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            animImageSmall.clearAnimation();
                            animImageSmall.setVisibility(View.GONE);
                            animImageSmallLl.setVisibility(View.GONE);


//                            Handler h = new Handler();
//                            h.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
                            container.setVisibility(View.VISIBLE);
                            fadAnimation();
//
//                                }
//                            }, 2000);
                        }
                    }, 800);
                }
            }, 500);


        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void loginUser() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(LoginActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(LoginActivity.this);


        JSONObject object = new JSONObject();

        try {
            object.put("Password", passwordEdt.getText().toString());
            object.put("AuthKey", Constants.AUTH_KEY);


            object.put("UserName", emailEdt.getText().toString());


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUser/SignIn", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.getApplication().hideProgressDialog();
                        try {
                            System.out.println("response>>> " + response.toString());
//

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 0 && code == 202) {
                                popMessage("Invalid Username and Password");
                            } else if (rflag == 1 && code == 0) {
                                String AuthToken = response.getString("AuthToken");
                                MyApplication.saveAuthToken("AuthToken", AuthToken);
                                MyApplication.saveUserEmail("email", emailEdt.getText().toString());
                                MyApplication.saveUserInfo("userinfo", response.toString());
                                if (getIntent().getExtras().getInt("login") == 1 || getIntent().getExtras().getInt("login") == 3) {
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                    LoginActivity.this.overridePendingTransition(R.anim.left,
                                            R.anim.right);
                                    finish();
                                } else {
                                    LoginActivity.this.overridePendingTransition(R.anim.left,
                                            R.anim.right);
                                    finish();
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MyApplication.getApplication().hideProgressDialog();
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    public void popMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void forgotpassDialog() {
        forgot_dialog_box = new Dialog(LoginActivity.this, R.style.Theme_Custom);
        forgot_dialog_box.setContentView(R.layout.forgot_dialog_box);
        forgot_dialog_box.show();
        forgot_dialog_box.setCanceledOnTouchOutside(true);
        final EditText emailTxt = (EditText) forgot_dialog_box.findViewById(R.id.emailTxt);
        TextView sendTxt = (TextView) forgot_dialog_box.findViewById(R.id.sendTxt);
        sendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(emailTxt.getText().toString())) {
                    emailTxt.setError("Please enter email");
                    emailTxt.requestFocus();
                } else {
                    if (MyApplication.isInternetWorking(LoginActivity.this)) {
                        forgotpass(emailTxt.getText().toString());
                    }
                }
            }
        });
        ImageView closeImg = (ImageView) forgot_dialog_box.findViewById(R.id.closeImg);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgot_dialog_box.dismiss();
            }
        });

    }

    private void forgotpass(String email) {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(LoginActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(LoginActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);


            object.put("Email", email);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUser/ForgetPassword", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.getApplication().hideProgressDialog();
                        try {
                            System.out.println("response>>> " + response.toString());
//

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 0 && code == 202) {
                                popMessage(message);
                            } else if (rflag == 1 && code == 0) {
                                forgot_dialog_box.dismiss();
                                popMessage(response.getString("Message"));
                            }else
                            {
                                popMessage(message);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MyApplication.getApplication().hideProgressDialog();
//                            hideProgressDialog();
                    }
                });
        requestQueue.add(jsObjRequest);
    }

    private void signUp() {
        if (MyApplication.isInternetWorking(LoginActivity.this)) {

            if (TextUtils.isEmpty(signupemailEdt.getText().toString())) {

                Toast.makeText(LoginActivity.this, "Email cannot be empty", Toast.LENGTH_LONG).show();
            } else if (TextUtils.isEmpty(signuppasswordEdt.getText().toString())) {
                Toast.makeText(LoginActivity.this, "Password cannot be empty", Toast.LENGTH_LONG).show();
            } else if (TextUtils.isEmpty(confirmPasswordEdt.getText().toString())) {
                Toast.makeText(LoginActivity.this, "Confirm password cannot be empty", Toast.LENGTH_LONG).show();
            } else if (!signuppasswordEdt.getText().toString().equalsIgnoreCase(confirmPasswordEdt.getText().toString())) {
                Toast.makeText(LoginActivity.this, "Passwords do not match", Toast.LENGTH_LONG).show();
            } else if (signuppasswordEdt.getText().toString().length() < 6) {
                Toast.makeText(LoginActivity.this, "Password too short", Toast.LENGTH_LONG).show();
            } else if (!Patterns.EMAIL_ADDRESS.matcher(signupemailEdt.getText().toString()).matches()) {
                Toast.makeText(LoginActivity.this, "Invaild email", Toast.LENGTH_LONG).show();
            } else if (TextUtils.isEmpty(iso_code_str)) {
                Toast.makeText(LoginActivity.this, "Please select country", Toast.LENGTH_LONG).show();
            } else {

                registerUser();
            }
        }
    }

    private void registerUser() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(LoginActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(LoginActivity.this);


        JSONObject object = new JSONObject();

        try {
            object.put("Password", signuppasswordEdt.getText().toString());
            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", "");

            object.put("Username", "");

            JSONObject userObject = new JSONObject();
            userObject.put("FullName", nameEdt.getText().toString());
            userObject.put("Email", signupemailEdt.getText().toString());

            userObject.put("Phone", "");
            userObject.put("DOB", birthdayTxt.getText().toString());

            userObject.put("Gender", genderString);
            userObject.put("CountryIsoCode", iso_code_str);

            userObject.put("NationalityIsoCode", "");

            userObject.put("CountryOfBirthIsoCode", "");
            userObject.put("Address", "");
            userObject.put("City", "");
            userObject.put("PostalCode", "");


            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUser/SignUp", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null}}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                String AuthToken = response.getString("AuthToken");
                                MyApplication.saveAuthToken("AuthToken", AuthToken);
                                MyApplication.saveUserEmail("email", signupemailEdt.getText().toString());
                                MyApplication.saveUserInfo("userinfo", response.toString());

                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                LoginActivity.this.overridePendingTransition(R.anim.left,
                                        R.anim.right);


                                finish();
                            } else {
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();

        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date

                return new DatePickerDialog(this, datePickerListener,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));


        }
        return null;
    }

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("MMM dd yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String formateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = targetFormat.parse(dateString);
            String formattedDate = originalFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String dateString;
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {


        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview


            String dayString = day + "", monthString = (month + 1) + "";

            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
            dateString = monthString + "-" + dayString + "-" + year;
            birthdayTxt.setText(formateDate(dateString));
            dobString = updateDateForServer(dateString);

        }
    };

    @Override
    public void onBackPressed() {
        if (signupLayout.isShown()) {
            MyApplication.getApplication().uptoButtom(signupLayout);
        } else if (checkrateLayout.isShown()) {
            MyApplication.getApplication().uptoButtom(checkrateLayout);

        } else {
            overridePendingTransition(R.anim.back_in, R.anim.back_out);

            finish();
        }
    }

    public void setNationalityArr() {
//        Collections.reverse(countryList);
//        countryList.add("Select your Country");
//        Collections.reverse(countryList);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, countryList) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        nationalitySpinner.setAdapter(spinnerArrayAdapter);
        nationalitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                    iso_code_str = sendingios_code.get(nationalitySpinner.getSelectedItem().toString()).toString();
                    System.out.println("iso_code_str" + iso_code_str);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        birthPlaceSpinner.setAdapter(spinnerArrayAdapter);
//        countrySpinner.setAdapter(spinnerArrayAdapter);

    }

    private String updateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @OnClick(R.id.closecheckrateImgBtn)
    public void closecheckrateImgBtn() {
        MyApplication.getApplication().uptoButtom(checkrateLayout);
    }

    @OnClick(R.id.getrateTxt)
    public void getrateTxt() {
        if (receivingiso_code_country.equalsIgnoreCase("") || sendingiso_code_country.equalsIgnoreCase("")) {
            Toast.makeText(LoginActivity.this, "Please select country", Toast.LENGTH_SHORT).show();
        } else {
            getExchangeratelist();
        }
    }

    private void getsendCountrylist() {

        MyApplication.getApplication().showProgressDialog(LoginActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(LoginActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetSendingCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceSendingCountryList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryList.add(sub_obj.getString("CountryName"));
                                    sendingios_code.put(sub_obj.getString("CountryName"), sub_obj.getString("Iso3Code"));
                                    sendingcurrency_code.put(sub_obj.getString("CountryName"), sub_obj.getString("CurrencyIsoCode"));
                                }
                                Collections.reverse(countryList);
                                countryList.add("Select country");
                                Collections.reverse(countryList);
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(LoginActivity.this, R.layout.spinner_layout, countryList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                sendingcountrySpinner.setAdapter(spinnerArrayAdapter);

//
                                sendingcountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        try {
                                            if (!sendingcountrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                                                sendingiso_code_country = sendingios_code.get(sendingcountrySpinner.getSelectedItem().toString()).toString();

                                                String country_name = sendingcountrySpinner.getSelectedItem().toString();

                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(LoginActivity.this, R.layout.spinner_layout, countryList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                nationalitySpinner.setAdapter(spinnerArrayAdapter1);
                                nationalitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (!nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                                            iso_code_str = sendingios_code.get(nationalitySpinner.getSelectedItem().toString()).toString();
                                            System.out.println("iso_code_str" + iso_code_str);
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, LoginActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getExchangeratelist() {
        final ArrayList<ExchangeRateModel> exchangeRateModels = new ArrayList<>();

        MyApplication.getApplication().showProgressDialog(LoginActivity.this);
        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            JSONObject aceExchangeRateListRequestJsonObject = new JSONObject();
            aceExchangeRateListRequestJsonObject.put("SendingCountryIso3Code", sendingiso_code_country);
            aceExchangeRateListRequestJsonObject.put("ReceivingCountryIso3Code", receivingiso_code_country);
            object.put("AceExchangeRateListRequest", aceExchangeRateListRequestJsonObject);


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetExchangeRateList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.getApplication().hideProgressDialog();
                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceExchangeRateListResponce");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject subAceExchangeRateListResponce = aceCountryJsonArray.getJSONObject(i);
                                    ExchangeRateModel exchangeRateModel = new ExchangeRateModel();
                                    exchangeRateModel.setExchangeRate(subAceExchangeRateListResponce.getString("ExchangeRate"));
                                    exchangeRateModel.setPayerName(subAceExchangeRateListResponce.getString("PayerName"));
                                    exchangeRateModel.setRecievingCurrencyyISOCode(subAceExchangeRateListResponce.getString("RecievingCurrencyyISOCode"));
                                    exchangeRateModels.add(exchangeRateModel);

                                }
                                if (exchangeRateModels.size()==0)
                                {
                                    Toast.makeText(LoginActivity.this,"Rates are not available for this country",Toast.LENGTH_SHORT).show();
                                }
                                ExchangerateAdapter exchangerateAdapter = new ExchangerateAdapter(LoginActivity.this, exchangeRateModels);
                                exchangeRateListView.setAdapter(exchangerateAdapter);

                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, LoginActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getreceiveCountrylist() {
        final ArrayList<String> countryList = new ArrayList<>();


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


//            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetReceivingCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            System.out.println("response>>> " + response.toString());
//                            {"AceResult":{"Rflag":1,"Code":0,"Message":"Successfully done","Description":null},"AuthToken":"edc34852-6d5a-44c2-8e00-dcbd251ee4ff"}

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONArray aceCountryJsonArray = response.getJSONArray("AceReceivingCountryList");
                                for (int i = 0; i < aceCountryJsonArray.length(); i++) {
                                    JSONObject sub_obj = aceCountryJsonArray.getJSONObject(i);
                                    countryList.add(sub_obj.getString("CountryName"));
                                    receivingios_code.put(sub_obj.getString("CountryName"), sub_obj.getString("Iso3Code"));
                                    receivingcurrency_code.put(sub_obj.getString("CountryName"), sub_obj.getString("CurrencyIsoCode"));
                                }
                                Collections.reverse(countryList);
                                countryList.add("Select country");
                                Collections.reverse(countryList);
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(LoginActivity.this, R.layout.spinner_layout, countryList) {
                                    public View getView(int position, View convertView,
                                                        ViewGroup parent) {
                                        View v = super.getView(position, convertView, parent);


                                        if (position == 0) {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.gray));
                                        } else {

                                            ((TextView) v).setTextColor(getResources()
                                                    .getColorStateList(R.color.black));
                                        }
                                        return v;
                                    }

                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View v = super.getDropDownView(position, convertView,
                                                parent);


                                        ((TextView) v).setTextColor(Color.BLACK);


                                        return v;
                                    }
                                }; //selected item will look like a spinner set from XML
                                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
                                receivecountrySpinner.setAdapter(spinnerArrayAdapter);

//                                if (!Constants.current_country.equalsIgnoreCase("")) {
//                                    nationalitySpinner.setSelection(countryList.indexOf(Constants.current_country));
//                                }
                                receivecountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        try {
                                            if (!receivecountrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                                                receivingiso_code_country = receivingios_code.get(receivecountrySpinner.getSelectedItem().toString()).toString();

                                                String country_name = receivecountrySpinner.getSelectedItem().toString();

                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, LoginActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private class ExchangerateAdapter extends BaseAdapter {
        Context context;


        public ExchangerateAdapter(Context context, ArrayList<ExchangeRateModel> rowItems) {
            this.context = context;
            LoginActivity.this.rowItems = rowItems;


        }

        /* private view holder class */
        private class ViewHolder {
            TextViewAvenirLTStdBook payernameTxt, currencycodeTxt, exchangerateTxt;


        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder = null;

            try {
                LayoutInflater mInflater = (LayoutInflater) context
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                if (convertView == null) {
                    convertView = mInflater.inflate(
                            R.layout.exchangerate_list_layout, null);
                    holder = new ViewHolder();
                    holder.payernameTxt = (TextViewAvenirLTStdBook) convertView
                            .findViewById(R.id.payernameTxt);

                    holder.currencycodeTxt = (TextViewAvenirLTStdBook) convertView
                            .findViewById(R.id.currencycodeTxt);
                    holder.exchangerateTxt = (TextViewAvenirLTStdBook) convertView.findViewById(R.id.exchangerateTxt);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                holder.payernameTxt.setText(rowItems.get(position).getPayerName());
                holder.currencycodeTxt.setText(rowItems.get(position).getRecievingCurrencyyISOCode());
                holder.exchangerateTxt.setText(rowItems.get(position).getExchangeRate());


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

    }

    //    public long monthsBetween() {
//
//        String toyBornTime = ageDate(dateString);
//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//                "yyyy-MM-dd HH:mm:ss");
//        long year = 0;
//        try {
//
//            Date oldDate = dateFormat.parse(toyBornTime);
//            System.out.println(oldDate);
//
//            Date currentDate = new Date();
//            Calendar a = getCalendar(oldDate);
//            Calendar b = getCalendar(currentDate);
//            int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
////            if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
////                    (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
////                diff--;
////            }
//
//            diff--;
//            a.set(Calendar.YEAR, a.get(Calendar.YEAR) + diff);
//            Log.e("b.get(Calendar.YEAR)", b.get(Calendar.YEAR) + "");
//            Date firstDate = a.getTime();
//            Date secondDate = b.getTime();
//            long daysdiff = secondDate.getTime() - firstDate.getTime();
//            long diffDays = daysdiff / (24 * 60 * 60 * 1000);
//
//            System.out.println("diff" + diffDays);
////            long diff = currentDate.getTime() - oldDate.getTime();
////            long diffSeconds = diff / 1000 % 60;
////            long diffMinutes = diff / (60 * 1000) % 60;
////            long diffHours = diff / (60 * 60 * 1000) % 24;
////            long diffDays = diff / (24 * 60 * 60 * 1000);
////            long diffDays2 = diff / (24 * 60 * 60 * 1000);
////            long diffYears = diffDays2/(12 * 30);
//
////            Calendar c = Calendar.getInstance();
////////Set time in milliseconds
////            c.setTimeInMillis(diff);
////            int mYear = c.get(Calendar.YEAR);
////            int mMonth = c.get(Calendar.MONTH);
////            int mDay = c.get(Calendar.DAY_OF_MONTH);
////            long seconds = diff / 1000;
////            long minutes = seconds / 60;
////            long hours = minutes / 60;
////            long days = hours / 24;
////            long montth = days/30;
////             year = montth/12;
////           Date diffBirth = dateFormat.parse(String.valueOf(oldDate.getYear()+year));
////            long diffB = currentDate.getTime() - diffBirth.getTime();
////            long diffSeconds = diffB / 1000 % 60;
////            long diffMinutes = diffB / (60 * 1000) % 60;
////            long diffHours = diffB / (60 * 60 * 1000) % 24;
////            long diffDays = diffB / (24 * 60 * 60 * 1000);
//
////            remainingDays = montth%12;
//
//
////            if (oldDate.before(currentDate)) {
////
////                Log.e("oldDate", "is previous date");
////                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
////                        + " hours: " + hours + " days: " + days +montth);
////
////            }
//
//            // Log.e("toyBornTime", "" + toyBornTime);
//
//        } catch (ParseException e) {
//
//            e.printStackTrace();
//        }
//        return year;
//    }
    private void getAge() {
        try {
            String toyBornTime = ageDate(dateString);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");

            Date dateOfBirth = null;

            dateOfBirth = dateFormat.parse(toyBornTime);

            Calendar dob = Calendar.getInstance();
            dob.setTime(dateOfBirth);
            Calendar today = Calendar.getInstance();
            int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
            if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
                age--;
            } else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
                    && today.get(Calendar.DAY_OF_MONTH) < dob
                    .get(Calendar.DAY_OF_MONTH)) {
                age--;
            }

            if (age < 18) {
                System.out.println("diff" + age);
                MyApplication.popErrorMsg("", "You must be 18 years of age to register with us", LoginActivity.this);
            } else {
                System.out.println("diff" + age);
                signUp();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    private String ageDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

}
