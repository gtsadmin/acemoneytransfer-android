package com.acemoneytransfer.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.acemoneytransfer.R;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.fragments.AddIDFragment;
import com.acemoneytransfer.fragments.HomeFragment;
import com.acemoneytransfer.fragments.MenuFragment;
import com.acemoneytransfer.model.UserInfoWrapper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    public FrameLayout frameContainer;
    private DrawerLayout Drawer;
    private HomeFragment homeFragment;
    //    private SendMoneyActivity transferFragment;
    private ActionBarDrawerToggle mDrawerToggle;
    public Toolbar toolbar;
    public View logo;
    MenuFragment menuFragment;

    private static final String TAG = "MainActivity";
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    double latitude_cur, longitude_cur, latitude, longitude;
    Location mCurrentLocation;
    String mLastUpdateTime;
    ArrayList<String> countryList = new ArrayList<>();
    ArrayList<String> isoList = new ArrayList<>();
    HashMap<String, String> country_name = new HashMap<>();
    HashMap<String, String> ios_code = new HashMap<>();
    UserInfoWrapper userInfoWrapper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getUiComponents();
        menuFragment = new MenuFragment();
        loadHomeFragment(homeFragment, "HomeFragment");
        createLocationRequest();
        buildGoogleApiClient();
        setData();


    }

    private void getUiComponents() {
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);


        frameContainer = (FrameLayout) findViewById(R.id.frameContainer);

        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, Drawer,
                toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        Drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.icn_menu));


    }

    public void closeDrawers() {
        Drawer.closeDrawers();

    }

    public void loadHomeFragment(Fragment fragment, String tag) {
        Drawer.closeDrawers();

//        hideFragment(transferFragment);


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (tag.equalsIgnoreCase("HomeFragment")) {
//            if (homeFragment != null) {
//                ft.show(homeFragment).commit();
//            } else {
            getSupportActionBar().setTitle("");
            ((TextView) logo.findViewById(R.id.txt)).setText("ACE Money Transfer");
            homeFragment = new HomeFragment();

            ft.replace(frameContainer.getId(), homeFragment);
            ft.addToBackStack(null);
            ft.commit();

//            }
        } else if (tag.equalsIgnoreCase("AddIDFragment")) {
            final AddIDFragment addIDFragment = new AddIDFragment();

            ft.replace(frameContainer.getId(), addIDFragment);
            ft.addToBackStack(null);
            ft.commit();
//            if (addIDFragment.isAdded()) {
//                hideVisibleFragments();
//                getSupportFragmentManager().beginTransaction().show(addIDFragment).commit();
//
//            }else
//            {
//                FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
//                ft1.add(R.id.frameContainer, addIDFragment);
//                ft1.commit();
//
//            }
           new CountDownTimer(1000,1000)
           {

               @Override
               public void onTick(long millisUntilFinished) {

               }

               @Override
               public void onFinish() {
               addIDFragment.setData();
               }
           }.start();


        } else if (tag.equalsIgnoreCase("IDSListFragment")) {
            ft.replace(frameContainer.getId(), fragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }
    public void hideVisibleFragments() {
        List<Fragment> allFragments = getSupportFragmentManager().getFragments();
        if (allFragments == null || allFragments.isEmpty()) {
            return;
        }


        for (Fragment fragment : allFragments) {
            if (!(fragment instanceof MenuFragment) && fragment.isVisible()) {
                getSupportFragmentManager().beginTransaction().hide(fragment).commit();
            }
        }


    }
//    public void loadTransferFragment() {
//        Drawer.closeDrawers();
//
//        hideFragment(homeFragment);
//        toolbar.setTitle("Transfer");
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//
//        if (transferFragment != null) {
//            ft.show(transferFragment).commit();
//        } else {
//            transferFragment = new SendMoneyActivity();
//
//            ft.add(frameContainer.getId(), transferFragment);
//            // ft.addToBackStack(null);
//            ft.commit();
//
//        }
//    }

    private void hideFragment(Fragment fragment) {

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().hide(fragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {

        backPressAlert();
    }

    private void backPressAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("" + getResources().getString(R.string.app_name));
        builder.setMessage("Are you sure you want to exit application")
                .setCancelable(false)
                .setPositiveButton("Yes",

                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_home:
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("ACE Money Transfer");
        if (mGoogleApiClient!=null) {
            mGoogleApiClient.connect();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = menuFragment.addIDFragment;
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub
        if (mGoogleApiClient!=null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        try {
            mCurrentLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
            latitude_cur = mCurrentLocation.getLatitude();
            longitude_cur = mCurrentLocation.getLongitude();
            List<Address> Dragged_address_arr = getMarkerDraggedAddress(
                    latitude_cur,
                    longitude_cur);
            if (Dragged_address_arr != null) {
                if (Dragged_address_arr.size() > 0) {
                    String address = Dragged_address_arr.get(0)
                            .getAddressLine(0);
                    String city = Dragged_address_arr.get(0)
                            .getAddressLine(1);
                    String country = Dragged_address_arr.get(0)
                            .getCountryName();
                    Constants.current_country = country;

                }


            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        mCurrentLocation = location;
        latitude_cur = mCurrentLocation.getLatitude();
        longitude_cur = mCurrentLocation.getLongitude();
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public List<Address> getMarkerDraggedAddress(double latitude, double longitude) {

        try {
            Geocoder geocoder;

            if (!Geocoder.isPresent()) {
                System.out
                        .println(">>>>Geocoder is unavailable\n in geocoder.getFromLocation(latitude, longitude, 1);");
                return null;
            }

            List<Address> addresses;
            geocoder = new Geocoder(MainActivity.this);

            addresses = geocoder.getFromLocation(latitude, longitude, 1);

            return addresses;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }


    private void setData() {


        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(MainActivity.this);
        dbHelper.openDataBase();
        countryList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
        isoList = dbHelper.getCountryList(SqlLiteDbHelper.CURRENCY_CODE);
        for (int i = 0; i < countryList.size(); i++) {
            country_name.put(isoList.get(i), countryList.get(i));
            ios_code.put(countryList.get(i), isoList.get(i));
        }


    }
}


