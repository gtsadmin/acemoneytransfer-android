package com.acemoneytransfer.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.model.CountryModel;
import com.acemoneytransfer.model.UserInfoWrapper;
import com.acemoneytransfer.views.CircularImageView;
import com.acemoneytransfer.views.EditTextAvenirLTStdBook;
import com.acemoneytransfer.views.RadioButtonAvenirLTStdBook;
import com.acemoneytransfer.views.TextDrawable;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileActivity extends AppCompatActivity {


    @Bind(R.id.backImgBtn)
    ImageButton backImgBtn;
    @Bind(R.id.signInTxt)
    TextView signInTxt;
    @Bind(R.id.userImgView)
    CircularImageView userImgView;
    @Bind(R.id.imageContainer)
    RelativeLayout imageContainer;
    @Bind(R.id.nameEdt)
    EditTextAvenirLTStdBook nameEdt;
    @Bind(R.id.emailEdt)
    TextViewAvenirLTStdBook emailEdt;
    @Bind(R.id.phoneEdt)
    EditTextAvenirLTStdBook phoneEdt;
    @Bind(R.id.addressEdt)
    EditTextAvenirLTStdBook addressEdt;
    @Bind(R.id.cityEdt)
    EditTextAvenirLTStdBook cityEdt;
    @Bind(R.id.postalCodeEdt)
    EditTextAvenirLTStdBook postalCodeEdt;
    @Bind(R.id.birthdayTxt)
    TextViewAvenirLTStdBook birthdayTxt;
    @Bind(R.id.passwordEdt)
    EditTextAvenirLTStdBook passwordEdt;
    @Bind(R.id.confirmPasswordEdt)
    EditTextAvenirLTStdBook confirmPasswordEdt;

    @Bind(R.id.radioSex)
    RadioGroup radioSex;
    @Bind(R.id.countrySpinner)
    Spinner countrySpinner;
    @Bind(R.id.nationalitySpinner)
    Spinner nationalitySpinner;
    @Bind(R.id.birthPlaceSpinner)
    Spinner birthPlaceSpinner;
    @Bind(R.id.profileUpTxt)
    TextViewAvenirLTStdBook profileUpTxt;
    @Bind(R.id.radioMale)
    RadioButtonAvenirLTStdBook radioMale;
    @Bind(R.id.radioFemale)
    RadioButtonAvenirLTStdBook radioFemale;
    private String genderString = "male", dobString = "", iso_code_country = "", iso_code_nationality = "", iso_code_birthPlace = "";
    private final int DATE_DIALOG_ID = 1;
    private int year;
    private int month;
    private int day;
    ArrayList<String> countryList = new ArrayList<>();
    ArrayList<String> isoList = new ArrayList<>();
    HashMap<String, String> country_name = new HashMap<>();
    HashMap<String, String> ios_code = new HashMap<>();
    Toolbar toolbar;
    ArrayList<CountryModel> countryModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        if (MyApplication.isInternetWorking(ProfileActivity.this)) {
            getCountrys();

        }


        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Profile");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!countrySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {
                    iso_code_country = ios_code.get(countrySpinner.getSelectedItem().toString()).toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        nationalitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!nationalitySpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {

                    iso_code_nationality = ios_code.get(nationalitySpinner.getSelectedItem().toString()).toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        birthPlaceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!birthPlaceSpinner.getSelectedItem().toString().equalsIgnoreCase("Select country")) {

                    iso_code_birthPlace = ios_code.get(birthPlaceSpinner.getSelectedItem().toString()).toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        nameEdt.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z ]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });
        cityEdt.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z ]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setData() {


//        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(ProfileActivity.this);
//        dbHelper.openDataBase();
//        countryList = dbHelper.getCountryList(SqlLiteDbHelper.NAME);
//        isoList = dbHelper.getCountryList(SqlLiteDbHelper.ISO_ALPHA3);
//
//        for (int i = 0; i < countryList.size(); i++) {
//            country_name.put(isoList.get(i), countryList.get(i));
//            ios_code.put(countryList.get(i), isoList.get(i));
//        }
        radioFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    genderString = "female";
            }
        });


        radioMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    genderString = "male";
            }
        });

    }

    @OnClick(R.id.birthdayTxt)
    public void birthdayTxt() {

        showDialog(DATE_DIALOG_ID);

    }

    @OnClick(R.id.profileUpTxt)
    public void setProfileUpTxt() {
        if (MyApplication.isInternetWorking(ProfileActivity.this)) {

            if (TextUtils.isEmpty(emailEdt.getText().toString())) {

                Toast.makeText(ProfileActivity.this, "Email cannot be empty", Toast.LENGTH_LONG).show();
            } else {
                if (!TextUtils.isEmpty(nameEdt.getText().toString())&&!TextUtils.isEmpty(phoneEdt.getText().toString())&&!TextUtils.isEmpty(addressEdt.getText().toString())&&!TextUtils.isEmpty(cityEdt.getText().toString())&&!TextUtils.isEmpty(iso_code_country)&&!TextUtils.isEmpty(iso_code_birthPlace)&&!TextUtils.isEmpty(iso_code_nationality)) {
                    updateProfile();
                }else
                {
                    Toast.makeText(ProfileActivity.this, "All fields are mandatory. Please check", Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();

        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date

                return new DatePickerDialog(this, datePickerListener,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));


        }
        return null;
    }

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("MMM dd yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String formateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = targetFormat.parse(dateString);
            String formattedDate = originalFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String updateDateForServer(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String dateString;
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {


        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview


            String dayString = day + "", monthString = (month + 1) + "";

            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
            dateString = monthString + "-" + dayString + "-" + year;
            birthdayTxt.setText(formateDate(dateString));
            dobString = updateDateForServer(dateString);

        }
    };

    public void setNationalityArr() {
        Collections.reverse(countryList);
        countryList.add("Select country");
        Collections.reverse(countryList);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, countryList)
        {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.black));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        }; //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout);
        nationalitySpinner.setAdapter(spinnerArrayAdapter);
        birthPlaceSpinner.setAdapter(spinnerArrayAdapter);
        countrySpinner.setAdapter(spinnerArrayAdapter);

    }

    private void getProfile() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().showProgressDialog(ProfileActivity.this);
        MyApplication.getApplication().hideSoftKeyBoard(ProfileActivity.this);


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUser/GetProfile", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, ProfileActivity.this);
                            } else {
                                JSONObject AceUser = response.getJSONObject("AceUser");

                                setDataUserInfo(AceUser.getString("FullName"), AceUser.getString("Email"), AceUser.getString("Phone"), AceUser.getString("DOB"), AceUser.getString("Gender"), AceUser.getString("CountryIsoCode"), AceUser.getString("NationalityIsoCode"), AceUser.getString("CountryOfBirthIsoCode"), AceUser.getString("Address"), AceUser.getString("City"), AceUser.getString("PostalCode"));

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    public void popMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                finish();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setDataUserInfo(String FullName, String Email, String Phone, String DOB, String Gender, String CountryIsoCode, String NationalityIsoCode, String CountryOfBirthIsoCode, String Address, String City, String PostalCode)

    {


        nameEdt.append(FullName);
        emailEdt.append(Email);

        String dob = formateDateForServer(DOB);
        birthdayTxt.setText(formateDateForServer(DOB));
        if (Gender.equalsIgnoreCase("Female")) {
            radioFemale.setChecked(true);
        } else {
            radioMale.setChecked(true);
        }
        countrySpinner.setSelection(countryList.indexOf(country_name.get(CountryIsoCode).toString()));
        countrySpinner.setEnabled(false);
        if (NationalityIsoCode.equalsIgnoreCase(""))
        {
//            nationalitySpinner.setSelection(countryList.indexOf(Constants.current_country));
        }else
        {
            nationalitySpinner.setSelection(countryList.indexOf(country_name.get(NationalityIsoCode).toString()));
        }
        if (CountryOfBirthIsoCode.equalsIgnoreCase(""))
        {
            birthPlaceSpinner.setSelection(countryList.indexOf(Constants.current_country));
        }else
        {
            birthPlaceSpinner.setSelection(countryList.indexOf(country_name.get(CountryOfBirthIsoCode).toString()));
        }
//        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(ProfileActivity.this);
//        dbHelper.openDataBase();
//        String countryName =country_name.get(CountryIsoCode).toString();
        String firstCh = ""+Phone.charAt(0);
        if (firstCh.equalsIgnoreCase("+")) {
            phoneEdt.append(Phone);
        } else {
            final String globleCode = MyApplication.countryCodeMap.get(country_name.get(CountryIsoCode).toString());
            phoneEdt.setText("+"+globleCode);
            Selection.setSelection(phoneEdt.getText(), phoneEdt.getText().length());
            phoneEdt.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().startsWith("+" + globleCode)) {
                        phoneEdt.setText("+" + globleCode);
                        Selection.setSelection(phoneEdt.getText(), phoneEdt
                                .getText().length());

                    }

                }

            });
            phoneEdt.append(Phone);
        }

        addressEdt.append(Address);
        cityEdt.append(City);
        postalCodeEdt.append(PostalCode);
    }

    private void updateProfile() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
        MyApplication.getApplication().hideSoftKeyBoard(ProfileActivity.this);
        MyApplication.getApplication().showProgressDialog(ProfileActivity.this);



        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


            JSONObject userObject = new JSONObject();
            userObject.put("FullName", nameEdt.getText().toString());
            userObject.put("Email", emailEdt.getText().toString());

            userObject.put("Phone", phoneEdt.getText().toString());
            userObject.put("DOB", birthdayTxt.getText().toString());

            userObject.put("Gender", genderString);
            userObject.put("CountryIsoCode", iso_code_country);

            userObject.put("NationalityIsoCode", iso_code_nationality);

            userObject.put("CountryOfBirthIsoCode", iso_code_birthPlace);
            userObject.put("Address", addressEdt.getText().toString());
            userObject.put("City", cityEdt.getText().toString());
            userObject.put("PostalCode", postalCodeEdt.getText().toString());


            object.put("AceUser", userObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUser/UpdateProfile", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());

                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                getDashboard();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

    private void getCountrys() {
        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

        MyApplication.getApplication().hideSoftKeyBoard(ProfileActivity.this);

        MyApplication.getApplication().showProgressDialog(ProfileActivity.this);
        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetCountryList", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());
                            MyApplication.getApplication().hideProgressDialog();
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                if (response.has("AceCountryList")) {
                                    JSONArray aceSendingCountryList = response.getJSONArray("AceCountryList");
                                    for (int i = 0; i < aceSendingCountryList.length(); i++) {
                                        CountryModel countryModel = new CountryModel();
                                        JSONObject acesendingSub = aceSendingCountryList.getJSONObject(i);
                                        if (acesendingSub.has("CountryName")) {
                                            countryList.add(acesendingSub.getString("CountryName"));
                                            countryModel.setCountryName(acesendingSub.getString("CountryName"));
                                        }
                                        if (acesendingSub.has("Iso2Code")) {
                                            countryModel.setIso2Code(acesendingSub.getString("Iso2Code"));
                                        }
                                        if (acesendingSub.has("Iso3Code") && acesendingSub.has("CountryName")) {
                                            countryModel.setIso3Code(acesendingSub.getString("Iso3Code"));
                                            ios_code.put(acesendingSub.getString("CountryName"), acesendingSub.getString("Iso3Code"));
                                            country_name.put(acesendingSub.getString("Iso3Code"), acesendingSub.getString("CountryName"));
                                        }
                                        if (acesendingSub.has("IsoNumericCode")) {
                                            countryModel.setIsoNumericCode(acesendingSub.getString("IsoNumericCode"));
                                        }
                                        if (acesendingSub.has("CurrencyIsoCode")) {
                                            countryModel.setCurrencyIsoCode(acesendingSub.getString("CurrencyIsoCode"));
                                        }
                                        countryModels.add(countryModel);
                                    }
                                    setData();
                                    setNationalityArr();
                                    getProfile();
                                }
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, ProfileActivity.this);
                            }

                            else {
                                Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        MyApplication.getApplication().hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }
    private void getDashboard() {



        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUser/GetDashboard", object.toString(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response" + response);
                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {

                                JSONObject ace_userObj = response.getJSONObject("AceUser");
                                UserInfoWrapper userInfoWrapper = new UserInfoWrapper();
                                userInfoWrapper.setAddress(ace_userObj.getString("Address"));
                                userInfoWrapper.setCity(ace_userObj.getString("City"));
                                userInfoWrapper.setCountryIsoCode(ace_userObj.getString("CountryIsoCode"));
                                userInfoWrapper.setDOB(ace_userObj.getString("DOB"));
                                userInfoWrapper.setCountryOfBirthIsoCode(ace_userObj.getString("CountryOfBirthIsoCode"));
                                userInfoWrapper.setEmail(ace_userObj.getString("Email"));
                                userInfoWrapper.setFullName(ace_userObj.getString("FullName"));
                                userInfoWrapper.setGender(ace_userObj.getString("Gender"));
                                userInfoWrapper.setNationalityIsoCode(ace_userObj.getString("NationalityIsoCode"));
                                userInfoWrapper.setPhone(ace_userObj.getString("Phone"));
                                userInfoWrapper.setPostalCode(ace_userObj.getString("PostalCode"));

                                userInfoWrapper.setCurrencyCode(ios_code.get(ace_userObj.getString("CountryIsoCode")));

                                DataManager.getInstance().setUserInfoWrapper(userInfoWrapper);
                                finish();
                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, ProfileActivity.this);
                            } else {
                                popMessage(message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }

}
