package com.acemoneytransfer.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.application.MyApplication;
import com.acemoneytransfer.datacontroller.Constants;
import com.acemoneytransfer.datacontroller.DataManager;
import com.acemoneytransfer.model.ReceivingMethod;
import com.acemoneytransfer.views.EditTextAvenirLTStdBook;
import com.acemoneytransfer.views.TextViewAvenirLTStdBook;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SendMoneyActivity extends AppCompatActivity {


    @Bind(R.id.sendingMethodTxt)
    TextViewAvenirLTStdBook sendingMethodTxt;
    @Bind(R.id.recievingCountryTxt)
    TextViewAvenirLTStdBook recievingCountryTxt;
    @Bind(R.id.enterAmountEdt)
    EditTextAvenirLTStdBook enterAmountEdt;


    @Bind(R.id.sendMoneyTxt)
    TextViewAvenirLTStdBook sendMoneyTxt;
    @Bind(R.id.amountTxt)
    TextViewAvenirLTStdBook amountTxt;
    @Bind(R.id.chargesTxt)
    TextViewAvenirLTStdBook chargesTxt;
    @Bind(R.id.totalamountTxt)
    TextViewAvenirLTStdBook totalamountTxt;


    @Bind(R.id.sendingMethodtypeTxt)
    TextViewAvenirLTStdBook sendingMethodtypeTxt;
    @Bind(R.id.sendingMethodLayout)
    RelativeLayout sendingMethodLayout;
    @Bind(R.id.recievingMethodtypeTxt)
    TextViewAvenirLTStdBook recievingMethodtypeTxt;
    @Bind(R.id.recievingLayout)
    RelativeLayout recievingLayout;
    @Bind(R.id.amount)
    TextViewAvenirLTStdBook amount;
    @Bind(R.id.charges)
    TextViewAvenirLTStdBook charges;
    @Bind(R.id.totalamout)
    TextViewAvenirLTStdBook totalamout;
    @Bind(R.id.senderCurrencySpinner)
    TextViewAvenirLTStdBook senderCurrencySpinner;
    @Bind(R.id.recieverCurrencySpinner)
    TextViewAvenirLTStdBook recieverCurrencySpinner;
    @Bind(R.id.recieverGetAmounts)
    EditTextAvenirLTStdBook recieverGetAmounts;
    @Bind(R.id.exchangeRateTxt)
    TextViewAvenirLTStdBook exchangeRateTxt;
    @Bind(R.id.exchangeRate)
    TextViewAvenirLTStdBook exchangeRate;
    @Bind(R.id.methodLayout)
    LinearLayout methodLayout;
    @Bind(R.id.infochargesImg)
    ImageView infochargesImg;
    private boolean isSenderCurrencySelected = false;

    int charge = 0;
    private Toolbar toolbar;
    String exchangeRateStr = "";
    ReceivingMethod receivingMethod;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_send_money);
        ButterKnife.bind(this);
        receivingMethod = MyApplication.getApplication().getReceivingMethod();
        if (receivingMethod!=null) {
            enterAmountEdt.append(receivingMethod.getPayInAmount());
        }

        toolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(toolbar);
        View logo = getLayoutInflater().inflate(R.layout.toolbar_customview, null);
        toolbar.addView(logo);
        getSupportActionBar().setTitle("");
        ((TextView) logo.findViewById(R.id.txt)).setText("Send Money");

//        getSupportActionBar().setTitle("Send Money");
        // cat_price = getIntent().getExtras().getString("cat_price");
        toolbar.setNavigationIcon(R.mipmap.icn_close);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

        enterAmountEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        if (receivingMethod != null) {
                            MyApplication.getApplication().hideSoftKeyBoard(SendMoneyActivity.this);
                            if (!receivingMethod.getPayerId().equalsIgnoreCase("") && !enterAmountEdt.getText().toString().equalsIgnoreCase("")) {
                                if (!TextUtils.isEmpty(exchangeRateStr) && exchangeRateStr != null && !exchangeRateStr.equalsIgnoreCase("null")) {
                                    float send_money = (Float.parseFloat(enterAmountEdt.getText().toString()) * Float.parseFloat(exchangeRateStr));
                                    double finalValue = Math.round(send_money * 100.0) / 100.0;
                                    recieverGetAmounts.setText("" + finalValue);
                                    amountTxt.setText(enterAmountEdt.getText().toString());

                                    DataManager.getInstance().transactionModel.setSending_amount(enterAmountEdt.getText().toString());
                                    DataManager.getInstance().transactionModel.setReceiver_amount("" + finalValue);
                                    if (MyApplication.isInternetWorking(SendMoneyActivity.this)) {
                                        GetServiceCharges(receivingMethod.getPayerId());
                                    }
                                } else {
                                    float send_money = (Float.parseFloat(enterAmountEdt.getText().toString()) * Float.parseFloat("1.0"));
                                    double finalValue = Math.round(send_money * 100.0) / 100.0;
                                    recieverGetAmounts.setText("" + finalValue);
                                    amountTxt.setText(enterAmountEdt.getText().toString());
                                    DataManager.getInstance().transactionModel.setSending_amount(enterAmountEdt.getText().toString());
                                    DataManager.getInstance().transactionModel.setReceiver_amount("" + finalValue);
                                    if (MyApplication.isInternetWorking(SendMoneyActivity.this)) {
                                        GetServiceCharges(receivingMethod.getPayerId());
                                    }
                                }
                            }
                        }
                        break;


                }
                return true;
            }
        });
        recieverGetAmounts.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        if (receivingMethod != null) {
                            MyApplication.getApplication().hideSoftKeyBoard(SendMoneyActivity.this);
                            if (!receivingMethod.getPayerId().equalsIgnoreCase("") && !recieverGetAmounts.getText().toString().equalsIgnoreCase("")) {
                                if (!TextUtils.isEmpty(exchangeRateStr) && exchangeRateStr != null && !exchangeRateStr.equalsIgnoreCase("null")) {
                                    float send_money = (Float.parseFloat(recieverGetAmounts.getText().toString()) / Float.parseFloat(exchangeRateStr));
                                    double finalValue = Math.round(send_money * 100.0) / 100.0;
                                    enterAmountEdt.setText("" + finalValue);
                                    amountTxt.setText(enterAmountEdt.getText().toString());
                                    DataManager.getInstance().transactionModel.setSending_amount(enterAmountEdt.getText().toString());
                                    DataManager.getInstance().transactionModel.setReceiver_amount("" + finalValue);
                                    if (MyApplication.isInternetWorking(SendMoneyActivity.this)) {
                                        GetServiceCharges(receivingMethod.getPayerId());
                                    }
                                } else {
                                    float send_money = (Float.parseFloat(enterAmountEdt.getText().toString()) * Float.parseFloat("1.0"));
                                    double finalValue = Math.round(send_money * 100.0) / 100.0;
                                    recieverGetAmounts.setText("" + finalValue);
                                    amountTxt.setText(enterAmountEdt.getText().toString());
                                    DataManager.getInstance().transactionModel.setSending_amount(enterAmountEdt.getText().toString());
                                    DataManager.getInstance().transactionModel.setReceiver_amount("" + finalValue);
                                    if (MyApplication.isInternetWorking(SendMoneyActivity.this)) {
                                        GetServiceCharges(receivingMethod.getPayerId());
                                    }
                                }
                            }


                        }
                        break;


                }
                return true;
            }
        });

//        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_in, R.anim.back_out);

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);

    }

    @OnClick(R.id.infochargesImg)
    public void infochargesImg() {
        if (!TextUtils.isEmpty(amountTxt.getText().toString())) {
            Double creditCardCharge=0.0;
            if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Credit Card")) {
                creditCardCharge = (Double) ((Float.parseFloat(amountTxt.getText().toString()) * 2.35) / 100);
            }

            final Dialog infochargeDialog = new Dialog(SendMoneyActivity.this, R.style.Theme_Custom);
            infochargeDialog.setContentView(R.layout.chargerates_dialog_box);
            infochargeDialog.show();
            infochargeDialog.setCanceledOnTouchOutside(true);
            TextViewAvenirLTStdBook serviceChargeTxt, cardChargeTxt, totalChargeTxt;
            ImageView closeImg;

            serviceChargeTxt = (TextViewAvenirLTStdBook) infochargeDialog.findViewById(R.id.serviceChargeTxt);
            serviceChargeTxt.setText("" + charge);
            closeImg =(ImageView)infochargeDialog.findViewById(R.id.closeImg);
            cardChargeTxt = (TextViewAvenirLTStdBook) infochargeDialog.findViewById(R.id.cardChargeTxt);
            cardChargeTxt.setText("" + creditCardCharge);
            totalChargeTxt = (TextViewAvenirLTStdBook) infochargeDialog.findViewById(R.id.totalChargeTxt);
            Double total_charge = (Double) (creditCardCharge + charge);
            totalChargeTxt.setText("" + total_charge);
            closeImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    infochargeDialog.dismiss();
                }
            });

        }else
        {
            Toast.makeText(SendMoneyActivity.this,"Please enter amount",Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.sendingMethodLayout)
    public void sendingMethodLayout() {
        startActivity(new Intent(SendMoneyActivity.this, SendingMethodActivity.class));
        overridePendingTransition(R.anim.left,
                R.anim.right);
    }

    @OnClick(R.id.recievingLayout)
    public void recievingLayout() {
        startActivity(new Intent(SendMoneyActivity.this, ReceivingMethodActivity.class));
        overridePendingTransition(R.anim.left,
                R.anim.right);
    }

    @OnClick(R.id.sendMoneyTxt)
    public void sendMoneyTxt() {
        if (TextUtils.isEmpty(enterAmountEdt.getText().toString())) {
            enterAmountEdt.setError("Please enter amount");
            enterAmountEdt.requestFocus();
        } else if (Constants.RECEIVINGMETHOD == false) {
            MyApplication.popErrorMsg("", "Please select receiver method", SendMoneyActivity.this);
        } else if (Constants.SENDINGMETHOD == false) {
            MyApplication.popErrorMsg("", "Please select sending method", SendMoneyActivity.this);
        } else if (TextUtils.isEmpty(exchangeRateStr) || exchangeRateStr.equalsIgnoreCase("null")) {
            MyApplication.popErrorMsg("", "Exchange rate is null", SendMoneyActivity.this);
        } else {
            if (getIntent().getExtras().getInt("code") == Constants.RECEIVERLISTTOSENDMONEY) {
                startActivity(new Intent(SendMoneyActivity.this, RecieverDetailActivity.class));
                overridePendingTransition(R.anim.left,
                        R.anim.right);
            } else if (getIntent().getExtras().getInt("code") == Constants.SENDMONEYTORECEIVERDETAILS) {
                startActivity(new Intent(SendMoneyActivity.this, RecieverDetailActivity.class));
                overridePendingTransition(R.anim.left,
                        R.anim.right);
            } else {
                startActivity(new Intent(SendMoneyActivity.this, RecepientListActivity.class).putExtra("code", Constants.SENDMONEYTORECEIVERLIST).putExtra("isocode", receivingMethod.getISO_Code()));
                overridePendingTransition(R.anim.left,
                        R.anim.right);
            }
        }
    }

    private void GetExchangeRate(String PayerID) {


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            JSONObject AceExchangeRateRequest = new JSONObject();
            AceExchangeRateRequest.put("PayerID", PayerID);


            object.put("AceExchangeRateRequest", AceExchangeRateRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final RequestQueue requestQueue = Volley.newRequestQueue(SendMoneyActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetExchangeRate", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response>>> " + response.toString());

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONObject aceExchangeRateResponceobj = response.getJSONObject("AceExchangeRateResponce");
                                if (aceExchangeRateResponceobj.has("ExchangeRate")) {
                                    exchangeRateStr = aceExchangeRateResponceobj.getString("ExchangeRate");
                                    if (exchangeRateStr != null&&!exchangeRateStr.equalsIgnoreCase("null")) {
                                        exchangeRateTxt.setText(exchangeRateStr);
                                    }
                                    DataManager.getInstance().transactionModel.setRate(exchangeRateStr);
                                    if (!TextUtils.isEmpty(enterAmountEdt.getText().toString())) {
                                        float send_money = (Float.parseFloat(enterAmountEdt.getText().toString()) * Float.parseFloat(exchangeRateStr));
                                        double finalValue = Math.round(send_money * 100.0) / 100.0;
                                        recieverGetAmounts.setText("" + finalValue);
                                        if (receivingMethod != null) {
                                            if (!receivingMethod.getPayerId().equalsIgnoreCase("") && !enterAmountEdt.getText().toString().equalsIgnoreCase("")) {
                                                if (!TextUtils.isEmpty(exchangeRateStr) && exchangeRateStr != null && !exchangeRateStr.equalsIgnoreCase("null")) {
                                                    //     float send_money = (Float.parseFloat(enterAmountEdt.getText().toString()) * Float.parseFloat(exchangeRateStr));
                                                    recieverGetAmounts.setText("" + finalValue);
                                                    amountTxt.setText(enterAmountEdt.getText().toString());

                                                    DataManager.getInstance().transactionModel.setSending_amount(enterAmountEdt.getText().toString());
                                                    DataManager.getInstance().transactionModel.setReceiver_amount("" + finalValue);
                                                    GetServiceCharges(receivingMethod.getPayerId());
                                                } else {
                                                    //  float send_money = (Float.parseFloat(enterAmountEdt.getText().toString()) * Float.parseFloat("1.0"));
                                                    recieverGetAmounts.setText("" + send_money);
                                                    amountTxt.setText(enterAmountEdt.getText().toString());

                                                    DataManager.getInstance().transactionModel.setSending_amount(enterAmountEdt.getText().toString());
                                                    DataManager.getInstance().transactionModel.setReceiver_amount("" + finalValue);
                                                    GetServiceCharges(receivingMethod.getPayerId());
                                                }
                                            } else {
                                                send_money = 0;
                                                recieverGetAmounts.setText("" + finalValue);
                                            }
                                        }

                                    }
                                }
                                if (aceExchangeRateResponceobj.has("RecievingCountryName")) {
                                    DataManager.getInstance().transactionModel.setAddress(aceExchangeRateResponceobj.getString("RecievingCountryName"));
                                }
                                if (aceExchangeRateResponceobj.has("RecievingCurrencyyISOCode")) {
                                    //   DataManager.getInstance().transactionModel.setReceiver_currency(aceExchangeRateResponceobj.getString("RecievingCurrencyyISOCode"));
//                                    recieverCurrencySpinner.setText(aceExchangeRateResponceobj.getString("RecievingCurrencyyISOCode"));
                                }
                                if (aceExchangeRateResponceobj.has("PayerName")) {
                                    DataManager.getInstance().transactionModel.setBank_name(aceExchangeRateResponceobj.getString("PayerName"));
                                }
                                System.out.println("exchangeRateStr" + exchangeRateStr);
                                //    GetServiceChargesList(receivingMethod.getPayerId());

                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, SendMoneyActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        requestQueue.add(jsObjRequest);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onResume() {
        super.onResume();
        sendingMethodtypeTxt.setText("unselected");
        recievingMethodtypeTxt.setText("unselected");
        receivingMethod = MyApplication.getApplication().getReceivingMethod();
        recieverCurrencySpinner.setText("" + DataManager.getInstance().transactionModel.getReceiver_currency());

        if (receivingMethod != null)
        {
            if (!receivingMethod.getPayerId().equalsIgnoreCase("")) {
                System.out.println("PayerId" + receivingMethod.getPayerId());
                GetExchangeRate(receivingMethod.getPayerId());

            }
        }
        if (!MyApplication.getApplication().getSending_method().equalsIgnoreCase("")) {
            if (!MyApplication.getApplication().getSending_method().equalsIgnoreCase("Select")) {
                Drawable leftDrawable = getResources().getDrawable(R.mipmap.icon_done);
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.arrow_next);
                int lh = leftDrawable.getIntrinsicHeight();
                int lw = leftDrawable.getIntrinsicWidth();
                leftDrawable.setBounds(0, 0, lw, lh);
                int rh = rightDrawable.getIntrinsicHeight();
                int rw = rightDrawable.getIntrinsicWidth();
                rightDrawable.setBounds(0, 0, rw, rh);
                sendingMethodTxt.setCompoundDrawables(leftDrawable, null, null, null);
                sendingMethodtypeTxt.setText(MyApplication.getApplication().getSending_method());

            } else {
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.arrow_next);
                int rh = rightDrawable.getIntrinsicHeight();
                int rw = rightDrawable.getIntrinsicWidth();
                rightDrawable.setBounds(0, 0, rw, rh);
                sendingMethodTxt.setCompoundDrawables(null, null, null, null);
            }
        }
        if (receivingMethod != null) {
            if (!receivingMethod.getSelect_payment_method().equalsIgnoreCase("Select payment method") && !receivingMethod.getPayerId().equalsIgnoreCase("") && Constants.RECEIVINGMETHOD == true&&!receivingMethod.getPayerName().equalsIgnoreCase("Select bank")&&!receivingMethod.getPayer_branch_name().equalsIgnoreCase("Select branch")) {
//            if (!MyApplication.getApplication().getReceiving_method().equalsIgnoreCase("Select")) {
                Drawable leftDrawable = getResources().getDrawable(R.mipmap.icon_done);
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.arrow_next);
                int lh = leftDrawable.getIntrinsicHeight();
                int lw = leftDrawable.getIntrinsicWidth();
                leftDrawable.setBounds(0, 0, lw, lh);
                int rh = rightDrawable.getIntrinsicHeight();
                int rw = rightDrawable.getIntrinsicWidth();
                rightDrawable.setBounds(0, 0, rw, rh);
                recievingCountryTxt.setCompoundDrawables(leftDrawable, null, null, null);
                recievingMethodtypeTxt.setText(receivingMethod.getSelect_payment_method());
                senderCurrencySpinner.setText(DataManager.getInstance().getUserInfoWrapper().getCurrencyCode());

            }else
            {
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.arrow_next);

                int rh = rightDrawable.getIntrinsicHeight();
                int rw = rightDrawable.getIntrinsicWidth();
                rightDrawable.setBounds(0, 0, rw, rh);
                recievingCountryTxt.setCompoundDrawables(null, null, null, null);
            }
        }else {

            Drawable rightDrawable = getResources().getDrawable(R.mipmap.arrow_next);

            int rh = rightDrawable.getIntrinsicHeight();
            int rw = rightDrawable.getIntrinsicWidth();
            rightDrawable.setBounds(0, 0, rw, rh);
            recievingCountryTxt.setCompoundDrawables(null, null, null, null);
        }

    }

    public void popMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SendMoneyActivity.this);
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(SendMoneyActivity.this, LoginActivity.class));
                finish();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void GetServiceCharges(String PayerID) {


        JSONObject object = new JSONObject();

        try {

            object.put("AuthKey", Constants.AUTH_KEY);
            object.put("AuthToken", MyApplication.getAuthToken("AuthToken"));
            JSONObject AceChargesRequest = new JSONObject();
            AceChargesRequest.put("PayerID", PayerID);
            AceChargesRequest.put("Amount", enterAmountEdt.getText().toString());


            object.put("AceChargesRequest", AceChargesRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(SendMoneyActivity.this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, MyApplication.APP_URL + "aceUtils/GetServiceCharges", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("GetServiceCharges>>> " + response.toString());

                            JSONObject AceResult = response.getJSONObject("AceResult");
                            int rflag = AceResult.getInt("Rflag");
                            int code = AceResult.getInt("Code");
                            String message = AceResult.getString("Message");
                            if (rflag == 1 && code == 0) {
                                JSONObject aceChargesResponce = response.getJSONObject("AceChargesResponce");
                                int charge = aceChargesResponce.getInt("Charges");
//                                chargesTxt.setText("" + charge);
                                SendMoneyActivity.this.charge = charge;
                                float creditCardCharge = (float) ((Float.parseFloat(amountTxt.getText().toString()) * 2.35) / 100);

                                if (MyApplication.getApplication().getSending_method().equalsIgnoreCase("Credit Card")) {
                                    chargesTxt.setText("" + (charge + creditCardCharge));
                                } else {
                                    chargesTxt.setText("" + charge);
                                }

                                Double total_amount = (Double.parseDouble(enterAmountEdt.getText().toString()) + Double.parseDouble(chargesTxt.getText().toString()));
                                System.out.println("total_amount>>> " + total_amount);
                                totalamountTxt.setText("" + total_amount);

                                DataManager.getInstance().transactionModel.setAmount("" + total_amount);
                                DataManager.getInstance().transactionModel.setCharges("" + chargesTxt.getText().toString());

                            } else if (rflag == 0 && code == 101) {
                                MyApplication.getApplication().popMessage(message, SendMoneyActivity.this);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
    }


}
