package com.acemoneytransfer.model;

/**
 * Created by suarebits on 5/11/15.
 */
public class PayerBranchModel {

    String BranchName="",BranchAddress="",BranchCode="",BranchPayCode="",BranhCity="",ReceivingCountryIsoCode="",ReceivingCurrencyIsoCode="";

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getBranchAddress() {
        return BranchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        BranchAddress = branchAddress;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public String getBranchPayCode() {
        return BranchPayCode;
    }

    public void setBranchPayCode(String branchPayCode) {
        BranchPayCode = branchPayCode;
    }

    public String getBranhCity() {
        return BranhCity;
    }

    public void setBranhCity(String branhCity) {
        BranhCity = branhCity;
    }

    public String getReceivingCountryIsoCode() {
        return ReceivingCountryIsoCode;
    }

    public void setReceivingCountryIsoCode(String receivingCountryIsoCode) {
        ReceivingCountryIsoCode = receivingCountryIsoCode;
    }

    public String getReceivingCurrencyIsoCode() {
        return ReceivingCurrencyIsoCode;
    }

    public void setReceivingCurrencyIsoCode(String receivingCurrencyIsoCode) {
        ReceivingCurrencyIsoCode = receivingCurrencyIsoCode;
    }
}
