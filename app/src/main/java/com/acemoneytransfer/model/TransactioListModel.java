package com.acemoneytransfer.model;

import java.io.Serializable;


public class TransactioListModel implements Serializable{
    String PaymentNumber ="";
    String PaymentMethod="";
    String PaymentDate="";
    String SendingCountry="";
    String SendingCurrency="";
    String ReceivingCountry="";
    String ReceivingCurrency="";
    String PayInAmount="";
    String PayOutAmount="";
    String ExchangeRate="";
    String SenderName="";
    String BeneName="";
    String PaymentStatus="";
    String PayerID="";
    String PayerName="";
    String PayerBranchCode="";
    String BeneID="";
    String payoutBranchName ="";
    String sendingPaymentMethod="";

    public String getPayoutBranchName() {
        return payoutBranchName;
    }

    public void setPayoutBranchName(String payoutBranchName) {
        this.payoutBranchName = payoutBranchName;
    }

    public String getSendingPaymentMethod() {
        return sendingPaymentMethod;
    }

    public void setSendingPaymentMethod(String sendingPaymentMethod) {
        this.sendingPaymentMethod = sendingPaymentMethod;
    }

    public String getPayoutBranchCode() {
        return PayoutBranchCode;
    }

    public void setPayoutBranchCode(String payoutBranchCode) {
        PayoutBranchCode = payoutBranchCode;
    }

    String PayoutBranchCode ="";
    public String getBeneID() {
        return BeneID;
    }

    public void setBeneID(String beneID) {
        BeneID = beneID;
    }



    public String getPaymentNumber() {
        return PaymentNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        PaymentNumber = paymentNumber;
    }

    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        PaymentMethod = paymentMethod;
    }

    public String getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        PaymentDate = paymentDate;
    }

    public String getSendingCountry() {
        return SendingCountry;
    }

    public void setSendingCountry(String sendingCountry) {
        SendingCountry = sendingCountry;
    }

    public String getSendingCurrency() {
        return SendingCurrency;
    }

    public void setSendingCurrency(String sendingCurrency) {
        SendingCurrency = sendingCurrency;
    }

    public String getReceivingCountry() {
        return ReceivingCountry;
    }

    public void setReceivingCountry(String receivingCountry) {
        ReceivingCountry = receivingCountry;
    }

    public String getReceivingCurrency() {
        return ReceivingCurrency;
    }

    public void setReceivingCurrency(String receivingCurrency) {
        ReceivingCurrency = receivingCurrency;
    }

    public String getPayInAmount() {
        return PayInAmount;
    }

    public void setPayInAmount(String payInAmount) {
        PayInAmount = payInAmount;
    }

    public String getPayOutAmount() {
        return PayOutAmount;
    }

    public void setPayOutAmount(String payOutAmount) {
        PayOutAmount = payOutAmount;
    }

    public String getExchangeRate() {
        return ExchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        ExchangeRate = exchangeRate;
    }

    public String getSenderName() {
        return SenderName;
    }

    public void setSenderName(String senderName) {
        SenderName = senderName;
    }

    public String getBeneName() {
        return BeneName;
    }

    public void setBeneName(String beneName) {
        BeneName = beneName;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getPayerID() {
        return PayerID;
    }

    public void setPayerID(String payerID) {
        PayerID = payerID;
    }

    public String getPayerName() {
        return PayerName;
    }

    public void setPayerName(String payerName) {
        PayerName = payerName;
    }

    public String getPayerBranchCode() {
        return PayerBranchCode;
    }

    public void setPayerBranchCode(String payerBranchCode) {
        PayerBranchCode = payerBranchCode;
    }
}
