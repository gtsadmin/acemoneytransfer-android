package com.acemoneytransfer.model;

import java.io.Serializable;

/**
 * Created by suarebits on 30/10/15.
 */
public class IDListModel implements Serializable{
    String DocID="",DocType="",DocNumber="",DocIssueDate="",DocExpireDate="",DocAddedDate="",DocFileUrl="",DocFileBackUrl="",DocBodyBack="";

    public String getDocID() {
        return DocID;
    }

    public void setDocID(String docID) {
        DocID = docID;
    }

    public String getDocType() {
        return DocType;
    }

    public void setDocType(String docType) {
        DocType = docType;
    }

    public String getDocNumber() {
        return DocNumber;
    }

    public void setDocNumber(String docNumber) {
        DocNumber = docNumber;
    }

    public String getDocIssueDate() {
        return DocIssueDate;
    }

    public void setDocIssueDate(String docIssueDate) {
        DocIssueDate = docIssueDate;
    }

    public String getDocExpireDate() {
        return DocExpireDate;
    }

    public void setDocExpireDate(String docExpireDate) {
        DocExpireDate = docExpireDate;
    }

    public String getDocAddedDate() {
        return DocAddedDate;
    }

    public void setDocAddedDate(String docAddedDate) {
        DocAddedDate = docAddedDate;
    }

    public String getDocFileUrl() {
        return DocFileUrl;
    }

    public void setDocFileUrl(String docFileUrl) {
        DocFileUrl = docFileUrl;
    }

    public String getDocFileBackUrl() {
        return DocFileBackUrl;
    }

    public void setDocFileBackUrl(String docFileBackUrl) {
        DocFileBackUrl = docFileBackUrl;
    }

    public String getDocBodyBack() {
        return DocBodyBack;
    }

    public void setDocBodyBack(String docBodyBack) {
        DocBodyBack = docBodyBack;
    }
}
