package com.acemoneytransfer.model;

/**
 * Created by suarebits on 7/11/15.
 */
public class BankBranchModel {
    String BranchId="",BranchName="",BranchAddress="",BranchCode="",BranchPayCode="";

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getBranchAddress() {
        return BranchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        BranchAddress = branchAddress;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public String getBranchPayCode() {
        return BranchPayCode;
    }

    public void setBranchPayCode(String branchPayCode) {
        BranchPayCode = branchPayCode;
    }
}
