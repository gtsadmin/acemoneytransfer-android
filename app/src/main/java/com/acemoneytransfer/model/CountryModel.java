package com.acemoneytransfer.model;

/**
 * Created by suarebits on 5/11/15.
 */
public class CountryModel {
    String CountryName="",Iso2Code="",Iso3Code="",IsoNumericCode="",CurrencyIsoCode="";

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getIso2Code() {
        return Iso2Code;
    }

    public void setIso2Code(String iso2Code) {
        Iso2Code = iso2Code;
    }

    public String getIso3Code() {
        return Iso3Code;
    }

    public void setIso3Code(String iso3Code) {
        Iso3Code = iso3Code;
    }

    public String getIsoNumericCode() {
        return IsoNumericCode;
    }

    public void setIsoNumericCode(String isoNumericCode) {
        IsoNumericCode = isoNumericCode;
    }

    public String getCurrencyIsoCode() {
        return CurrencyIsoCode;
    }

    public void setCurrencyIsoCode(String currencyIsoCode) {
        CurrencyIsoCode = currencyIsoCode;
    }
}
