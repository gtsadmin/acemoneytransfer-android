package com.acemoneytransfer.model;

/**
 * Created by suarebits on 6/1/16.
 */
public class ExchangeRateModel {
    String ExchangeRate="",RecievingCountryName="",RecievingCurrencyyISOCode="",PayerID="",PayerName="";

    public String getExchangeRate() {
        return ExchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        ExchangeRate = exchangeRate;
    }

    public String getRecievingCountryName() {
        return RecievingCountryName;
    }

    public void setRecievingCountryName(String recievingCountryName) {
        RecievingCountryName = recievingCountryName;
    }

    public String getRecievingCurrencyyISOCode() {
        return RecievingCurrencyyISOCode;
    }

    public void setRecievingCurrencyyISOCode(String recievingCurrencyyISOCode) {
        RecievingCurrencyyISOCode = recievingCurrencyyISOCode;
    }

    public String getPayerID() {
        return PayerID;
    }

    public void setPayerID(String payerID) {
        PayerID = payerID;
    }

    public String getPayerName() {
        return PayerName;
    }

    public void setPayerName(String payerName) {
        PayerName = payerName;
    }
}
