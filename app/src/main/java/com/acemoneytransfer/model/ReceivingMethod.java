package com.acemoneytransfer.model;

/**
 * Created by suarebits on 26/10/15.
 */
public class ReceivingMethod {

    String BankName="";
    String BankCode="";
    String PayerId="";
    String ISO_Code="";
    String PayerName="";
    String branch_name="";
    String payer_branch_name="";

    public String getPayInAmount() {
        return PayInAmount;
    }

    public void setPayInAmount(String payInAmount) {
        PayInAmount = payInAmount;
    }

    String PayInAmount ="";
    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    String country_name="";
    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public String getPayer_branch_name() {
        return payer_branch_name;
    }

    public void setPayer_branch_name(String payer_branch_name) {
        this.payer_branch_name = payer_branch_name;
    }

    public String getSelect_payment_method() {
        return select_payment_method;
    }

    public void setSelect_payment_method(String select_payment_method) {
        this.select_payment_method = select_payment_method;
    }

    String select_payment_method="";

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getPayerId() {
        return PayerId;
    }

    public void setPayerId(String payerId) {
        PayerId = payerId;
    }

    public String getISO_Code() {
        return ISO_Code;
    }

    public void setISO_Code(String ISO_Code) {
        this.ISO_Code = ISO_Code;
    }

    public String getPayerName() {
        return PayerName;
    }

    public void setPayerName(String payerName) {
        PayerName = payerName;
    }
}
