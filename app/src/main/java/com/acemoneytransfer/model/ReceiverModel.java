package com.acemoneytransfer.model;

import java.io.Serializable;

/**
 * Created by suarebits on 23/10/15.
 */
public class ReceiverModel implements Serializable {
    String BeneID = "", BeneName = "", BeneAddress = "", BeneCity = "",
            BenePostCode = "", BeneCountryIsoCode = "", BenePhone = "",
            BeneBankName = "", BeneBranchName = "", BeneAccountNumber = ""
            ,BeneBankCode="",BeneBankBranchCode="";

    public String getBeneBankCode() {
        return BeneBankCode;
    }

    public void setBeneBankCode(String beneBankCode) {
        BeneBankCode = beneBankCode;
    }

    public String getBeneBankBranchCode() {
        return BeneBankBranchCode;
    }

    public void setBeneBankBranchCode(String beneBankBranchCode) {
        BeneBankBranchCode = beneBankBranchCode;
    }

    public String getBeneID() {
        return BeneID;
    }

    public void setBeneID(String beneID) {
        BeneID = beneID;
    }

    public String getBeneName() {
        return BeneName;
    }

    public void setBeneName(String beneName) {
        BeneName = beneName;
    }

    public String getBeneAddress() {
        return BeneAddress;
    }

    public void setBeneAddress(String beneAddress) {
        BeneAddress = beneAddress;
    }

    public String getBeneCity() {
        return BeneCity;
    }

    public void setBeneCity(String beneCity) {
        BeneCity = beneCity;
    }

    public String getBenePostCode() {
        return BenePostCode;
    }

    public void setBenePostCode(String benePostCode) {
        BenePostCode = benePostCode;
    }

    public String getBeneCountryIsoCode() {
        return BeneCountryIsoCode;
    }

    public void setBeneCountryIsoCode(String beneCountryIsoCode) {
        BeneCountryIsoCode = beneCountryIsoCode;
    }

    public String getBenePhone() {
        return BenePhone;
    }

    public void setBenePhone(String benePhone) {
        BenePhone = benePhone;
    }

    public String getBeneBankName() {
        return BeneBankName;
    }

    public void setBeneBankName(String beneBankName) {
        BeneBankName = beneBankName;
    }

    public String getBeneBranchName() {
        return BeneBranchName;
    }

    public void setBeneBranchName(String beneBranchName) {
        BeneBranchName = beneBranchName;
    }

    public String getBeneAccountNumber() {
        return BeneAccountNumber;
    }

    public void setBeneAccountNumber(String beneAccountNumber) {
        BeneAccountNumber = beneAccountNumber;
    }
}
