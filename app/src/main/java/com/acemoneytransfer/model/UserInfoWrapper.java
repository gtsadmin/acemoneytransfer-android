package com.acemoneytransfer.model;

/**
 * Created by suarebits on 10/11/15.
 */
public class UserInfoWrapper {

    String FullName="";
    String Email="";
    String Phone="";
    String DOB="";
    String Gender="";
    String CountryIsoCode="";
    String NationalityIsoCode="";
    String CountryOfBirthIsoCode="";
    String Address="";
    String City="";
    String PostalCode="";

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        CurrencyCode = currencyCode;
    }

    String CurrencyCode ="";

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCountryIsoCode() {
        return CountryIsoCode;
    }

    public void setCountryIsoCode(String countryIsoCode) {
        CountryIsoCode = countryIsoCode;
    }

    public String getNationalityIsoCode() {
        return NationalityIsoCode;
    }

    public void setNationalityIsoCode(String nationalityIsoCode) {
        NationalityIsoCode = nationalityIsoCode;
    }

    public String getCountryOfBirthIsoCode() {
        return CountryOfBirthIsoCode;
    }

    public void setCountryOfBirthIsoCode(String countryOfBirthIsoCode) {
        CountryOfBirthIsoCode = countryOfBirthIsoCode;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }
}
