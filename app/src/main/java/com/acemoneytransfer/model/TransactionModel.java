package com.acemoneytransfer.model;

import java.io.Serializable;

/**
 * Created by suarebits on 5/11/15.
 */
public class TransactionModel implements Serializable {

  String bank_name="";
    String station="";
    String station_Address="";
    String currency="";
    String rate="";
    String amount="";
    String due_amount="";
    String charges="";
    String admin_charge="";
    String total_due="";
    String total_paid="";
    String balance="";
    String user_name="";
    String address="";
    String relationship="";
    String telephone="";
    String customer_id="";
    String customer_name="";
    String customer_telephone="";
    String payment_method="";
    String receiver_name="";
    String receiver_lastname="";
    String receiver_contact="";
    String receiver_country="";
    String receiver_city="";
    String receiver_postalcode="";
    String receiver_address="";
    String sending_purpose="";
    String receiverBankCode="";
    String receiverBankName="";
    String receiverBranchName="";
    String receiverBankBranchCode="";
    String ReceiverBankAccountNumber="";
    String bene_id="";
    String payerBankname ="";
    String payerBranchname="";

    public String getPayerBankname() {
        return payerBankname;
    }

    public void setPayerBankname(String payerBankname) {
        this.payerBankname = payerBankname;
    }

    public String getPayerBranchname() {
        return payerBranchname;
    }

    public void setPayerBranchname(String payerBranchname) {
        this.payerBranchname = payerBranchname;
    }

    public String getPayerBranchcode() {
        return payerBranchcode;
    }

    public void setPayerBranchcode(String payerBranchcode) {
        this.payerBranchcode = payerBranchcode;
    }

    String payerBranchcode="";

    public String getIbanNumber() {
        return ibanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        this.ibanNumber = ibanNumber;
    }

    String ibanNumber="";

    public String getReceiverBankAccountNumber() {
        return ReceiverBankAccountNumber;
    }

    public void setReceiverBankAccountNumber(String receiverBankAccountNumber) {
        ReceiverBankAccountNumber = receiverBankAccountNumber;
    }

    public String getBene_id() {
        return bene_id;
    }

    public void setBene_id(String bene_id) {
        this.bene_id = bene_id;
    }

    public String getReceiverBankCode() {
        return receiverBankCode;
    }

    public void setReceiverBankCode(String receiverBankCode) {
        this.receiverBankCode = receiverBankCode;
    }

    public String getReceiverBankName() {
        return receiverBankName;
    }

    public void setReceiverBankName(String receiverBankName) {
        this.receiverBankName = receiverBankName;
    }

    public String getReceiverBranchName() {
        return receiverBranchName;
    }

    public void setReceiverBranchName(String receiverBranchName) {
        this.receiverBranchName = receiverBranchName;
    }

    public String getReceiverBankBranchCode() {
        return receiverBankBranchCode;
    }

    public void setReceiverBankBranchCode(String receiverBankBranchCode) {
        this.receiverBankBranchCode = receiverBankBranchCode;
    }

    public String getReceiver_currency() {
        return receiver_currency;
    }

    public void setReceiver_currency(String receiver_currency) {
        this.receiver_currency = receiver_currency;
    }

    String receiver_currency="";
    public String getSending_amount() {
        return sending_amount;
    }

    public void setSending_amount(String sending_amount) {
        this.sending_amount = sending_amount;
    }

    String sending_amount="";
    public String getReceiver_amount() {
        return receiver_amount;
    }

    public void setReceiver_amount(String receiver_amount) {
        this.receiver_amount = receiver_amount;
    }

    String receiver_amount="";

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getReceiver_lastname() {
        return receiver_lastname;
    }

    public void setReceiver_lastname(String receiver_lastname) {
        this.receiver_lastname = receiver_lastname;
    }

    public String getReceiver_contact() {
        return receiver_contact;
    }

    public void setReceiver_contact(String receiver_contact) {
        this.receiver_contact = receiver_contact;
    }

    public String getReceiver_country() {
        return receiver_country;
    }

    public void setReceiver_country(String receiver_country) {
        this.receiver_country = receiver_country;
    }

    public String getReceiver_city() {
        return receiver_city;
    }

    public void setReceiver_city(String receiver_city) {
        this.receiver_city = receiver_city;
    }

    public String getReceiver_postalcode() {
        return receiver_postalcode;
    }

    public void setReceiver_postalcode(String receiver_postalcode) {
        this.receiver_postalcode = receiver_postalcode;
    }

    public String getReceiver_address() {
        return receiver_address;
    }

    public void setReceiver_address(String receiver_address) {
        this.receiver_address = receiver_address;
    }

    public String getSending_purpose() {
        return sending_purpose;
    }

    public void setSending_purpose(String sending_purpose) {
        this.sending_purpose = sending_purpose;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getStation_Address() {
        return station_Address;
    }

    public void setStation_Address(String station_Address) {
        this.station_Address = station_Address;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDue_amount() {
        return due_amount;
    }

    public void setDue_amount(String due_amount) {
        this.due_amount = due_amount;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getAdmin_charge() {
        return admin_charge;
    }

    public void setAdmin_charge(String admin_charge) {
        this.admin_charge = admin_charge;
    }

    public String getTotal_due() {
        return total_due;
    }

    public void setTotal_due(String total_due) {
        this.total_due = total_due;
    }

    public String getTotal_paid() {
        return total_paid;
    }

    public void setTotal_paid(String total_paid) {
        this.total_paid = total_paid;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_telephone() {
        return customer_telephone;
    }

    public void setCustomer_telephone(String customer_telephone) {
        this.customer_telephone = customer_telephone;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }
}
