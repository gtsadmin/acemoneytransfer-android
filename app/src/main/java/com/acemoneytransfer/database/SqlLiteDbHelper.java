package com.acemoneytransfer.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class SqlLiteDbHelper extends SQLiteOpenHelper {


    public static final String TABLE_COUNTRY = "country";
    public static final String ID_COUNTRIES = "id_countries";
    public static final String NAME = "name";
    public static final String ISO_ALPHA2 = "iso_alpha2";
    public static final String ISO_ALPHA3 = "iso_alpha3";
    public static final String ISO_NUMERIC = "iso_numeric";
    public static final String CURRENCY_CODE = "currency_code";
    public static final String CURRENCY_NAME = "currency_name";
    public static final String CURRRENCY_SYMBOL = "currrency_symbol";
    public static final String FLAG = "flag";
    private DatabaseHelper DBHelper;

    private SQLiteDatabase db;
    public static final String TABLE_GLOBLECODE = "globalareacodes";
    public static final String COUNTRYNAME = "col_1";
    public static final String COUNTRYGLOBLECODE = "col_2";

// All Static variables
// Database Version
    private static final int DATABASE_VERSION = 1;
// Database Name
    private static final String DATABASE_NAME = "ACE_DB.1.sqlite";
    private static final String DB_PATH_SUFFIX = "/databases/";
    static Context ctx;

    public SqlLiteDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        ctx = context;
        DBHelper = new DatabaseHelper(context);

    }

    private class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {


        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("", "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            // dbMds.execSQL("DROP TABLE IF EXISTS " + QUERY_TABLE_TIMELINE);

            onCreate(db);
        }
    }

    public ArrayList<String> getCountryList(String type) {

        ArrayList<String> countryList = new ArrayList<>();

        try {
            String Query = "Select  *  from " + TABLE_COUNTRY;
            System.out.println("Query>>>" + Query);

            Cursor cursor = db.rawQuery(Query, null);
            if (cursor.getCount() > 0) {

                cursor.moveToFirst();
                // wrapper.setStartDate(cursor.getString(cursor
                // .getColumnIndex(STARTDATETIME)));
                // wrapper.setEndDate(cursor.getString(cursor
                // .getColumnIndex(ENDDATETIME)));
                do {
                    countryList.add(cursor.getString(cursor
                            .getColumnIndex(type)));

                    cursor.moveToNext();
                } while (!cursor.isAfterLast());

                return countryList;

                // if (startdate_db >= start_date && startdate_db <= end_date) {
                // return true;
                // }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countryList;

    }

    public void CopyDataBaseFromAsset() throws IOException {

        InputStream myInput = ctx.getAssets().open(DATABASE_NAME);

// Path to the just created empty db
        String outFileName = getDatabasePath();

// if the path doesn't exist first, create it
        File f = new File(ctx.getApplicationInfo().dataDir + DB_PATH_SUFFIX);
        if (!f.exists())
            f.mkdir();

// Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

// transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

// Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }


    private static String getDatabasePath() {
        return ctx.getApplicationInfo().dataDir + DB_PATH_SUFFIX
                + DATABASE_NAME;
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        File dbFile = ctx.getDatabasePath(DATABASE_NAME);

        if (!dbFile.exists()) {
            try {
                CopyDataBaseFromAsset();
                System.out.println("Copying sucess from Assets folder");
            } catch (IOException e) {
                throw new RuntimeException("Error creating source database", e);
            }
        }
        db = DBHelper.getWritableDatabase();

        return SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// TODO Auto-generated method stub

    }
    public HashMap<String,String> getCcountrycode()
    {
        HashMap<String,String> countryCodeMap = new HashMap<>();

        try {
            String Query = "Select  *  from " + TABLE_GLOBLECODE;
            System.out.println("Query>>>" + Query);

            Cursor cursor = db.rawQuery(Query, null);
            if (cursor.getCount() > 0) {

                cursor.moveToFirst();
                // wrapper.setStartDate(cursor.getString(cursor
                // .getColumnIndex(STARTDATETIME)));
                // wrapper.setEndDate(cursor.getString(cursor
                // .getColumnIndex(ENDDATETIME)));
                do {

                    countryCodeMap.put(cursor.getString(cursor
                            .getColumnIndex(COUNTRYNAME)),cursor.getString(cursor
                            .getColumnIndex(COUNTRYGLOBLECODE)));
                    cursor.moveToNext();
                } while (!cursor.isAfterLast());

                return countryCodeMap;

                // if (startdate_db >= start_date && startdate_db <= end_date) {
                // return true;
                // }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return countryCodeMap;
    }
}