package com.acemoneytransfer.application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.acemoneytransfer.R;
import com.acemoneytransfer.activities.LoginActivity;
import com.acemoneytransfer.database.SqlLiteDbHelper;
import com.acemoneytransfer.model.ReceivingMethod;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;
import java.util.HashMap;

public class MyApplication extends Application {
    private ProgressDialog pDialog;
    private static MyApplication myApplication = null;
    private static Context ctx;
    private String ACE_MONEY_TRANSFER = "ACE_MONEY_TRANSFER";
    public static SharedPreferences sp;
    public ImageLoader loader;

    public ImageLoaderConfiguration config;
    private RequestQueue mRequestQueue;
    public static String filepath = "";

    private Activity activity;
    public static final String TAG = MyApplication.class.getSimpleName();
    public static final String APP_URL = "http://104.153.64.72/api/";
    public static boolean DATAUPDATESTUTAS = true;
    public String getSending_method() {
        return sending_method;
    }
    public final static double AVERAGE_MILLIS_PER_MONTH = 365.24 * 24 * 60 * 60 * 1000 / 12;
    private static final String tutorialActivity = "tutorialActivity";
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public void setSending_method(String sending_method) {
        this.sending_method = sending_method;
    }

      String sending_method="",receiving_method="";
     ArrayList<String> countryList = new ArrayList<>();
     ArrayList<String> countryPayerList = new ArrayList<>();
     ArrayList<String> payerBranchList = new ArrayList<>();
     ArrayList<String> countryBankList = new ArrayList<>();

    public HashMap<String, String> getIos_code() {
        return ios_code;
    }

    public void setIos_code(HashMap<String, String> ios_code) {
        this.ios_code = ios_code;
    }

    HashMap<String, String> ios_code = new HashMap<>();
    public ArrayList<String> getCountryBankBranchList() {
        return countryBankBranchList;
    }

    public void setCountryBankBranchList(ArrayList<String> countryBankBranchList) {
        this.countryBankBranchList = countryBankBranchList;
    }

    ArrayList<String> countryBankBranchList = new ArrayList<>();

    public ProgressDialog getpDialog() {
        return pDialog;
    }

    public void setpDialog(ProgressDialog pDialog) {
        this.pDialog = pDialog;
    }

    public ArrayList<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<String> countryList) {
        this.countryList = countryList;
    }

    public ArrayList<String> getCountryPayerList() {
        return countryPayerList;
    }

    public void setCountryPayerList(ArrayList<String> countryPayerList) {
        this.countryPayerList = countryPayerList;
    }

    public ArrayList<String> getPayerBranchList() {
        return payerBranchList;
    }

    public void setPayerBranchList(ArrayList<String> payerBranchList) {
        this.payerBranchList = payerBranchList;
    }

    public ArrayList<String> getCountryBankList() {
        return countryBankList;
    }

    public void setCountryBankList(ArrayList<String> countryBankList) {
        this.countryBankList = countryBankList;
    }



    public String getReceiving_method() {
        return receiving_method;
    }

    public void setReceiving_method(String receiving_method) {
        this.receiving_method = receiving_method;
    }

    public ReceivingMethod getReceivingMethod() {
        return receivingMethod;
    }

    public void setReceivingMethod(ReceivingMethod receivingMethod) {
        this.receivingMethod = receivingMethod;
    }
  public static HashMap<String,String> countryCodeMap = new HashMap<>();
    ReceivingMethod receivingMethod = new ReceivingMethod();


    @Override
    public void onCreate() {

        super.onCreate();
        myApplication = this;
        ctx = getApplicationContext();

        sp = ctx.getSharedPreferences(ACE_MONEY_TRANSFER, 0);

        setImageLoaderConfig();
        loader = ImageLoader.getInstance();
        loader.init(ImageLoaderConfiguration.createDefault(this));
        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(ctx);
        dbHelper.openDataBase();
        countryCodeMap.putAll(dbHelper.getCcountrycode());
//        dbHelper.
    }


    public static boolean isInternetWorking(Activity activity) {
        if (!isConnectingToInternet(activity)) {

            Toast.makeText(activity,
                    "Please connect to working internet connection",
                    Toast.LENGTH_LONG).show();
            return false;

        }
        return true;
    }

    public static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(0).cacheInMemory(true)
            .showImageOnFail(0).cacheOnDisc(true).considerExifParams(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565).build();
    public static DisplayImageOptions option2 = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(500))
            .showImageForEmptyUri(0).cacheInMemory(true)
            .showImageOnFail(0).cacheOnDisc(true).considerExifParams(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565).build();
    public void hideSoftKeyBoard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = activity.getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void setImageLoaderConfig() {

        config = new ImageLoaderConfiguration.Builder(getApplicationContext())

                .memoryCacheSize(20 * 1024 * 1024)
                        // 20 Mb
                .memoryCache(new LruMemoryCache(20 * 1024 * 1024))
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;

    }

    public static void ShowMassage(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getUserData(String type) {

        String e = sp.getString(type, "");
        return e;
    }

    @SuppressLint("CommitPrefEdits")
    public static void saveUserData(String type, String value) {

        Editor editor = sp.edit();

        editor.putString(type, value);

        editor.commit();
    }


//    public static void saveSocialNetwork(String type, boolean isSet) {
//
//        Editor editor = sp.edit();
//
//        editor.putBoolean(type, isSet);
//
//        editor.commit();
//    }

    public static String getSocialFilterType(String type) {

        String e = sp.getString(type, "");
        return e;
    }

    @SuppressLint("CommitPrefEdits")
    public static void saveSocialFilterType(String type, String value) {

        Editor editor = sp.edit();

        editor.putString(type, value);

        editor.commit();
    }
    public static String getAuthToken(String type) {

        String e = sp.getString(type, "");
        return e;
    }

    @SuppressLint("CommitPrefEdits")
    public static void saveAuthToken(String type, String value) {

        Editor editor = sp.edit();

        editor.putString(type, value);

        editor.commit();
    }

    public static String getUserInfo(String type) {

        String e = sp.getString(type, "");
        return e;
    }

    @SuppressLint("CommitPrefEdits")
    public static void saveUserInfo(String type, String value) {

        Editor editor = sp.edit();

        editor.putString(type, value);

        editor.commit();
    }

    public static Boolean getLoginBoolean(String type) {

        boolean e = sp.getBoolean(type, false);
        return e;
    }
    public static boolean getTutarialActivity() {
        boolean auto = false;

        auto = sp.getBoolean(tutorialActivity, false);
        return auto;
    }

    public static void savetutorialActivity(boolean flag) {


        // String e = sp.getString(PASSWORD, null);
        boolean settoserverflag = sp.getBoolean(tutorialActivity, false);
        if (settoserverflag == true) {
            // Do not save, data already in preference
            return;
        }

        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(tutorialActivity, flag);
        editor.commit();

    }
    @SuppressLint("CommitPrefEdits")
    public static void saveloginBoolean(String type, boolean value) {

        Editor editor = sp.edit();

        editor.putBoolean(type, value);

        editor.commit();
    }
    public static String getUserEmail(String type) {

        String e = sp.getString(type, "");
        return e;
    }

    @SuppressLint("CommitPrefEdits")
    public static void saveUserEmail(String type, String value) {

        Editor editor = sp.edit();

        editor.putString(type, value);

        editor.commit();
    }
//    public static boolean isSocialNetworkSet(String type) {
//
//        boolean e = sp.getBoolean(type, false);
//        return e;
//    }

    public static void popErrorMsg(String titleMsg, String errorMsg,
                                   Context context) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(titleMsg).setMessage(errorMsg)
                    .setPositiveButton("Ok", new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public static void popNoBranch(Context context) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("").setMessage("Bank branch unavailable for this bank. Please enter bank branch name and branch code manually")
                    .setPositiveButton("Ok", new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public static MyApplication getApplication() {
        return myApplication;
    }

    public void showProgressDialog(Activity activity) {
        try {

            pDialog = new ProgressDialog(activity);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            if (pDialog.isShowing())
                pDialog.dismiss();
            if (pDialog.isShowing())
                pDialog.dismiss();
            pDialog.show();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        try {
            if (pDialog.isShowing())
                pDialog.dismiss();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }
    public void produceAnimation(View v) {
        Animation alphaDown = new AlphaAnimation(1.0f, 0.3f);
        Animation alphaUp = new AlphaAnimation(0.3f, 1.0f);
        alphaDown.setDuration(1000);
        alphaUp.setDuration(500);
        alphaDown.setFillAfter(true);
        alphaUp.setFillAfter(true);
        v.startAnimation(alphaUp);
    }
    public void popMessage(String msg, final Context context) {
        final Activity activity =(Activity)context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                activity.startActivity(new Intent(context, LoginActivity.class).putExtra("login",2));


            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
    public void buttomtoUp(View view) {
        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_up);

        view.startAnimation(bottomUp);
        view.setVisibility(View.VISIBLE);
    }

    public void uptoButtom(View view) {
        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_down);

        view.startAnimation(bottomUp);
        view.setVisibility(View.GONE);
    }
}
